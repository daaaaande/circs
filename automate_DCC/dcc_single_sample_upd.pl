# wrapper for one single execution per sample- at a time
#!/usr/bin/perl -w
use strict;

open(ER,'>>',"../logfile_auto.log")||die "$!";		# global logfile

chdir "../dcc_2_out/";

my$file_1=$ARGV[0];
chomp $file_1;
my$file_2=$ARGV[1];
chomp $file_2;
my$sample_name=$ARGV[2];
chomp $sample_name;
my$all_out_dir=$ARGV[3]; # directory where this script should put the final outfile into, from all infiles
chomp $all_out_dir;

my$out_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_2_out";
my$scripts_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/DCC/DCC";# really?
my$bowtie_index_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full"; # bowtie2 index build dir, refernce out called "hg38". same for STAR ?
my$infile_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs";
# make all files parameter names





# execute everything for that samploe in that sample dir
mkdir "run_$sample_name";
# make dir, move files there, one dir per sample
chdir "run_$sample_name";#
# we are now in /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_2_out/run_$sample_name/.
my$exec_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_2_out/run_$sample_name";

my$tim=localtime();
my$starttime= time;
print ER "##############################################################\n";
print ER "starting @ $tim \nfinding circs in sample $sample_name with DCC (hg38)...\n";
# both lanes
my$er_star=`STAR --runThreadN 10 --genomeDir $bowtie_index_dir/ --readFilesIn $infile_dir/$file_1 $infile_dir/$file_2 --outFileNamePrefix $sample_name. --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterMultimapNmax 20 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimSegmentMin 15 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15 --limitBAMsortRAM 512000000000`;
print ER "STAR both lanes errors sample $sample_name : $er_star\n";

my$laneonename="lane_1$sample_name";
my$lanetwoname="lane_2$sample_name";

# lane 1
my$lane1_out=`STAR --runThreadN 10 --genomeDir  $bowtie_index_dir --outSAMtype None --readFilesIn $infile_dir/$file_1 --outFileNamePrefix $laneonename. --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --seedSearchStartLmax 30 --outFilterMultimapNmax 20 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimSegmentMin 15 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15 --limitBAMsortRAM 512000000000`;
print ER "STAR lane 1 sample $sample_name  errors:$lane1_out\n";

# lane 2
my$lane2_out=`STAR --runThreadN 10 --genomeDir  $bowtie_index_dir --outSAMtype None --readFilesIn $infile_dir/$file_2 --outFileNamePrefix $lanetwoname. --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --seedSearchStartLmax 30 --outFilterMultimapNmax 20 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimSegmentMin 15 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15 --limitBAMsortRAM 512000000000`;
print ER "STAR lane 2 sample $sample_name  errors:$lane2_out\n";


my$bothlanesname="$sample_name.Chimeric.out.junction";
#
#
#//TODO//
my$dcc_run=`python2 $scripts_dir/main.py  $exec_dir/$bothlanesname -mt1 $exec_dir/$laneonename.Chimeric.out.junction -mt2 $exec_dir/$lanetwoname.Chimeric.out.junction -D -an $bowtie_index_dir/hg38_ucsc_ref_new_chroms.gtf -Pi -M -Nr 2 1 -A $bowtie_index_dir/hg38_full.fa`;
print ER "DCC run sample $sample_name errors: $dcc_run\n";
# now we can parse it like before, maybe check other DCC params and fix STAR index on HPC
#
#

#
#
#
my$sed=`sed -i '1d' CircRNACount`;
my$sed2=`sed -i '1d' CircCoordinates`;# to make number of lines equal in both files
print "errors removing headers:$sed2\t$sed\n";

# bedtools annotations
print ER "using bedtools for annotation of CircRNACount file for $sample_name...\n";
my$betout=`bedtools window -a $exec_dir/CircRNACount -b $bowtie_index_dir/chr_refseq_genes_hg38.bed -w 1 >$exec_dir/CircRNACount_annotated.tsv`;
print ER "errors annotating DCC hg38 sample  $sample_name:$betout\n";



# now parsing the output
print ER "parsing in run_$sample_name ...\n";
my$err_running_dcc_outreader=`perl $infile_dir/automate_DCC/dcc_2_outreader.pl $exec_dir/CircRNACount_annotated.tsv $exec_dir/CircCoordinates $exec_dir/processed_run_$sample_name.tsv $sample_name`;
print ER "errors parsing in run_$sample_name/ : $err_running_dcc_outreader\n";
# in the annotated file, we see
# chr20	25656368	25656448	+	1	chr20	25654850	25677469	NM_015655	0	-	25655667	25667053	0	5	2823,96,127,76,75,	0,11352,11781,12176,22544,
#DCCcirc coordinates not yet -1 corrected| quant | bedtools found gene coordinates surrounding circRNA coords | refseqid connected to those | exons
my$cp_out=`cp $exec_dir/processed_run_$sample_name.tsv $out_dir/$all_out_dir/`;
print ER "DCC2 e_copy $sample_name: $cp_out\n";


my$total_time=((time)-$starttime)/60;
print ER "done. \n\n took $total_time minutes for sample $sample_name\n";
print ER "############################################################\nsample  DCC2 hg38  $sample_name done.\n";
