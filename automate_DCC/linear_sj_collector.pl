#!/usr/bin/perl -w
use strict;
# collect the linear Sj files, cat, annotate and create .mat3 file with all
use Getopt::Long qw(GetOptions);

# defaults
my$annotater_dir=".";
my$dir =".";
my$delete=0;
my$outfile="matrix_linear_reads.tab";
my$mapping_file="/home/daric/work_enclave/auto_circs/auto_find_circ/refseq_to_gene_names_hg38.tsv"; # need to hand that later to refseq_to_gene.pl
my$bed_file="~/work_enclave/auto_circs/auto_find_circ/hg38_ucsc_refseq_all.bed";
GetOptions('mapping_file|m=s' =>\$mapping_file,'dir|d=s'=>\$dir,'outfile|o=s'=>\$outfile,'anot_d|an=s'=>\$annotater_dir,'delete|del=i'=>\$delete,'bedfile|bed=s'=>\$bed_file)|| warn "using default parameters!\n";
#
chomp ($dir,$outfile,$mapping_file,$annotater_dir,$delete,$bed_file);


# get the files of interest
my$files_to_do=`ls -1f $dir/*linearSJ.out.tab`;
# for these files :
#
#column 2: first base of the intron (1-based)
#column 3: last base of the intron (1-based)
#column 4: strand (0: undefined, 1: +, 2: -)
#column 5: intron motif: 0: non-canonical; 1: GT/AG, 2: CT/AC, 3: GC/AG, 4: CT/GC, 5:
#AT/AC, 6: GT/AT
#column 6: 0: unannotated, 1: annotated (only if splice junctions database is used)
#column 7: number of uniquely mapping reads crossing the junction
#column 8: number of multi-mapping reads crossing the junction
#column 9: maximum spliced alignment overhang

#
#
#
#
print("\nfiles with linear splice junctions found:\n$files_to_do\n");
my@all_single_files=split(/\n/,$files_to_do);

foreach my $file (@all_single_files){
      chomp $file;
      # annotate each file first - we need a matrix infile-y thing
      my$annot=`bedtools window -a $dir/$file -w 1 -b $bed_file >$dir/$file.annotated `;

      # sample anme cleanup
      my$sample_name=$file;

      $sample_name=~s/\.linearSJ\.out\.tab//igx;
      $sample_name=~s/\///igx;
      $sample_name=~s/\.//igx;

      my$outfile_matready="$dir/"."$file._prepared.tsv";
      # read the file into array
      open(IN,"$dir/$file.annotated")|| die "$!";
      my@alines=<IN>;
      open(OUT,">",$outfile_matready)|| die "$!";
      print OUT "i_coords\tstrand\tsample_name\tquant\tintron_motif\tmax_splc_al_overh\trefseqid\n";
      foreach my $single_line (@alines){
            my@line_parts=split(/\s+/,$single_line);
            my$chrom=$line_parts[0];
            my$strand=$line_parts[1];
            if($strand =~/1/){
                  $strand="+";
            }
            elsif($strand =~/2/){
                  $strand="-";
            }
            else{
                  $strand="not-decided";
            }
            my$start=$line_parts[1];
            my$end=$line_parts[2];
            my$num_reads=$line_parts[6];
            my$intron_coords="$chrom".":"."$start"."-"."$end";
            my$refseqid=$line_parts[12];
            my$intron_motif=$line_parts[4];
            my$max_al_splice_overhang=$line_parts[8];
            # get each lines
            # split each line
            # construct valid coordinates for annotation
            # annotate linear_sj coordinates
            # - make matrix infile ready
            print OUT "$intron_coords\t$strand\t$sample_name\t$num_reads\t$intron_motif\t$max_al_splice_overhang\t$refseqid\n";




      }

      # get each lines
      # split each line
      # construct valid coordinates for annotation
      # annotate linear_sj coordinates
      # - make matrix infile ready
      # build matrix with refseqid and gene annotation

}
my$cat=`cat $dir/*_prepared.tsv >$outfile`;
print "done\n";
