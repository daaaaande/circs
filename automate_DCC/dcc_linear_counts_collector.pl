#!/usr/bin/perl -w
use strict;
# collect the linearDCC files, cat, annotate and create .mat3 file with all
use Getopt::Long qw(GetOptions);

# defaults
my$annotater_dir=".";
my$dir =".";
my$delete=0;
my$outfile="matrix_linear_reads.tab";
my$mapping_file="/home/daric/work_enclave/auto_circs/auto_find_circ/refseq_to_gene_names_hg38.tsv"; # need to hand that later to refseq_to_gene.pl
my$bed_file="~/work_enclave/auto_circs/auto_find_circ/hg38_ucsc_refseq_all.bed";
GetOptions('mapping_file|m=s' =>\$mapping_file,'dir|d=s'=>\$dir,'outfile|o=s'=>\$outfile,'anot_d|an=s'=>\$annotater_dir,'delete|del=i'=>\$delete,'bedfile|bed=s'=>\$bed_file)|| warn "using default parameters!\n";
#
chomp ($dir,$outfile,$mapping_file,$annotater_dir,$delete,$bed_file);


# get the files of interest
my$files_to_do=`ls -1f $dir/*inearCount*`;
# for these files :
#
#column 2: first base of the intron (1-based)
#column 3: last base of the intron (1-based)
#column 4: strand (0: undefined, 1: +, 2: -)
#column 5: intron motif: 0: non-canonical; 1: GT/AG, 2: CT/AC, 3: GC/AG, 4: CT/GC, 5:
#AT/AC, 6: GT/AT
#column 6: 0: unannotated, 1: annotated (only if splice junctions database is used)
#column 7: number of uniquely mapping reads crossing the junction
#column 8: number of multi-mapping reads crossing the junction
#column 9: maximum spliced alignment overhang

#
#
#
#
print("\nfiles with linear counts found:\n$files_to_do\n");
my@all_single_files=split(/\n/,$files_to_do);

foreach my $file (@all_single_files){
      chomp $file;
      # annotate each file first - we need a matrix infile-y thing
      my$remove_header=`sed '1d' $file >$file.short`;
      # need the other file aswellö-because of strand ?
      my$annot=`bedtools window -a $file.short -b $bed_file >$file.annotated `;

      # sample anme cleanup
      my$sample_name=$file;

      $sample_name=~s/LinearCount//igx;
      $sample_name=~s/\///igx;
      $sample_name=~s/\.//igx;
      $sample_name=~s/gpfsprojectdaric102circs_hilbert_scratchgs//igx;
      # gpfsprojectdaric102circs_hilbert_scratchgs
      # cleanup even more 

      my$outfile_matready="$file.linearcounts_prepared.tsv";
      # read the file into array
      open(IN,"$file.annotated")|| die "$!";
      my@alines=<IN>;
      open(OUT,">",$outfile_matready)|| die "$!";
      print OUT "coords\tstrand\tsample_name\tquant\tintron_motif\tmax_splc_al_overh\trefseqid\n";
      foreach my $single_line (@alines){
            my@line_parts=split(/\s+/,$single_line);
            my$chrom=$line_parts[0];
            my$start=$line_parts[1];
            my$end=$line_parts[2];
            my$num_reads=$line_parts[3]; # need to see the annotated file after a trial execution, guessing col 7 is the raw counts
            my$intron_coords="$chrom".":"."$start"."-"."$end";
            my$refseqid=$line_parts[7];# need to see the annotated file after a trial execution, guessing col 7 is the raw counts
            # get each lines
            my$strand_from_annot=$line_parts[9];
            # split each line
            # construct valid coordinates for annotation
            # annotate linear_sj coordinates
            # - make matrix infile ready
            print OUT "$intron_coords\t$strand_from_annot\t$sample_name\t$num_reads\t1\t1\t$refseqid\n";




      }

      # get each lines
      # split each line
      # construct valid coordinates for annotation
      # annotate linear_sj coordinates
      # - make matrix infile ready
      # build matrix with refseqid and gene annotation

}     # exclude header, summarize all int a matrixmaker ready infile--> no checker or correcteur needed here
my$cat=`cat $dir/*linearcounts_prepared.tsv |grep -v coords >$outfile`;
print "done\n";
