#!/usr/bin/perl -w
use strict;
# dcc automation with options for other reference, paired end and single end reads
# examples:
# perl dcc_automation.pl --i "$infileline" --star STAR
# perl dcc_automation.pl --i "$infileline" --out /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_2_out --f_q /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_full.fa --ref /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full --anot /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_ucsc_ref_new_chroms.gtf --bed /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/hg38_ucsc_refseq_all.bed

use Getopt::Long qw(GetOptions);
# DCC for one sample
#### input parameters ####
my $start = time;
# file directories- you will need to adapt those with the options given                                     # hg 38 equivalent opotions
my$base_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs";
my$out_dir="$base_dir/dcc_out"; # can also use                                                              "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_2_out"
my$dcc_command="python $base_dir/pipelines/DCC/main.py"; # thats how one starts DCC or                      "python2 /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/DCC/DCC/main.py"
my$pl_scripts="$base_dir/automate_DCC";# dir where the parser script should be
my$infile_dir=$base_dir;
my$fastqs_files="$base_dir/pipelines/hg19.fa";# can also use /                                              "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_full.fa"
# other parameters
my$dcc_parse_file="dcc_outreader.pl";# dcc_outreader.pl for hg19, for hg38 dcc_2_outreader.pl
my$refseq_file="$base_dir/pipelines/all_ref.gtf";# can also use                                             "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_ucsc_ref_new_chroms.gtf"
my$bt_ref="$base_dir/pipelines";# where STAR genome index is stored can also use                            "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full"
my$input_line="A_1.fastq      A_2.fastq   A  output_dc";
my$bed_ref="$base_dir/pipelines/Genes_RefSeq_hg19_09.20.2013.bed"; # can also be                             "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/chr_refseq_genes_hg38.bed"
my$mode="pe"; # "pe"- paired end, 2 fastq files per sample ;"se"- single end, one fastq file per sample (no linetwo file needed)
my$dryRun=0; # enter a 1 here to only have the used command printed out, and not executed (good for testing)
my$err_file="../logfile_auto.log"; # logfile
my$threads=12;
my$delete="none";# options are "none" for no deletion of in-between files, "big" for only big files
# retrieving non-default parameters if given- for --i "fastq.fastq (fastq2.fastq) samplename"
my$star_command="STAR";
my$run_parentGeneCountMode=0; # use DCCs built-in parent gene expression detection module if set to 1
my$filter="";# can add filter here like -R path/to/Repeats.gtf
# STAR versions: 2.7.1a /2.5.1.b
# hg19 own source: "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/star/STAR-2.5.1b/bin/Linux_x86_64/STAR"
# hg38 own source: "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/star/STAR-master/bin/Linux_x86_64/STAR"
GetOptions('base_dir|B=s' => \$base_dir,'out_dir|out=s' =>\$out_dir,'logfile|l=s' => \$err_file,'threads|t=i' => \$threads,'parser_dir|pld=s'=>\$pl_scripts,'input_line|i=s' => \$input_line,'mode|M=s' => \$mode,'dry_run|dry=i' => \$dryRun,'infile_dir|i_dir=s' => \$infile_dir,'fastq_files|f_q=s' => \$fastqs_files,'bt_ref|ref=s' => \$bt_ref,'annotation_file|anot=s' =>\$refseq_file,'bed_ref|bed=s'=>\$bed_ref,'star_command|star=s'=>\$star_command,'parser|prs=s'=>\$dcc_parse_file,'dcc_command|dc_co=s'=>\$dcc_command,'delete|del=s'=>\$delete,'get_linear|linear=i'=>\$run_parentGeneCountMode,'filter|fir=s'=>\$filter) or warn "Using default parameters now \n";
# 13 parameters
chomp($base_dir,$dcc_parse_file,$out_dir,$err_file,$threads,$input_line,$mode,$dryRun,$infile_dir,$fastqs_files,$bt_ref,$refseq_file,$pl_scripts,$bed_ref,$star_command,$dcc_command,$delete,$run_parentGeneCountMode,$filter);
chdir $out_dir;
# for debugging and logging: dump final params
# for input line: hand parts into final params
my@split_input_line=split(/\s+/,$input_line);
my$lineonefile=$split_input_line[0];
my$linetwofile=$split_input_line[1];
my$sample_name=$split_input_line[2];
my$outfiles_dir=$split_input_line[3];
if($mode eq "pe" && ($outfiles_dir eq "")){# 4th element is only whitespace
  print("set to paired-end mode but found only 3 columns in infile: switching to single end mode\n");
  $mode="se";
}
# single end mode = only one fastq file
if($mode eq "se"){
      $lineonefile=$split_input_line[0];
      $sample_name=$split_input_line[1];
      $outfiles_dir=$split_input_line[2];
      $linetwofile="none";# this var should then not be needed anymore

}


# creating full file paths for later
my$full_out_dir="$out_dir/$outfiles_dir";
my$sample_dir_out="$out_dir/run_$sample_name";# /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/f_c2_out/run_A/. <- where all files belonging to sample A will be saved
mkdir "$sample_dir_out";
chdir "$sample_dir_out";
#### start of execution ####
open(ER,'>>',$err_file)||die "$!";
print  ER "started DCC $bt_ref run with parameters:\nbase_dir=$base_dir\nthreadsN=$threads\ninfile1=$lineonefile\tinfile2=$linetwofile\tmode=$mode\ndryRun=$dryRun\nsamplename=$sample_name\nout_dir_sample=$sample_dir_out\nreference_files_in_(STAR index dir)=$bt_ref\nfastq_reference_in=$fastqs_files\nwill_copy_final_output_into=$full_out_dir\nref=$bt_ref\nannotation_file=$refseq_file\nparser_dir=$pl_scripts\nstar_command=$star_command\nparser=$dcc_parse_file\ndeleting=$delete\nget_linear_quants=$run_parentGeneCountMode\nfilter=$filter\n";

# still TODO:
# - will create linear/ dir in out_dir if chosen to be run (1)? - or just dump the linear into the run_dir, where it can be collected later by script?
# - will later copy the linear into group_out_dir/linear ?
# - then, somehow we need to merge those into the same format and annotate- linear matrioxmaker? just paste + annotate ?
# cat only useful lines as part of a prep/post guide extension?

# first to check the input params
if($dryRun){
      # just print all commands
      print"\n\ndry run mode. commands:\n";
      if($mode eq "pe"){

            print "$star_command --runThreadN $threads --genomeDir $bt_ref --outSAMtype BAM SortedByCoordinate --readFilesIn $infile_dir/$lineonefile $infile_dir/$linetwofile --outFileNamePrefix $sample_name. --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterMultimapNmax 20 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimSegmentMin 15 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15 --limitBAMsortRAM 512000000000\n" ;
            my$laneonename="lane_1$sample_name";
            my$lanetwoname="lane_2$sample_name";
            my$bothlanesname="$sample_name.Chimeric.out.junction";
            print "$star_command --runThreadN $threads --genomeDir $bt_ref --outSAMtype BAM SortedByCoordinate --readFilesIn $infile_dir/$lineonefile --outFileNamePrefix $laneonename. --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --seedSearchStartLmax 30 --outFilterMultimapNmax 20 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimSegmentMin 15 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15 --limitBAMsortRAM 512000000000\n";
            print "$star_command --runThreadN $threads --genomeDir $bt_ref --outSAMtype BAM SortedByCoordinate --readFilesIn $infile_dir/$linetwofile --outFileNamePrefix $lanetwoname. --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --seedSearchStartLmax 30 --outFilterMultimapNmax 20 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimSegmentMin 15 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15 --limitBAMsortRAM 512000000000\n";
            #print("module load SamTools");
            print("samtools index *.bam");
            print "$dcc_command $sample_dir_out/$bothlanesname -mt1 $sample_dir_out/$laneonename.Chimeric.out.junction -mt2 $sample_dir_out/$lanetwoname.Chimeric.out.junction -D -fg -an $refseq_file -Pi -M -Nr 2 1 -A $fastqs_files -N -T $threads -G $filter \n";
            if($run_parentGeneCountMode){
              # get genecount table for sample
              print "$star_command --runThreadN $threads --genomeDir $bt_ref --readFilesIn $infile_dir/$lineonefile $infile_dir/$linetwofile --outFileNamePrefix $sample_name.linear --quantMode GeneCounts\n";
            }

      }
      elsif($mode eq "se"){
            my$bothlanesname="$sample_name.Chimeric.out.junction";
            print "$star_command --runThreadN $threads --genomeDir $bt_ref --outSAMtype BAM SortedByCoordinate --readFilesIn $infile_dir/$lineonefile --outFileNamePrefix $bothlanesname. --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --seedSearchStartLmax 30 --outFilterMultimapNmax 20 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimSegmentMin 15 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15 --limitBAMsortRAM 512000000000\n";
            #print("module load SamTools\n");
            print("samtools index *.bam\n");
            print "$dcc_command $sample_dir_out/$bothlanesname -D -fg -an $refseq_file -M -Nr 2 1 -A $fastqs_files -N -T $threads -G  $filter\n";
            print "\n";
            if($run_parentGeneCountMode){
              # get genecount table for sample
              print "$star_command --runThreadN $threads --genomeDir $bt_ref --readFilesIn $infile_dir/$lineonefile --outFileNamePrefix $sample_name.linear --quantMode GeneCounts\n";
            }


      }
      print "sed -i '1d' $sample_dir_out/CircRNACount\n";
      print "bedtools window -a $sample_dir_out/CircRNACount -b $bed_ref -w 1 >$sample_dir_out/CircRNACount_annotated.tsv\n";
      print "perl $pl_scripts/$dcc_parse_file $sample_dir_out/CircRNACount_annotated.tsv $sample_dir_out/CircCoordinates $sample_dir_out/processed_run_$sample_name.tsv $sample_name\n";
      print "cp $sample_dir_out/processed_run_$sample_name.tsv $full_out_dir/\n\nfinished.\n";
}

# non-dry run- actually executing here
else{
      print ER "started at $start with DCC sample $sample_name ########################\n";
      if($mode eq "pe"){
            # first alignment: both lanes
            my$er_bt=`$star_command --runThreadN $threads --genomeDir $bt_ref --outSAMtype BAM SortedByCoordinate --readFilesIn $infile_dir/$lineonefile $infile_dir/$linetwofile --outFileNamePrefix $sample_name. --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterMultimapNmax 20 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimSegmentMin 15 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15 --limitBAMsortRAM 512000000000`;
            print ER "$star_command first alignment paired end:$er_bt\n ";
            my$laneonename="lane_1$sample_name";
            my$lanetwoname="lane_2$sample_name";
            my$bothlanesname="$sample_name.Chimeric.out.junction";
            # 2nd alignment : 1st lane only
            my$err_star_line1=`$star_command --runThreadN $threads --genomeDir $bt_ref --outSAMtype BAM SortedByCoordinate --readFilesIn $infile_dir/$lineonefile --outFileNamePrefix $laneonename. --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --seedSearchStartLmax 30 --outFilterMultimapNmax 20 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimSegmentMin 15 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15 --limitBAMsortRAM 512000000000`;
            print ER "errors during STAR lane 1 alignment:\n $err_star_line1\n";
            #3rd alignment: 2nd lane only
            my$err_star_line2=`$star_command --runThreadN $threads --genomeDir $bt_ref --outSAMtype BAM SortedByCoordinate --readFilesIn $infile_dir/$linetwofile --outFileNamePrefix $lanetwoname. --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --seedSearchStartLmax 30 --outFilterMultimapNmax 20 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimSegmentMin 15 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15 --limitBAMsortRAM 512000000000`;
            print ER "errors during STAR lane 2 alignment:\n $err_star_line2\n";
            #now we have
            #run_$samplename/$samplenameChimeric.out.junction for sample1
            #run_$samplename/lane_1$samplenameChimeric.out.junction for lane 1
            #run_$samplename/lane_2$samplenameChimeric.out.junction for lane 2
          #  my$load_samtools=`module load SamTools`;
            my$indexing_mainbam=`samtools index *.bam`;
            print ER "indexing the .bam files for linear: $indexing_mainbam\n";
            my$dcc_err=`$dcc_command $sample_dir_out/$bothlanesname -mt1 $sample_dir_out/$laneonename.Chimeric.out.junction -mt2 $sample_dir_out/$lanetwoname.Chimeric.out.junction -D -fg -an $refseq_file -Pi -M -Nr 2 1 -A $fastqs_files -N -T $threads -G $filter`;
            print ER "errors running dcc: $dcc_err\n";

            if($run_parentGeneCountMode){
              # get genecount table for sample
              my$linear_align_er=`$star_command --runThreadN $threads --genomeDir $bt_ref --readFilesIn $infile_dir/$lineonefile $infile_dir/$linetwofile --outFileNamePrefix $sample_name.linear --quantMode GeneCounts`;
              print ER "error getting raw linear read counts : $linear_align_er\n";
            }

      }

      elsif($mode eq "se"){
            # single read
            my$bothlanesname="$sample_name.Chimeric.out.junction";
            my$err_star_line1=`$star_command --runThreadN $threads --genomeDir $bt_ref --outSAMtype BAM SortedByCoordinate --readFilesIn $infile_dir/$lineonefile --outFileNamePrefix $bothlanesname. --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --seedSearchStartLmax 30 --outFilterMultimapNmax 20 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimSegmentMin 15 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15 --limitBAMsortRAM 512000000000`;
            print ER "errors during STAR single lane alignment:\n $err_star_line1\n";
            #my$load_samtools=`module load SamTools`;
            my$indexing_mainbam=`samtools index *.bam`;
            print ER "indexing the .bam files for linear: $indexing_mainbam\n";

            my$dcc_err=`$dcc_command $sample_dir_out/$bothlanesname -D -fg -an $refseq_file -M -Nr 2 1 -A $fastqs_files -N -T $threads -G $filter`;
            print ER "errors running dcc: $dcc_err\n";
            if($run_parentGeneCountMode){
              # get genecount table for sample
              my $linear_single_end=`$star_command --runThreadN $threads --genomeDir $bt_ref --readFilesIn $infile_dir/$lineonefile --outFileNamePrefix $sample_name.linear --quantMode GeneCounts`;
            }

      }
      ## all other pipeline commands
      #
      my$sed=`sed -i '1d' $sample_dir_out/CircRNACount`;
      my$sed2=`sed -i '1d' $sample_dir_out/CircCoordinates`;
      print "errors removing headers:\n$sed\n$sed2";

      my$betout=`bedtools window -a $sample_dir_out/CircRNACount -b $bed_ref -w 1 >$sample_dir_out/CircRNACount_annotated.tsv`;
      print ER "errors annotating $sample_name :\n$betout\n";

      my$err_running_dcc_outreader=`perl $pl_scripts/$dcc_parse_file $sample_dir_out/CircRNACount_annotated.tsv $sample_dir_out/CircCoordinates $sample_dir_out/processed_run_$sample_name.tsv $sample_name`;
      print ER "errors parsing in $sample_dir_out/processed_run_$sample_name.tsv: \n$err_running_dcc_outreader\n";

      my$er_cp=`cp $sample_dir_out/processed_run_$sample_name.tsv $full_out_dir/`;
      print ER "errors copy outfile to $full_out_dir: $er_cp \n";

      # deleting all in-between files that take up space
      if($delete eq "big"){
        chdir "$sample_dir_out";
        `rm $sample_dir_out/*.bam $sample_dir_out/*.sam $sample_dir_out/*.qfa $sample_dir_out/*Unmapped.out* $sample_dir_out/*.junction $sample_dir_out/*.reads $sample_dir_out/*.bam $sample_dir_out/.bai`;
      }
      elsif($delete eq "none"){
        chdir "$sample_dir_out";
        print ER "user chose to not delete any files\n";
      }
      else{
        print ER "could not recognize delete choice: use --delete none|big\n";
      }



      my $duration = ((time - $start)/60);
      print ER "Execution time DCC $sample_name: $duration minutes\ncreated $sample_dir_out/run_$sample_name.tsv\n";
      print ER "############################################################\ndone.\n";

}

=pod

=head1 dcc_automation.pl

Options with default values :

--base_dir /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs -> where your base repo dir is

--out_dir /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_out" -> where the in-between files will be stored for each sample                                                            "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_2_out"

--dcc_command "python $base_dir/pipelines/DCC/main.py" -> thats how one starts DCC                       "python2 /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/DCC/DCC/main.py"

--parser_dir /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/automate_DCC -> dir where the parser script should be

--infile_dir /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs -> where the infile .fastqs are

--fastq_files /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg19.fa -> where the reference .fasta file is; can also use /                                              "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_full.fa"

--parser dcc_outreader.pl -> which parser to execute; dcc_outreader.pl for hg19, for hg38 dcc_2_outreader.pl

--annotation_file /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/all_ref.gtf -> where genome annotation .gtf file is stored; can also use                                             "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_ucsc_ref_new_chroms.gtf"

--bt_ref /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines -> directory name where STAR genome index is stored; can also use                            "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full"

--input_line "A_1.fastq      A_2.fastq   A  output_dc" -> the input file of the current sample : read1\tread2\tsamplename\trun_out_dir

--bed_ref /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/Genes_RefSeq_hg19_09.20.2013.bed -> genome annotation .bed file; can also be                             "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/chr_refseq_genes_hg38.bed"

--mode pe -> execution mode # "pe"- paired end, 2 fastq files per sample ;"se"- single end, one fastq file per sample (no linetwo file needed)

--dryRun 0 -> enter a 1 here to only have the used command printed out, and not executed (good for testing)

--logfile ../logfile_auto.log -> logfile

--threads 12 -> number of cores available

--star_command STAR -> what exact command should be used to execute STAR: can also use local installations

--delete none -> what to delete : "none"= delete nothing,"big"= delete big in-between files

=cut

1;
