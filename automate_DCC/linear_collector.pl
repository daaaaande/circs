#!/usr/bin/perl -w
use strict;
use Getopt::Long qw(GetOptions);
# example execution : perl ../linear_collector.pl --d .. --o test3_outfile_full_matrix.tsv --an .. --del 1 id parser script an infiles (tab counts ) are in ../.

# defaults
my$annotater_dir=".";
my$dir =".";
my$delete=0;
my$outfile="matrix_linear_reads.tab";
my$mapping_file="/home/daric/work_enclave/auto_circs/auto_find_circ/refseq_to_gene_names_hg38.tsv"; # need to hand that later to refseq_to_gene.pl
GetOptions('mapping_file|m=s' =>\$mapping_file,'dir|d=s'=>\$dir,'outfile|o=s'=>\$outfile,'anot_d|an=s'=>\$annotater_dir,'delete|del=i'=>\$delete)|| warn "using default parameters!\n";
#
chomp ($dir,$outfile,$mapping_file,$annotater_dir,$delete);
# first know a way to get all linear reads into one file,
# rename the 2.nd column as the samplename,
# remove first 3 value columns
# annotate reafseqids
# check if refseqids are the same order
# put out matrix with raw reads


# put script where the linear files are : tab out with 4 columns
#`bash prep_lin.sh`;
#`perl col2_torenamed.pl`;

# to check for any not-same colnames in the readcounts files - if this is only space, then everything is fine
#`diff get_read_counts_try1_paired_hg19ReadsPerGene.out.tab.genes get_read_counts_try1_paired_hg38mb24ReadsPerGene.out.tab.genes`;

# now do
#`paste "last genes file " *.fixed > matrix_raw_.tsv `;

# remove 3 rows but not first from matrix

# rename first column "sample"

# now annotate and add gene id coolumns



# first we want to get a list of alReadsPerGene.out.tab files in the current dir, we will later figure out how to collect them into one dir

my$all_genetab_files=`ls -f1 $dir/*ReadsPerGene.out.tab`;
# count files, do the isolation with it
my@all_files=split(/\n/,$all_genetab_files);
my$num_files=scalar(@all_files);

# do the splitting
foreach my $single_countsfile (@all_files){
  # get column2, rename
  # get column 1, rename
  my$get_line2=`cat $single_countsfile | cut -f 2 -> $single_countsfile.counts`;
  my$get_refseqs= `cat $single_countsfile | cut -f 1 -> $single_countsfile.refseqids`;
}
# now check if all gene files are in the same order, then chosse the first one for matrix creation
my$diffs=`paste $dir/*.refseqids >$dir/all_refseqids_check.txt`; # get all the refseqids- should be the same though
chomp $diffs;
print("check all_refseqids_check.txt for diffs...\n");
#if($diffs =~/[A-z]/igx){
#  die "found differences in genecounts order. check if all infiles were aligned to the same reference \n";
#}


my$using_for_matrix=`ls -1f $dir/*.refseqids | head -n 1 `;
chomp $using_for_matrix;# later the first file to be pasted, add the "sample row later"


# now we need to add the samplename into the counts, add row to genes file

my$all_col2_files=`ls -1f $dir/*.counts`;
my@all_si=split(/\n/,$all_col2_files);
foreach my$reads_d(@all_si){
  # print samplename and
  # then rest of the file into a new file
  chomp $reads_d;
  #$infile=~s///;
  open(IN,$reads_d)|| die "$!";
  my@alllines=<IN>;
  my$count_line=0;
  my$outfile="$reads_d.fixed";
  open(OUT,'>',$outfile)|| die "$!";
  # cleanup the sample name
  $reads_d =~s/get_read_counts_try1_paired_hg//;
  $reads_d =~s/ReadsPerGene*//;
  $reads_d = $reads_d."\n";
  unshift(@alllines,$reads_d);
  # now print the whole array again into file
  print OUT @alllines;
}
# add sample to using_matrix
my$add_sample_row=`sed '1i \ sample' $using_for_matrix >$dir/refseq_column.tx`;
# now all files should have one row more
# paste, then annotate later
# get the old file without the header, add annotations
# $using_for_matrix
my$annotation=`perl $annotater_dir/refseq_to_gene.pl --m $mapping_file --i $dir/refseq_column.tx >$dir/genes_annot.tx`;

# now paste all together
# then remove first 4? lines?
my $paste_all=`paste $dir/genes_annot.tx $dir/refseq_column.tx $dir/*.fixed >$dir/linear_reads_raw_matrix.mat3`;

# cleanup the file: remove lines 2-5 , make nicer my$headergrep

# read the finished file , edit
open( LIN,"$dir/linear_reads_raw_matrix.mat3")|| die "$!\n";
my@all_matrix_lines=<LIN>;

open(OUT,">$outfile")|| die "could not open outfile:$!\n";

for (my$line_count=0;$line_count<(scalar(@all_matrix_lines));$line_count=$line_count + 1 ){
  my$line_of_matrix_raw=$all_matrix_lines[$line_count];
  if($line_count ==0){
    chomp $line_of_matrix_raw;
    $line_of_matrix_raw =~s/None/gene/;
    $line_of_matrix_raw =~s/sample/refseqID/;
    $line_of_matrix_raw =~s/\.out\.tab\.counts//g;
    $line_of_matrix_raw =~s/\///;
    $line_of_matrix_raw =~s/\.//;
    $line_of_matrix_raw =~s/linear//;
    # make None into gene
    # make sample into RefseqID
    # cleanup sampleanmes
    print OUT "$line_of_matrix_raw\n";
  }# header
  elsif($line_count < 5){
    # mapping counts, ignore those
  }
  else{
    print OUT $line_of_matrix_raw;
  }
}


# now cleanup if wanted
if ($delete==1){
  # cleanup
  my$cleanup=`rm $dir/*.counts $dir/*.fixed $dir/*.mat3 $dir/*.refseqids $dir/genes_annot.tx $dir/refseq_column.tx `;
}

close OUT;
1;
