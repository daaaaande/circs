#/usr/bin/perl -w
use strict;

# starting vars
my$currdir=`pwd`;
chdir "../dcc_out/";

open(ER,'>>',"../logfile_auto.log")||die "$!";		# global logfile

`rm Chimeric.out.junction`;
`rm fusion_junction.txt`;

my$file_1=$ARGV[0];
my$file_2=$ARGV[1];
my$sample_name=$ARGV[2];
my$all_out_dir=$ARGV[3]; # directory where this script should put the final outfile into, from all infiles
my$base_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs";
my$tim=localtime();
print ER "##############################################################\n";
print ER "starting @ $tim \nfinding circs in sample $sample_name with DCC ...\n";

my$error=`perl $base_dir/automate_DCC/dcc_starter.pl $file_1 $file_2 $sample_name`;
print ER "errors:\n$error\n\n";
my$errortwo=`cp run_$sample_name/processed_run_$sample_name.tsv $all_out_dir/`;
print ER "errors auto_moving:\n$errortwo\n";
1;
# we expect here the same input as we got from the prior infile:
#inputs: file1,file2,sample,(group)
# could be doable with head/tail of the infile based on array index
# such as:
# filename=`cat /scratch_gs/daric102/pipeline_old_circs_infile.infile | tail -n +${PBS_ARRAY_INDEX} | head -1`
#so now we would get the full line, we might have to split it, argv should take care of this
