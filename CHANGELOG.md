# CHANGELOG for circs
this is a human-readable, up-to-date version of changes in
https://gitlab.com/daaaaande/circs

#### v1.8.4
- created automate_DCC/linear_sj_collector.pl  to deal with linear splice junctions output:
      - collects all linearSJ.out.tab in a dir and makes it a matrixmaker-ready infile.
      - the resultingg outfile can be made into a .mat1 with matrixmakerV4, then a .mat2 with matrixtwo-v4.pl
- fixed up linear_collector.pl- thats the total quantifications of linear transcripts
- introduced samtools index: DCC should now be able to quantify linear transcripts after samtools index star output.bam files 

#### v1.8.3
- reverted annotation file in hg38 f_c and dcc
- run_prep_guide.pl: se mode: does not expect lane identifier
- automation.pl scripts: if paired end mode and only 3 columns in infile, it will automatically switch to single end mode

#### v1.8.2
- added most needed hg38+hg19 annotation files into data_files/ for easy migration
- dcc_automation.pl: added `--filter "-R path_to_repeats.gtf"` (default: off ) parameter for repeatmasking in DCC - need genome specific Repeatsfiles (.gtf formats)
- changed annotation in f_c_outreader.pl, dcc_outreader.pl and execution_scripts/automation_hg38_array_example.sh with new file and more possible columns to get annotations from
- f_c_automation.pl; added `--strict 1` parameter (default : off ) to only accept find_circ alignments with 40x40 qualities- works with all genomes

#### v1.8.1
- dcc_automation.pl: -G does not seem to work for now, enabled linear_counts option now by default (for now)
- f_c_outreader.pl: if the 22.nd column of annotated circ junctions has no RefseqID, check 23.rd and 24.th column

#### v1.8
- automate_find_circ.pl: now renamed auto.bam to sample_name.auto.bam for better identification of .bam files used
- dcc_automation.pl: per default: use DCC linear count option (-G)
- minimized the modules to be loaded in execution_scripts/*.sh ***
- STAR will be loaded only shortly before first STAR execution
- cleanup

#### v1.7.9
- included execution_scripts/prep_unpack.pl, execution_scripts/e_example_unpack.sh and integrated both into run_prep_guide.pl:
      - now able to handle .gz compressed .fastq infiles (.fastq.gz)
      - while the prep_run_guide.pl is running, you will be asked:
            - if the files are packed (if execution_scripts/prep_unpack.pl, execution_scripts/e_example_unpack.sh are needed or not )
            - if it is a small amount of files or not (use the --pbs 1 option in prep_unpack.pl or not)
                  - the rule of thumb here: more than 10 .gz files should be unpacked in parallel (so with the --pbs 1 )
- included new option in pbs_execution.pl:
      - `--au 1` (--automation 1|0) : changes from single_sample.pl scripts to automation.pl scripts, default is 0 (staying at the older single_sample.pl ones)
      - `--scr /path/to/execution/pbs/script/dirs` (--scripts_dir /gpfs/project/daric102/circs_hilbert_scratchgs/exec/); with this you set where the pbs jobfiles will be copied from, best to use the included ones in circs/execution_scripts/

#### v1.7.8
- included trial scripts for linear RNA reads : early stages, so not 100% save yet!  
      - dcc_automation.pl has now the --get_linear 1 option to trigger another STAR alignment that results in GeneCount tables for each sample
      - currently, these GeneCount tables files need to be collected into one directory, where then linear_collector.pl can take over
      - linear_collector summarized GeneCounts into one .tsv file once and needs refseq_to_gene.pl to function, its options enable to define custom directories for the infiles/outfiles/parser script
- by default, matrixmaker uses now the hg38 mapping file also for the hg19 runs. this is due to more annotations and minimal mismatch between existing hg19-> hg38 annotations. this can be changed manually
- added refseq_to_gene.pl as a part of linear matrix creation
- matrix_infile_correctuer.pl: relaxed the samplename cutting a little to not lose unique samplenames while correcting  

### v1.7.7
- execution script automation: fixed differences in circs detected due to different annotation file - before the annotation in the automation was done with a smaller annotation file, thus leading to less detected circs/sample in the find_circ hg38 case only.
- same for hg38 DCC automation run, different .bed now for annotation in execution script
- dcc_2 automation uses now the same DCC version as dcc_single_sample_upd.pl does
- renamed the execution file

### v1.7.6
- matrixmaker-V4.pl: made the circs matcher more strict: before, it could confuse chr2:3-1 with chr2:3-12 (and anything else after the 1 ), now not anymore. this did not happen often but its fixed now
- execution scripts: added a matrix execution example script, here the post_guide edits this file on demand and executes the edited file aswell.
- now all bash execution scripts (can be changed and then executed quickly) are in execution_scripts/.

### v1.7.5
- matrixmaker-4.pl/matrixtwo_V4.pl: now allowing predicted nc/mRNA annotations : XM 	predicted mRNA model;XR 	predicted ncRNA model
- minor adjustment in matrixtwo_V4.pl: hallmark is now cleaned from underscore before the hallmark name starts
- fixed a bug in matrixtwo_V4.pl where the hallmark name was not cleared after each gene by default
- changed the filename for auto_find_circ/find_circ_autmation.pl to auto_find_circ/find_circ_automation.pl , also changed the filename in the example pbs .sh script
- introduced the ability to delete big in-between files with dcc/f_c/cx_automation.pl scripts: `--delete big`- default is now `--delete none`
- introduced the ability to change the executed parser script in dcc_automation.pl:
`--prs dcc_outreader.pl` or `--prs dcc_2_outreader.pl` are the options so far, you can also make your own
version 2 is meant for hg38 runs, since the annotation here results in a slightly different Refseq annotation column compared to hg19  
- added circs logo to README
- run_post_guide.pl: will now log everything in special logfile that gets created next to the matrix creation file, and later moved to matrix job file if requested
- matrixtwo_V4.pl: added parameter `--exclude_circbank 0 ` or `--excl_cb 0` to enable | disable (--exclude_circbank 1) circbank information(spliced length, mm9 circ and coding probability according to circbank information)
- run_post_guide uses this now as a default parameter (`--excl_cb 1`)
- added clarification that DCC needs a .gtf as annotation file (`--anot `), but also a .bed as `--bed`
- cleaned the logfile: deleted old testruns, left one example
- added POD documentation to dcc_automation.pl: `perldoc dcc_automation.pl` to read the manual
- added DCC execution parameter to dcc_automation.pl: `--dcc_command "python $base_dir/pipelines/DCC/main.py"` to change the DCC place that needs to be executed
- starting this CHANGELOG
