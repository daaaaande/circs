# installation instructions  first test_run instructions:
# to be executed after git clone https://gitlab.com/daaaaande/circs is finished
start_dir=`pwd`# can now got to $start_dir/circs/
# installing software
sudo apt install rna-star -y # for hg38 compatability
# download, gunzip and install from https://github.com/alexdobin/STAR/releases/tag/2.5.1b for hg19
sudo apt install python2.7 -y
sudo apt install bedtools -y
sudo apt install bowtie2 -y
sudo apt install samtools -y
sudo cpanm install Parallel::ForkManager # for the matrixmaker scripts
# create output directories - hg19
mkdir $start_dir/circs/pipelines # where reference genome and annotations can be collected
mkdir $start_dir/circs/dcc_out
mkdir $start_dir/circs/f_c_out
mkdir $start_dir/circs/cx1_out
cd $start_dir/circs/pipelines/
# clone circs detection pipelines into pipelines/ folder
# you can also use the original sources
git clone https://github.com/daaaaande/find_circ
git clone https://github.com/dieterich-lab/DCC
git clone https://github.com/daaaaande/CIRCexplorer
git clone https://github.com/YangLab/CIRCexplorer2
cd $start_dir/

# hg38
mkdir $start_dir/circs/cx2_out
mkdir $start_dir/circs/dcc_2_out
mkdir $start_dir/circs/f_c2_out
# logfile- can be customized with automation.pl command --log your/log/file.log
touch circs/logfile_auto.log
# get the reference genome(s)
# hg19
mkdir $start_dir/circs/pipelines/hg19
cd $start_dir/circs/pipelines/hg19
rsync -avzP rsync://hgdownload.cse.ucsc.edu/goldenPath/hg19/chromosomes/
cat *.fa >hg19.fa
bowtie2-build hg19.fa hg19 # for find_circ hg19
STAR --runThreadN 10 --runMode genomeGenerate --genomeDir $start_dir/circs/pipelines/hg19 \
--genomeFastaFiles hg19.fa # for DCC and CX hg19
# download hg19 gene annotqation file fromhttp://genome.ucsc.edu/cgi-bin/hgTables?command=start (.gtf format,hg19 into pipelines/all_ref.gtf)
# do the same with hg38/your custom genome files but in other genome directories not to confuse
# for hg38
cd $start_dir/
mkdir $start_dir/circs/pipelines/hg38
# go to https://hgdownload.cse.ucsc.edu/goldenPath/hg38/bigZips/
# download hg38.fa.gz
gunzip $start_dir/circs/pipelines/hg38.fa.gz
# create genome reference indexes as with hg19.
cd $start_dir/circs/
# get fastq files into circs/
out_dir="test_run"# change this to whatever you want, the outfiles from all runs with this as an outdir will be copied here
mkdir $start_dir/circs/f_c_out/$out_dir
mkdir $start_dir/circs/dcc_out/$out_dir
mkdir $start_dir/circs/cx1_out/$out_dir

perl $start_dir/circs/auto_find_circ/find_circ_automation.pl --B . --S pipelines/find_circ --l logfile_auto.log --t 12 --pld auto_find_circ/ --i "fastq_read1.fq	fastq_read2.fastq	sample1 test_run" --M pe --i_dir path/to/fastq/files --f_q /path/to/all/reference_genome_chroms/ --ref pipelines/hg19 --anot auto_find_circ/Genes_RefSeq_hg19_09.20.2013.bed --out $start_dir/circs/f_c_out --dry 0

perl $start_dir/circs/automate_DCC/dcc_automation.pl  --i "fastq_read1.fq	fastq_read2.fastq	sample1 test_run" --out $start_dir/circs/dcc_out --f_q pipelines/hg19/hg19.fa --ref pipelines/hg19 --anot pipelines/all_ref.gtf --bed auto_find_circ/Genes_RefSeq_hg19_09.20.2013.bed --dry 0

perl $start_dir/circs/circexplorer1_auto/circexplorer_automation.pl --i "fastq_read1.fq	fastq_read2.fastq	sample1 test_run" --m pe --out $start_dir/circs/cx1_out --cx cx1 --t 12 --dry 0 --pld circexplorer1_auto/ --l logfile.log --i_dir path/to/fastq/files --f_q pipelines/hg19/hg19.fa --ref path/to/star/genome/index/dir --anot path/to_annotation_file.txt


# if hg38 test run
mkdir $start_dir/circs/cx2_out/test_run
mkdir $start_dir/circs/dcc_2_out/test_run
mkdir $start_dir/circs/f_c2_out/test_run

infileline="fastq2.fastq	fastq2.fastq	sample1	test_run"
perl $start_dir/circs/auto_find_circ/find_circ_autmation.pl --i "$infileline" --out $start_dir/circs/f_c2_out --f_q $start_dir/circs/pipelines/upd/all_chroms --ref $start_dir/circs/pipelines/hg38_full/hg38 --anot $start_dir/circs/auto_find_circ/hg38_ucsc_refseq_all.bed
perl $start_dir/circs/automate_DCC/dcc_automation.pl --i "$infileline" --out $start_dir/circs/dcc_2_out --f_q $start_dir/circs/pipelines/hg38_full/hg38_full.fa --ref $start_dir/circs/pipelines/hg38_full --anot $start_dir/circs/pipelines/hg38_full/hg38_ucsc_ref_new_chroms.gtf --bed $start_dir/circs/auto_find_circ/hg38_ucsc_refseq_all.bed
perl $start_dir/circs/circexplorer1_auto/circexplorer_automation.pl --i "$infileline" --out $start_dir/circs/cx2_out --f_q $start_dir/circs/pipelines/hg38_full/hg38_full.fa --ref $start_dir/circs/pipelines/hg38_full --anot $start_dir/circs/pipelines/hg38_full/hg_38_ref_fetched.txt --bed $start_dir/circs/auto_find_circ/hg38_ucsc_refseq_all.bed
# once this is done, use instructions in README.md chapter 3 for post-processing
