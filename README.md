# auto_circs
![Alt text](test_reshape_logo1.png?raw=true "circs logo")
# Chapters
1. [the simple workflow (as i am using it)](#simple)
2. [automation.pl scripts options](#automation)
3. [after automation.pl scripts workflow](#afterautomation)
4. [motivation](#mot)
5. [things needed before starting](#need)
6. [how to get things needed before starting](#get)
7. [structure of this repo / folders to create](#struc)
8. [other automation levels](#levels)
9. [script naming structure](#structure)
10. [script functions and use cases](#cases)
11. [example run-through with 1 sample, 3 pipelines, hg19](#example)
12. [matrix example output headers](#exam)
13. [additional credits](#end)

### this is version 1.8
a multiple sample multiple pipeline wrapper for RNA seq -> circ RNA detection;  automating the find_circ pipeline on a server (or HPC)
this is the more setup-friendly version of https://github.com/daaaaande/auto_find_circ, https://github.com/daaaaande/automate_DCC and https://github.com/daaaaande/circexplorer1_auto without duplicate files for each repo.
As I tried to exclude full file paths here, this version should be more easy to setup somewhere else,too.
This should be working with hg19 and hg38- hg38 is setup with CIRCexplorer2, other pipelines left the same.


# 1. the simple workflow <a name="simple"></a>
- for the specific setup i am dealing with (HPC; PBS Pro; Linux;) the simplified workflow (not yet 100% stable) for each new dataset is as follows:
1. download / unpack .fastq.gz files into one directory where nothing else is in (/gpfs/project/daric102/circs_hilbert_scratchgs/fastqs_dataset1/.)
2. `perl run_prep_guide.pl` -> does the preparation and starts the arrayjob (each sample : CX/DCC/F_C with hg19/38/both); can also now uncompress .fastq.gz files!
3. `perl run_post_guide.pl` -> does the cleanup, prepares and executes the matrix creation
4. `Rscript auto_voting.R` and `Rscript norm_a_voted_circs_df.R `-> removes most false positives in the data; normalizes the output dataframes
5. do __science__ with resulting data
- if your setup is different, there are several different ways included to automate the pipelines (godfather.pl/auto_automaker.pl)
- you can fallback to [dcc_|cx_|f_c_]single_sample.pl execution and then continue with chapter 3.
>> sidenote: run_prep_guide.pl uses [dcc_|cx_|f_c]single_sample.pl under the hood, but will be replaced with the [circexplorer|find_circ|dcc]automation.pl scripts once they are 100% tested and working


# 2. introducing [circexplorer|find_circ|dcc]automation.pl scripts: <a name="automation"></a>

      automate_DCC/dcc_automation.pl
      auto_find_circ/find_circ_automation.pl
      circexplorer_auto/circexplorer_automation.pl

to be exectuted once for every sample
can work with any arbitrary genome, paired end and single end mode
### parameters all 3 scripts have in common
- input_line: `--i "A_1.fastq      A_2.fastq   sample_A  output_tests_dir"`: hands over the input files and sample name, dictates where the final output file will be copied to
- out_dir: `--out out_dir`: dictates where the run_samplename  (out_dir/run_sample_name/.)directory will be made where all in-between files will be created. This is different from the output directory part of the input line: the out_dir will become out_dir/run_your_sample_name/ where all in-between files will be giong. the above mentioned directory (output_tests_dir) will only be acessed once the scripts are finished and ONLY the final output file will be copied there. this is done this way to have all in-between files for each sample in a easy to find place and to have a convenient way to collect final outfiles from all samples done in one run.
- logfile: `--l ../logfile_auto.log`: where all logs will be pasted into. for debugging
- threads: `--t 12`: number of available CPU cores (more is better)
- parser_dir: `--pld /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ`: directory where the ..outreader.pl script was downloaded to. needed for parsing of the output file into a matrixmaker-usable .tsv-file- this is specific for each pipeline (so for DCC it will be `--pld /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/automate_DCC`)
- mode: `--M pe`; mode of script : pe = two .fastq files per sample; se = one .fastq file per sample
- dry_run: `--dry 0`; dry run: just print commands that would be executed. good for testing 1 = show commands, 0 = execute commands
- infile_dir: `--i_dir /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs`: directory where the .fastq files are expected
- fastq_files:`--f_q /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg19.fa`: expected .fasta reference genomes (find_circ requires one .fa(sta) file per chromosome, the others are just fine with one big .fa(sta) file )
- bt_ref:`--ref /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg19`: bowtie2/1/STAR reference genome file for Bowtie /place for STAR
- annotation_file:`--anot /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/Genes_RefSeq_hg19_09.20.2013.bed`: circs annotation file, depending on dcc/cx/fc needs to be in .bed/gtf/txt format (DCC:.gtf file but also needs .bed file as `--bed path/to/the/annotation_bed_file.bed`,f_c:.bed,CX:.bed)

### special pipeline-specific parameters
-  circexplorer_auto/circexplorer_automation.pl:
      - exec_mode:`--cx cx1`: CIRCexplorer1 (cx1) or CIRCexplorer2 (cx2) execution
- automate_DCC/dcc_automation.pl and circexplorer_auto/circexplorer_automation.pl:
      - star_command: `--star STAR`: for utilizing different STAR versions for different genomes:  
      # STAR versions: 2.7.1a /2.5.1.b
      # hg19 own source: "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/star/STAR-2.5.1b/bin/Linux_x86_64/STAR"
      # hg38 own source: "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/star/STAR-master/bin/Linux_x86_64/STAR"
- automate_DCC/dcc_automation.pl:
 - parser:`--prs dcc_outreader.pl`: for different parsing options: "dcc_outreader.pl" for hg19, "dcc_2_outreader.pl" for hg38, both included
 - get_linear; `--get_linear 1` : can align RNA data to reference to get linear counts, outfiles can then be taken together with automate_DCC/linear_collector.pl (before that is possible, you will need to collect all linear geneCount tables into one directory)
  >>> linear reads are then RAW and need to be normalized !

 - --filter; `--filter "-R path_to_repeats.gtf"`: default off, can mask repeating regions from DCC parameters

auto_find_circ/find_circ_automation.pl:
- `--strict 1` (default is 0 ) : switches on the find_circ strict filter (40x40 qualities) 


# 3. automation.pl/godfather.pl/f_c|dcc|cx_single_sample|one_lane|upd.pl output processing <a name="afterautomation"></a>

- paired end scenario
- the expected file format is
```bash   
test@test$ head processed_run_LNT229_HOT_2.tsv
coord	strand	sample	num_reads	score	score	annotation
chr1:879632-880025	-	LNT229_HOT_2	2	9	9	NM_152486
chr1:879632-880025	-	LNT229_HOT_2	2	9	9	NM_015658
chr1:1158623-1159348	+	LNT229_HOT_2	14	9	9	NM_016176
chr1:1158623-1159348	+	LNT229_HOT_2	14	9	9	NM_016547
chr1:1169417-1169865	-	LNT229_HOT_2	2	9	9	NM_080605
test@test$
```

- this can be  summarized into one file for each of the three pipelines from multiple samples :

`cat *.tsv >one_big_multi_sample_matrix_maker_input_file_all_find_circ_hg19.tsv`
- this can then be checked :

`perl matrix_infile_checker.pl one_big_multi_sample_matrix_maker_input_file_all_find_circ_hg19.tsv`
- and corrected:

`perl matrix_infile_correcteur.pl one_big_multi_sample_matrix_maker_input_file_all_find_circ_hg19.tsv >corrected_one_big_multi_sample_matrix_maker_input_file_all_find_circ_hg19.tsv`
- now we can run matrixmakerV4.pl :

`perl matrixmaker-V4.pl --i corrected_one_big_multi_sample_matrix_maker_input_file_all_find_circ_hg19.tsv --outfile _multi_sample_matrix_all_find_circ_hg19.mat1 --logfile log.logfile --c circbase_known_circs.bed --g genes_to_refseqID_nc_and_nr.tsv --t 12 --check_refseq 1`
- this matrix is big, so we can make a more user-friendly version with additional information like this :

`perl matrixtwo_V4.pl --i _multi_sample_matrix_all_find_circ_hg19.mat1 --o _multi_sample_matrix_all_find_circ_hg19.mat2 --l logfile.log --m miRNA_circRNA_ineractions.txt --c circRNA_protein_coding_potential.txt --h hallmark_genes.tsv --e mart_export_ensembl_gene_desc.txt --n read_mapping.pl --chseqi 1`
- once we did this for each pipeline, we can vote and filter the coordinates like :

`Rscript auto_voting.R _multi_sample_matrix_all_find_circ_hg19.mat2 _multi_sample_matrix_all_DCC_hg19.mat2 _multi_sample_matrix_all_CX1_hg19.mat2` >> the vote needs to happen BEFORE the normalization !
- with those 3 dataframes we only have the raw counts. to normalize by the number of .fastq reads we need to first create the fastq list file

 `ls -f1 *.fastq>all_fastqs.tx`
- to create the infile needed for each run of the pipeline execution scripts anyway:

 `perl fastq_list_to_infile_circs_ext.pl --g group_test --l1 R1 --l2 R2 --i all_fastqs.tx --m pe >infile.infile`
- with this then filter for the files we want to count reads in

`cat  infile.infile | cut -f1,2 >read_files_list.tx` >> two fastq files per line , separated by tab
- and finally count the reads per sample

 `perl fastqs_to_reads_per_sample.pl --m pe --i read_files_list.tx >reads_per_sample.txt`
- and then normalize the output files we created for each pipeline

`Rscript norm_a_voted_circs_df.R ordered_dcc_approved_by_all_three.csv reads_per_sample.txt norm_voted_appr_dcc_hg19_run_multi_sample.tsv `
- `norm_voted_appr_dcc_hg19_run_multi_sample.tsv` is the final output in this case- open it in R, Excel ...

# 4. ..but why? <a name="mot"></a>
We found ourselves in the situation to look for circular RNAs in human RNA sequence data more and more. The three pipelines find_circ, DCC and CIRCexplorer1 are known and we wanted to look for circs in multiple datasets at best with all three mentioned pipelines and compare the results thereof. the scripts provided are a solution to this problem. Configured correctly, the f_c_single_sample.pl/cx_single_sample.pl and dcc_single_sample.pl scripts will output circs from each pipeline - for all samples in an easy to parse fashion.
There are scripts for one infile-per sample only (single lane RNA-seq data) available aswell (...single_sample_one_lane.pl)!
It also leaves the option open to add samples later, re-run the matrixmaker scripts and redefine groups by just combining the individual outfiles as the user wants. As far as we tested, when you run the same sample 10 times, the output will be the same 10 times. The output format for each of the original pielines is different, but the here provided scripts will re-format the output into a minimalistic .tsv file for each sample in each pipeline.

# 5. will not work if one of these things is missing: <a name="need"></a>
- install instructions once you did git clone https://gitlab.com/daaaaande/circs :
> circs/install.sh includes installation, preparation and first run instructions

> many of the needed files are already included in the pipelines/ dir, including the correct find_circ version scripts
## alternatively install needed things:

- find_circ scripts from the official repo                
- DCC installed
- CIRCexplorer1 installed
- CIRCexplorer2 installed(hg38)
- STAR installed (used in DCC and CIRCexplorer1) (here you can use different versions for hg18 and hg38 if you want to)
- bedtools installed                                      -> `sudo apt install bedtools -y`
- bowtie2 installed                                       -> `sudo apt install bowtie2 -y`
- hg19.fa (hg38.fa)                                                -> download instructions from https://www.gungorbudak.com/blog/2014/04/13/download-human-reference-genome-hg19/ // should work with other organisms aswell
- circbase.org known circular RNAs mapping file           ->(.txt) http://circbase.org/cgi-bin/downloads.cgizz
- all here listed .pl files in the same hierachy         -> `git clone https://gitlab.com/daaaaande/circs`
- perl-module Parallel::ForkManager -> `cpanm install Parallel::ForkManager`# to multi-thread the heavy matrix perl-scripts
- for the R scripts you will need to install packages (in R session): `install.packages("gplots",'dplyr',methods,utils)`

# 6. preparation steps: <a name="get"></a>
- get a linux (Ubuntu) machine (more performance is better)
- install python 2.7, up-to-date Perl (plus package Parallel::ForkManager ;cpanm install Parallel::ForkManager)
- get bowtie2 to run, use hg19 to create genome index in the ./pipelines dir
- get STAR to run, use hg19 to create genome index in the ./pipelines dir : cd pipelines/ then STAR --runThreadN 6 --runMode genomeGenerate --genomeDir . --genomeFastaFiles hg19.fa
- create (mkdir) 3 directories in the auto_circs/ folder: cx1_out/ dcc_out/ and f_c_out/ -> the scripts will try to make all output there sorted by pipeline
- create the global logfile in auto_circs/logfile_auto.log -> where all error messages will be : touch auto_circs/logfile_auto.log
- get find_circ running (should be okay to only download)
- change the directories in each here given perl script (just exchange the file path to where you want to run it)
- first test test2.pl, then steptwo.pl, then find_circ_auto.pl, then f_c_single_sample.pl - matrixmaker_V4.pl and matrixtwo_V4.pl are optional and are helpful for multiple samples, test them only if you intend to use them
- get DCC and CIRCexplorer1 to work, you might need to install some more dependencies
- adjust usage to hardware (unfortunately not all steps are multi-threaded, but most steps do have some core parameters where you could optimize the usage)  
- godfather.pl only needs to work if you are planning to use more than one pipeline on the same sample/infiles with one command
- the here written scripts are made to work with hg38 AND hg19 - if you want to use both references and tailored scripts, you need to download reference files for hg38 aswell


# 7. example for prepared dirs <a name="struc"></a>
- all for STAR/Bowtie2/find_circ/DCC/CIRCexplorer1 needed file should be in /pipelines
```bash
test@test$ ls auto_circs/
auto_find_circ/
automate_DCC/
circexplorer1_auto/
cx1_out/
cx2_out/ - hg38 out
dcc_2_out/ - hg38 out
dcc_out/
f_c2_out/ - hg38 out
f_c_out/
LICENSE
logfile_auto.log
pipelines/ -> place for all mapping files and references
README.md
fastq_file_lane1.fastq
fastq_file_lane2.fastq
test_infile.infile
```

 >> for debugging: the logfile_auto.log includes error messages from every of these scripts and additional information

# 8. if the [circexplorer|find_circ|dcc]automation.pl scripts do not work for you: <a name="levels"></a>
###  you can test on each of these 3 levels of automation:
  __________________________
  1. manually:
`perl test2.pl infilelane1.fastq infilelane2.fastq samplename`
   this will create the dir find_circ/run_samplename/ and put the outfile in $dirn/auto_run_samplename.sites.bed  
  `perl steptwo.pl steptwoinput=steponedir/run_$samplename/auto_run_samplename.sites.bed `
   will create $dirn/auto_run_samplename.sites.bed.csv with better coordinates and only relevant information in one easy to parse \t separated file
   optional  step:   
  `cat all_samples_steptwo:output.csv >all_interesting_samples_circs.in  `   
   optional  step:   
  `perl matrixmaker-V4.pl --i steptwooutput.csv --o matrixoutput.tsv`     
   (or for multiple samples at once : all_interesting_samples_circs.in ) matrixoutput.tsv : this will create the file allimportantmatrix.txt where all circs with the relevent information is in.
   optional  step:   
  `perl matrixtwo_V4.pl --i matrixoutput.tsv --o matrixtwo_out.tsv `<- this file should be readable for R, Excel... # this will create a second, more dense form of information from the first matrix and add a few extra mappings
  __________________________
  2. find_circ_auto.pl above scripts executed for one sample:   
`perl find_circ_auto.pl infilelane1.fastq infilelane2.fastq samplename`
    -> perl matrixmaker-V4.pl can be done manually with its output aswell as
    -> matrixtwo_V4.pl can be used later to make the same information more dense
  __________________________
  3. find_circ_single_sample.pl script:
  `perl find_circ_single_sample.pl`  
  -> matrixmaker/matrixtwo scripts can be executed on output
  -> runs test2.pl and steptwo.pl under the hood

## DCC and CIRCexplorer1 differences:
- both do include only a few scripts to automate their respective pipeline, all other scripts are taken from auto_find_circ/ to avoid redundancies.
- both do include single sample- automation scripts (starter), and outfile converter (outfile_reader) that outputs a format compatable with the output from the other two pipelines.
- both also work with single lane: ...single_sample_single_lane.pl
- both have a updated, hg38 compatable verion : ..one_sample_upd.pl

# 9. script naming overview <a name="structure"></a>

execution_scripts/:
- bash files for execution of hg19 pipeline, hg38 pipeline and both (automation_job_all.sh)

find_circ:
- /auto_find_circ/f_c_single_sample.pl executes auto_find_circ/test2.pl and then /auto_find_circ/steptwo.pl for each sample line provided

DCC:
- automate_DCC/dcc_ex_single_sample.pl executes automate_DCC/dcc_starter.pl
- automate_DCC/dcc_starter.pl executes the DCC pipline and then formats its output with automate_DCC/dcc_outreader.pl for each sample line provided

CIRCexplorer:
- circexplorer1_auto/cx1_ex_single_sample.pl executes /circexplorer1_auto/circexplorer1_starter_1.pl and then circexplorer1_auto/circexplorer1_out_reader.pl for each sample line provided

..single_sample_one_lane.pl scripts:
- do the same as above, just with one input.fastq file per sample (infile thus needs to include only infile,sample and group)
- hg19 only

..upd.pl scripts:
- are the newest versions, more easy to read and understand. made for execution with hg38
- paired end only

>> for best results, use the newest versions of matrixmaker and matrixtwo (V4).pl !


# 10. what are all the scripts for? <a name="cases"></a>

#### auto_automaker.pl
- executes multiple find_circ_auto.pl (same as f_c_single_sample.pl) and then matrix_V4/matrixtwo_V4.pl for each group and all samples combined.[ f_c_single_sample.pl executes test2.pl (main find_circ) and steptwo.pl (find_circ output filtering)].

#### auto_voting.R
- is an example R script to filter for circRNA that are only detected in all three pipelines. Takes 3x matrixtwo outfiles as infiles.

#### f_c_single_sample_(one_lane).pl
- executes test2/steptwo for one sample (one lane sample). here for arrayjobs in the qeueing system of the utilized HPC its currently working on.

#### fastq_list_to_infile_circs_ext.pl
- input file (--i input_file.tx) is a ```ls -1 *.fastq >input_file.tx``` with the filenames in only ONE column
- can be used with paired end (--m pe ), single end (--m se) for either one or two files per sample
- needs the file1/two identifier as parameter (--l1 R1 --l2 R2) for mode pe
- puts all samples into one group (--g group)
- getting a lot of .fastq files into the infile format;
```bash
 file1.fastq\tfile2.fastq\tsample_name\toutput_dir\n
 ```
- more explanation in the file itself.

#### godfather.pl
- can start all three pipelines for all samples (3x auto_automaker.pl for all samples) in infile given.
- starts matrixmaker / matrixtwo for each group found in the infile and in the end for all samples combined aswell.

#### matrix_infile_checker.pl
- checks infiles for matrix (steptwo output, for other pipelines outreader.pl) for errors that will affect the created matrix. good if you "cat" multiple samples into one matrixmaker_V3A.pl infile and want to check if you forgot to remove headers.

#### matrix_infile_correcteur.pl
- tries to fix issues found with matrix_infile_checker.

#### matrix_infile_to_sample_names_included.pl
- can quickly derive all sample names included in an matrixmaker input file (all_interesting_samples_circs.in)

#### read_mapping.pl
- part of matrixtwo_V4.pl, reads a mapping file into a hash.

#### rna_seq_infile_combiner.pl
- can be used if you have multiple .fastq files that belong to the same sample/lane of the sample. cats fitting files together into two final files/sample.

#### run_prep_guide.pl
- can be started once all .fastq(.gz) files are in one folder and ready to be analyzed
- guides through the creation of infile scripts to creation of folders for output
- can be used to prepare each run if set up correctly
- can also submit an arrayjob
- after the arrayjob you can continue with run_post_guide.pl

#### run_post_guide.pl
- to be run after all jobs from run_prep_guide are finished
- guides you through post batch run- matrix infile creation for each used pipeline
- needs parent dir (/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs) and script dir as parameters, asks for everything else
- cleans up the out_dir/run_*** into out_dir/all_samples_project_name/run_***
- can also submit an arrayjob for matrix creation

#### trimmomatic_infiles_creator_from_dir.pl
- does what it says. helps to prepare trimmomatic jobs infiles from a list of .fastq files  in a given dir.
- can work with paired end , single end and paired end .gz files (--m pe/se/pez)
- needs lane identifier for modes pe and pez (--l1 R1 --l2 R2)
- input file from ```ls -1 *.fastq(gz) >trim_input.tx``` (--i trim_input.tx)

#### fastqs_to_reads_per_sample.pl
- takes the first 2 columns of a infile and creates a table with sample names and read counts in both .fastq files for a given sample. needs to be executed in directory of said .fastq files.
- needs an input file with eiter two (--m pe ) or one (--m se) .fastq filenames per line (from ```cat  infile.infile | cut -f1,2 >read_files_list,tx```) (--i read_files.tx)


#### execution_scripts/prep_unpack_job.pl
- can be used independently from prep_run_guide.pl but is part of it
- needs arguments:
      - `--g .`: dir where the .gz files are, defaults to "." (--gz_dir where/gz/files/are)
      - `--pbs 1`: if you want to use a PBS job or not. defaults to 1 (yes) (--use_pbs 0|1 )
      if set to 0, it will start gunzip commands in the current shell
      - `--j /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/execution_scripts/e_example_unpack.sh` where the PBS example script is. (--jobfile path/to/jobfile/jobfile.sh)

#### .txt /.bed/ .tsv files
- mapping files used for matrix1/2.

## hg38 executables

#### full_fin_circ_upd.pl
- includes the entire find_circ pipeline (test2.pl + steptwo.pl) from a single input line (file1.fastq\tfile2.fastq\tsample_name\toutput_dir\n) to matrixmaker input file for one sample.
- made with hg38 reference files
- needs to be adjusted to work in other environments (directories in lines 10-14)

# 11. example execution - 3 pipelines, 4 samples,  one infile, hg19 <a name="example"></a>


 example infile :
```bash

test@test$ cat test_infile.infile
LNT229_HOT_R1_trimmed.fastq     LNT229_HOT_R2_trimmed.fastq     LNT229_HOT_2    ulvi_gbm2
LNT229_neg_R1_trimmed.fastq     LNT229_neg_R2_trimmed.fastq     LNT229_neg_2    ulvi_gbm2
T98G_Hot_R1_trimmed.fastq       T98G_Hot_R2_trimmed.fastq       T98G_Hot_2      ulvi_gbm2
T98G_neg_R1_trimmed.fastq       T98G_neg_R2_trimmed.fastq       T98G_neg_2      ulvi_gbm2
test@test$
```
pipeline_execution_script.sh:
```bash

# filename retrieval
infileline=`cat /repo/circs/test_infile.infile | tail -n +${PBS_ARRAY_INDEX} | head -1`


echo "this is run 1 of sample $infileline " >> $LOGFILE


# find_circ

cd repo/circs/auto_find_circ/ 2>&1 >>$LOGFILE

perl f_c_single_sample.pl $infileline

echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " f_c run $infileline is done ...">>$LOGFILE


# DCC

cd repo/circs/automate_DCC/ 2>&1 >>$LOGFILE

perl dcc_ex_single_sample.pl $infileline

echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " DCC run $infileline is done ...">>$LOGFILE


# CX1

cd repo/circs/circexplorer1_auto/ 2>&1 >>$LOGFILE

perl cx1_ex_single_sample.pl $infileline

echo " CIRCex1 run $infileline should also be done ...">>$LOGFILE
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
```
example output files + all in-between file should be found in dcc_out/run_LNT229_HOT_2/processed_LNT229_HOT_2.tsv and all primary outfiles of the run should be in dcc_out/ulvi_gbm2/

example single-sample output file :

```bash   
test@test$ head processed_run_LNT229_HOT_2.tsv
coord	strand	sample	num_reads	score	score	annotation
chr1:879632-880025	-	LNT229_HOT_2	2	9	9	NM_152486
chr1:879632-880025	-	LNT229_HOT_2	2	9	9	NM_015658
chr1:1158623-1159348	+	LNT229_HOT_2	14	9	9	NM_016176
chr1:1158623-1159348	+	LNT229_HOT_2	14	9	9	NM_016547
chr1:1169417-1169865	-	LNT229_HOT_2	2	9	9	NM_080605
test@test$
```
here you can see that some "hits" are twice in that infile- thats because the same coordinates were annotated with two different refseqids, resulting in two unique lines. However, this will be ignored by the matrixmakers and only the first annotation will be used.


 after we collected all outfiles, catted them into one big infile per pipeline and checked/corrected it, we can finally create the matrix like

 ```bash
perl repo/circs/auto_find_circ/matrixmaker-V4.pl --i infiles_all_dcc_corrected.tsv --o mat1_dcc.tsv

perl repo/circs/auto_find_circ/matrixtwo_V4.pl --i mat1_dcc.tsv --o all_dcc_.mat2

echo " matrixtwo for run XX DCC is done ...">>$LOGFILE

 ```
 if you downloaded the here used scripts, you might need to change directories or give all used mapping files as parameter (--c --g --l for matrixmaker-V4.pl and for matrixtwo_V4.pl --m --c --h --e --l --n) with correct file paths for matrixmaker/matrixtwo.

now we can look at both files from each pipeline (mat1_dcc.tsv and all_dcc_.mat2 outfiles) and filter, sort, vote...
an example of that is given in auto_vote.R

# 12. example output files <a name="exam"></a>
- first, a corrected .tsv outfile from one of the pipelines, those are made 1 time for each sample for each pipeline (for each reference chosen):
```bash   
test@test$ head processed_run_LNT229_HOT_2.tsv
coord	strand	sample	num_reads	score	score	annotation
chr1:879632-880025	-	LNT229_HOT_2	2	9	9	NM_152486
chr1:879632-880025	-	LNT229_HOT_2	2	9	9	NM_015658
chr1:1158623-1159348	+	LNT229_HOT_2	14	9	9	NM_016176
chr1:1158623-1159348	+	LNT229_HOT_2	14	9	9	NM_016547
chr1:1169417-1169865	-	LNT229_HOT_2	2	9	9	NM_080605
test@test$
```
here you can see that some "hits" are twice in that infile- thats because the same coordinates were annotated with two different refseqids, resulting in two unique lines. However, this will be ignored by the matrixmakers and only the first annotation will be used.

- second, a .mat1 file as matrixmaker-V4.pl output with multiple samples included:
```bash   
test@test$ head -n 2 all_gbms_mm4_f_c_.mat1
coordinates	strand	RefseqID	Gene	known_circ	num_samples_present	total_sum_unique_counts	qualities	present_in_sample	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB	sample	-unique_count	-qualA	-qualB
chr1:155695172-155695810	+	NM_001199849	DAP3	hsa_circ_0014614	5	11	,37;4,5;38,5;38,4;38,6;39	-ICGC_GBM11_L7-ICGC_GBM49_L6-ICGC_GBM59_L8-ICGC_GBM60_L1-ICGC_GBM7_L8	ICGC_GBM11_L7	2	37	4	ICGC_GBM15_L1	0	0	0	ICGC_GBM16_L2	0	0	0	ICGC_GBM16_L7	0	0	0	ICGC_GBM17_L3	0	0	0	ICGC_GBM17_L8	0	0	0	ICGC_GBM18_L4	0	0	0	ICGC_GBM18_L8	0	0	0	ICGC_GBM19_L5	0	0	0	ICGC_GBM19_L8	0	0	0	ICGC_GBM1_L4	0	0	0	ICGC_GBM1_L7	0	0	0	ICGC_GBM24_L5	0	0	0	ICGC_GBM25_L6	0	0	0	ICGC_GBM26_L7	0	0	0	ICGC_GBM27_L2	0	0	0	ICGC_GBM28_L3	0	0	0	ICGC_GBM2_L5	0	0	0	ICGC_GBM2_L8	0	0	0	ICGC_GBM32_L5	0	0	0	ICGC_GBM33_L6	0	0	0	ICGC_GBM34_L7	0	0	0	ICGC_GBM36_L8	0	0	0	ICGC_GBM38_L1	0	0	0	ICGC_GBM39_L2	0	0	0	ICGC_GBM40_L4	0	0	0	ICGC_GBM41_L3	0	0	0	ICGC_GBM49_L6	2	5	38	ICGC_GBM4_L8	0	0	0	ICGC_GBM50_L1	0	0	0	ICGC_GBM52_L3	0	0	0	ICGC_GBM53_L4	0	0	0	ICGC_GBM56_L7	0	0	0	ICGC_GBM57_L6	0	0	0	ICGC_GBM58_L7	0	0	0	ICGC_GBM59_L8	2	5	38	ICGC_GBM5_L6	0	0	0	ICGC_GBM5_L7	0	0	0	ICGC_GBM60_L1	2	4	38	ICGC_GBM62_L1	0	0	0	ICGC_GBM63_L8	0	0	0	ICGC_GBM65_L1	0	0	0	ICGC_GBM66_L2	0	0	0	ICGC_GBM67_L3	0	0	0	ICGC_GBM6_L7	0	0	0	ICGC_GBM6_L8	0	0	0	ICGC_GBM73_L4	0	0	0	ICGC_GBM7_L5	0	0	0	ICGC_GBM7_L8	3	6	39
test@test$
```
here you see a lot of columns: for each sample a 0 is set where no reads of this circ have been found. also, with this file you could filter circs based on reads quality (strict find_circ 40x40 for example) however, this is not easy to work with, so we use matrixtwo

- third, a voted/appr/ .mat2 outfile (matrixmaker-V4.pl -> matrixtwo_V4.pl -> auto_vote.R ):
```bash   
test@test$ head hg19_myc_rnase_ordered_dcc_approved_by_all_three.mat2
"","biom_desc","circn","coordinates","gene","hallm","mm9_circ","prob_coding","refseqid","spliced_length","strand","trimmed.A204_S3__","trimmed.C_MB02_S1__","trimmed.C_MB04_S2__","trimmed.C_MB05_S3__","trimmed.C_MB07_S4__","trimmed.C_MB08_S1__","trimmed.C_MB10_S2__","trimmed.C_MB11_S3__","trimmed.C_MB12_S4__","trimmed.C_MB13_S1__","trimmed.C_MB15_S2__","trimmed.C_MB16_S3__","trimmed.C_MB17_S4__","trimmed.G401_S4__","trimmed.GABE_01_S1__","trimmed.GABE_02_S2__","trimmed.GABE_04_S4__","trimmed.RICK_05_S1__","trimmed.RICK_06_S2__","trimmed.RICK_11_S3__","trimmed.RICK_12_S4__","X"
"26811","amylo-alpha-1__6-glucosidase__4-alpha-glucanotransferase_","unknown","chr1:100340242-100350259","AGL","_MYOGENESIS","none","nA","NM_000642","nA","-",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,NA
"260","major_facilitator_superfamily_domain_containing_14A_","hsa_circ_0000096","chr1:100515464-100535241","MFSD14A","_KRAS_SIGNALING_UP","none","0.947063128105728","NM_033055","nA","+",4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NA
"10700","solute_carrier_family_30_member_7_","hsa_circ_0000098","chr1:101372407-101387397","SLC30A7","_BILE_ACID_METABOLISM","none","0.836970084843078","NM_001144884","nA","+",0,0,0,0,3,0,3,3,0,0,0,0,7,0,0,0,0,0,0,0,0,NA
test@test$
```
here the same information (except quality) is much more dense, with additional information from mapping files, the first column, "" is the row number, depending on your usage you can discard that column. The column "X" at the end can be discarded aswell

- next, a normalized (to junction reads per million, JPM) .mat2 file (matrixmaker-V4.pl -> matrixtwo_V4.pl -> auto_vote.R -> norm_a_voted_circs_df.R):

```bash   
test@test$ head normalized_hg19_myc_rnase_ordered_dcc_approved_by_all_three.csv
"","biom_desc","circn","coordinates","gene","hallm","mm9_circ","prob_coding","refseqid","spliced_length","strand","tr_.AGR_037_S1_L_001","tr_.ATRT_310_S2_L_001","tr_.ATRT_311_FHTC_S3_L_001","tr_.C_MB01_S4_L_001","tr_.C_MB18_S1__","tr_.C_MB19_S2__","tr_.C_MB20_S3__","tr_.C_MB22_S4__","tr_.JMU_RTK_2_S1_L_001","tr_.MB18_02_S2_L_001","tr_.MB18_03_S3_L_001","tr_.MB18_04_S4_L_001","tr_.MB18_13_S3__","tr_.MB18_14_S4__","tr_.MT161_S1__","tr_.MT229_S2__","tr_.MT314_S3__","tr_.MT435_S4__","tr_.PAND_01_S1__","tr_.PAND_02_S2__","tr_.PAND_04_S3__","tr_.PAND_05_S4__","tr_.PAND_07_S1__","tr_.PAND_08_S2__","tr_.RICK_01_S3__","tr_.RICK_02_S4__","tr_.RICK_03_S1__","tr_.RICK_04_S2__","tr_.RICK_07_S3__","tr_.RICK_08_S4__","tr_.RICK_09_S1__","tr_.RICK_10_S2__"
"chr1:100459092-100464971","solute_carrier_family_35_member_A3_","hsa_circ_0013298","chr1:100459092-100464971","SLC35A3","_GLYCOLYSIS","none","0.304181840132453","NM_001271684","nA","-",0,0,0,0,0,0,0,0,0.0412473971861191,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
"chr1:100459092-100480976","solute_carrier_family_35_member_A3_","unknown","chr1:100459092-100480976","SLC35A3","_GLYCOLYSIS","none","nA","NM_001271684","nA","-",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.0303185874133536,0,0
"chr1:100515464-100535241","major_facilitator_superfamily_domain_containing_14A_","hsa_circ_0000096","chr1:100515464-100535241","MFSD14A","_UV_RESPONSE_DN","none","0.947063128105728","NM_033055","nA","-",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.0252908453538403,0,0,0,0.0591878270053457,0.0606371748267073,0,0
"chr1:100602437-100609699","tRNA_methyltransferase_13_homolog_","unknown","chr1:100602437-100609699","TRMT13","_APOPTOSIS","none","nA","NM_019083","nA","-",0.0394569638029442,0,0,0,0,0,0.0231352052860798,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.0236751308021383,0,0,0
test@test$
```
basically the same as above, but now the circs quantifications are comparable between datasets- the reads are used to normalize the data

# 13. additional credits <a name="end"></a>
- thanks to Daniel Hein for the logo
- of course credits to the original authors of the here utilized pipelines and data sources (circBank,circbase,DCC,find_circ,CIRCexplorer1/2,UCSC,ENCODE,ensembl and many more)
- contributions are always welcome
