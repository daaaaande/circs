#/usr/bin/perl -w
use strict;
# change into the parent dir - this is where the infile needs to be
chdir "../f_c_out/"; # the find_circ output dir here
#######################################################
# usage: get samples.csv into find_circ/
#					 go to find_circ/auto_find_circ/
#						perl auto_automaker.pl samples.csv dirname
#######################################################
#auto_automaker.pl for find_circ
# 		- needs a inputfile as specified in the README.md
#			- will start find_circ_auto.pl for every sample
#			- can in return be started by the godfather.pl script, this will handle the infile location correctly
#			- makes a group into a dir of the parent dir where the bed.csv files for each group will be collected
#			- then makes the two matrices for each group in the groupfolders
#			- also makes one dir run_startdate in the parent dir where data from all samples will be made into two big matrices
###############################################

open(ER,'>>',"../logfile_auto.log")||die "$!";		# global logfile

my$er_rm1=`rm auto.bam.*.bam`;# just deleting leftovers to be sure
my$er_rm2=`rm tmp_*.bam`;

print ER "errors removing auto_bams: $er_rm1\nerrors removing tmp_bams: $er_rm2\n";

my$inputfile=$ARGV[0];
chomp$inputfile;
open(IN,$inputfile)|| die "$!";	# infile is a .csv file steptwo output.csv
my@lines=<IN>;
my$error="";# collecting dump
my@groups=();
my$errortwo="";

my$ndir=$ARGV[1];
chomp $ndir;
mkdir "$ndir";


foreach my $singleline (@lines){

	if($singleline =~ /[A-z]/gi){
		chomp $singleline;
		my@lineparts=split(/\s+/,$singleline);
		my$fileone=$lineparts[0];
		my$filetwo=$lineparts[1];
		my$samplename=$lineparts[2];
		$samplename=~s/\-/_/g;# avoid - in samplenames
		my$groupname=$lineparts[3];
		## if no group is defined, put one default group there
		if($groupname eq ""){ # empty or only space-char
			$groupname="default_group_$ndir";
		}




		#####
		chomp $samplename;
		chomp $fileone;
		chomp $filetwo;
		my$tim=localtime();
		print ER "##############################################################\n";
		print ER "starting @ $tim \nfinding circs in sample $samplename with find_circ ...\n";


		$error=`perl ../auto_find_circ/find_circ_auto.pl $fileone $filetwo $samplename`;
		print ER "errors:\n$error\n\n";
		if($groupname=~/[A-z]/gi){
			if(!(grep(/$groupname/,@groups))){ # check if group already present
				mkdir $groupname;		# IF NOT, MAKE GROUPDIR
				push(@groups,$groupname);
			}
			$errortwo=`cp run_$samplename/auto_run_$samplename.sites.bed.csv $groupname/`;
		}

		print ER "errors auto_moving:\n$errortwo\n";
	}


}



foreach my $groupname (@groups){
	my$errseding=`sed -i '1d' $groupname/*.csv`; # will remove first line from steptwo output i.e headers
	my$errcat=`cat $groupname/*.csv >$groupname/allsites_bedgroup_$groupname.csv`;
	my$errmatxrix=`nice perl ../auto_find_circ/matrixmaker-V4.pl --i  $groupname/allsites_bedgroup_$groupname.csv --o $groupname/allcircs_matrixout.txt`;
	my$matrtmaker=`perl ../auto_find_circ/matrixtwo_V4.pl --i $groupname/allcircs_matrixout.txt --o $groupname/allc_matrixtwo.tsv`;
	print ER "errors making second matrix for $groupname/allsites_bedgroup_$groupname.csv :\n$matrtmaker\n";
	my$er_p=`cp $groupname/allsites_bedgroup_$groupname.csv $ndir/`;
	print ER "errors catting $groupname .csv files together:\n$errcat\ncopying: $er_p\n";
	print ER "errors making matrix for $groupname/allsites_bedgroup_$groupname.csv :\n$errmatxrix\n";
}
my$erralcat=`cat $ndir/*.csv >$ndir/$ndir.allbeds.find_circ.out`;
my$er_check_in=`perl ../auto_find_circ/matrix_infile_checker.pl $ndir/$ndir.allbeds.find_circ.out`;
print ER "checking the infile for find_circ matrix in $ndir ...\n$er_check_in \n";
my$erralm1=`nice perl ../auto_find_circ/matrixmaker-V4.pl --i $ndir/$ndir.allbeds.find_circ.out --o $ndir/allsamples_matrix.find_circ.mat1`;
my$err_mat2=`perl ../auto_find_circ/matrixtwo_V4.pl --i $ndir/allsamples_matrix.find_circ.mat1 --o $ndir/allsamples_m_heatmap.find_circ.mat2 --excl_cb 1`;

print ER "error making files in $ndir :\ncat:\t$erralcat\nmatrix 1 creation:\t$erralm1 \nmatrix 2 creation:\n$err_mat2\n";


print ER "finished with all groups\n";
