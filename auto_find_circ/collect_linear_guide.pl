#!/usr/bin/perl -w
use strict;
# run after run_post_guide.pl has finished its jobs
# can choose all, will detect if no file is found
# will create foler with all needed output files wanted
print "hello, are you in the input directory of the run /gpfs/project/daric102/circs_hilbert_scratchgs/infiles___/. ? (y|n)\n";
my$in_infile_d=<STDIN>;

if($in_infile_d=~/n/ig){die "not in the input file directory!";}
my$create_collect_dir_here=`pwd`;
chomp $create_collect_dir_here;
print "assuming $create_collect_dir_here as startdir, will create all output dirs there\n";
# ask what dirs to create: linear, linear SJ, linear DCC and circs?
print "\nokay, now choose what you want to collect: (combine letters to collect multiple)\n
circs raw mat1+2 outfiles (default output circRNA quantifications)                => c\n
linearCounts DCC output (linear RNA counterparts detected by DCC with --linear 1) => l\n
linearSJ STAR output (linear splice junctions quantifications)                    => j\n
ReadsPerGene STAR output (transcript quantifications)                             => r\n###############################################################################\n";

my$things_todo=<STDIN>;

# create an output dir in the dir we are atm- the infiles_ dir
print "\nare you on an intercative job and already execute module load bedtools? this is needed for all parts! (y|n)\n";

my$ready=<STDIN>;
if($ready=~/n/ig){
  die "\n\nan interactive session is needed to collect all data\nplease retry\n";

}

# find out output dirs for circs circRNA matrices with
#[daric102@hpc-login7 circs]$ ls -dth dcc_out/* | head -n 12 | grep -v run_
#dcc_out/csf_biops
#dcc_out/test_lin_1
#[daric102@hpc-login7 circs]$ pwd
#/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs

#
#
#
#

# ask for script dir? - maybe as a todo later



my$last_out_dirs_hg19=`ls -dtk /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_out/* | grep -v run_ | head -n 7 `;
my$last_out_dirs_hg38=`ls -dtk /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_2_out/* | grep -v run_ | head -n 7 `;


print "last outfiles circ directories (hg19 DCC):\n$last_out_dirs_hg19\n";
print "last outfiles circ directories (hg38 DCC):\n$last_out_dirs_hg38\n";
print "\n\ndo you want to collect hg19 or hg38 results? (hg19|hg38)\n";
my$hg_mode=<STDIN>;
chomp $hg_mode;
my$create_dir=`mkdir $create_collect_dir_here/output.$hg_mode`;
# create there the dirs where linear/SJ, linearcounts will be collected, mat2 and 1 will be created. final final outputs then land in the Create_dir/.

my$outdir_circs="";

if($things_todo=~/c/ig){
  # ask for run run name
  print "what is the run name? repo/circs/*_out/___/. \n(do not attach / or ..  into the dir name)\n";
  # ask once for hg38/19/both to apply it to all - if too many are chose, worst case in an empty sub-dir in results
  # here we get the dirs of the circRNA matrices, hg38 and hg19 - we just need to copy them over
  $outdir_circs=<STDIN>;
  chomp $outdir_circs;

  my$circs_mat_list=`wc -l /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/*out/$outdir_circs/*.mat*`;
  print "will copy the following files over to result dir:\n(and number of lines info) \n$circs_mat_list\nok? (y|n)\n";
  my$agree_cp=<STDIN>;

  # ask for hg38/19/both
  if($agree_cp=~/y/ig){
    my$transfer_mat_circs=`cp /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/*out/$outdir_circs/*.mat2 $create_collect_dir_here/output.$hg_mode/`;
    print "done copying circRNA result matrix files into result dir:$transfer_mat_circs\n";
  }
  # list files, ask if correct, if 0 print error state
  # then copy the files, give mini stats (wc-l *mat* after the copy)

}


if($things_todo=~/l/ig){
  my$create_lin_star_dir2=`mkdir $create_collect_dir_here/output.$hg_mode/dcc_linear_counts`;
  print "created dir to collect linear transcript counts: $create_lin_star_dir2\n";
  # get all files to be collected
  my$where_to_look2="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_out";
  if($hg_mode=~/hg38/){
    $where_to_look2="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_2_out";
  }
  my$all_lincounts_files2=`wc -l $where_to_look2/run_*/*LinearCount`;
  print "will copy over and create matrix with these files:\n$all_lincounts_files2\nok? (y|n)\n";
  my$do_cp=<STDIN>;
  if($do_cp=~/y/ig){
    my$list_of_full_filenames=`ls -1f $where_to_look2/run_*/*LinearCount`;
    my@all_files_to_tr=split(/\n/,$list_of_full_filenames);
    # rename file while copying- need the sample name in the filename itself
    # will be someth8ing like run_try10_SRR3184285_1/LinearCount
    foreach my$single_file(@all_files_to_tr){
      # cleanup file name
      chomp $single_file;
      my$new_de=$single_file;

      $new_de=~s/\/gpfs\/project\/daric102\/circs_hilbert_scratchgs\/repo\/circs\/dcc_out//gi;
      $new_de=~s/\/gpfs\/project\/daric102\/circs_hilbert_scratchgs\/repo\/circs\/dcc_2_out//gi;
      $new_de=~s/\///gi;

      # when copying, need to attach new dir in front, should be cleaned by this point
      my$copy_files_rename=`cp $single_file $create_collect_dir_here/output.$hg_mode/dcc_linear_counts/$new_de`;

      # regex into nrew file
      # move
    }
    print "renamed and copied all LinearCount files, now creating a matrixmaker-type infile ...\n ";

    #according to hg38/19, we need to set the annotation file here accordingly:
    my$annot_file="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/hg38_ucsc_refseq_all.bed";
    if($hg_mode=~/19/){
      $annot_file="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/Genes_RefSeq_hg19_09.20.2013.bed";
    }

#    my$prep_anot=`module load bedtools`;
  #  print "loaded bedtools for annotation:\n$prep_anot\n";
                                    # populate all options here
    my$dcc_lin_collect_exec=`perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/automate_DCC/dcc_linear_counts_collector.pl --m /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/refseq_to_gene_names_hg38.tsv --o $create_collect_dir_here/output.$hg_mode/dcc_linear_counts/dcc_lincounts_matrix_infile.tsv --an $create_collect_dir_here/output.$hg_mode/dcc_linear_counts --bed $annot_file --d $create_collect_dir_here/output.$hg_mode/dcc_linear_counts`;
#run_try10_SRR3184300_1/LinearCount
    print " created matrixmaker infile for $hg_mode: $create_collect_dir_here/output.$hg_mode/dcc_linear_counts/dcc_lincounts_matrix_infile.tsv\nnow creating mat1 file, this can take a while ...\n ";




    # mat1/2: hg19/38 is not relevant here_ mapping will be wrong but we are looking at linear, so refseqid and gene are relevant, the rest is useless and can be discarded
    # maybe a todo for later: cleanup all extra info in dcc linear mat2 thats not correct/needed
    my$exec_of_matv4mak=`perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/matrixmaker-V4.pl --i $create_collect_dir_here/output.$hg_mode/dcc_linear_counts/dcc_lincounts_matrix_infile.tsv --o $create_collect_dir_here/output.$hg_mode/dcc_linear_counts/dcc_lincounts_matrix_1.mat1`;
    print "created mat1: $exec_of_matv4mak\nmaking matrix2 with dcc linear data...\n";
    my$exec_mat2_dc_l=`perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/matrixtwo_V4.pl --i $create_collect_dir_here/output.$hg_mode/dcc_linear_counts/dcc_lincounts_matrix_1.mat1 --o $create_collect_dir_here/output.$hg_mode/dcc_lincounts_matrix_2.mat2`;
    print "created .mat2 file with linear DCC quants: $exec_mat2_dc_l\n output file is: $create_collect_dir_here/output.$hg_mode/dcc_lincounts_matrix_2.mat2\n ";


  }
  # ask if hg19/38 or both
  # find all and copy into distinct folder (to be created)
# need to rename the files- the filename needs to include the samplename
# convert into matrixmaker ready file
# convert into matrixmaker ready file with dcc_linear_counts_collector.pl
# and make the matrixone, two
}

if($things_todo=~/j/ig){
  #ask for hg38/19/both
  # find all files
  # create sub-dir
  # run linear_sj_collector.pl
  # test this later





  # make mat1
  # make mat2

}
if($things_todo=~/r/ig){
  # ask for hg19/38/$both
  # create subdir (s) - one for each genome
  #
  my$create_lin_star_dir=`mkdir $create_collect_dir_here/output.$hg_mode/reads_per_gene_star`;
  print "created dir to collect linear transcript counts: $create_lin_star_dir\n";
  # get all files to be collected
  my$where_to_look="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_out";
  my$anot_bed="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/refseq_to_gene_names_hg38.tsv";
  if($hg_mode=~/hg38/){
    $where_to_look="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_2_out";
  }
  my$all_lincounts_files=`wc -l $where_to_look/run_*/*ReadsPerGene.out.tab`;
  print "will copy over and create matrix with these files:\n$all_lincounts_files\nok? (y|n)\n";
  my$do_cp=<STDIN>;
  if($do_cp=~/y/ig){
    my$copy_files=`cp $where_to_look/run_*/*ReadsPerGene.out.tab $create_collect_dir_here/output.$hg_mode/reads_per_gene_star/.`;
    print "copied files over:$copy_files\ņ";
    my$create_star_lin_mat=`perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/automate_DCC/linear_collector.pl --d $create_collect_dir_here/output.$hg_mode/reads_per_gene_star --o $create_collect_dir_here/output.$hg_mode/reads_per_gene_star/$outdir_circs.$hg_mode.star_linear_reads_per_gene.tsv --an /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ --m $anot_bed`;
    # run the collector
    print "created readsper_gene file $create_collect_dir_here/output.$hg_mode/reads_per_gene_star/$outdir_circs.$hg_mode.star_linear_reads_per_gene.tsv:\n$create_star_lin_mat  \n";
    # copy final output into ../.
    my$copy_into_result_dir=`cp $create_collect_dir_here/output.$hg_mode/reads_per_gene_star/$outdir_circs.$hg_mode.star_linear_reads_per_gene.tsv $create_collect_dir_here/output.$hg_mode/$outdir_circs.$hg_mode.star_linear_reads_per_gene.tsv`;
    print "copied resulting matrix into $create_collect_dir_here/output.$hg_mode/ . \n";
  }
   #
  # let these be confirmed
  # then copy over
  # then rn linear_collector.pl

}


# if both is chose, re-run all with the hg38 again



# module load bedtools only if needed
#l
#j
#r
# c
