#/usr/bin/perl -w
use strict;
# updated version of find_circ , automated into one script
# started in repo/circs/*/.
chdir "../f_c2_out/";

open(ER,'>>',"../logfile_auto.log")||die "$!";		# global logfile

# file directories- you will need to adapt those
my$out_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/f_c2_out";
my$scrips_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines";
my$bowtie_index_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full"; # bowtie2 index build dir, refernce out called "hg38".
my$infile_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs";
my$fastqs_files="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/all_chroms/";
# /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/all_chr
# infiles + samplename
my$lineonefile= $ARGV[0];
chomp $lineonefile;

my$linetwofile= $ARGV[1];
chomp $linetwofile;

# samplename +run_ will be folder name for sample :f_c_out/run_samplename/- where all in-between files will be
my$outfn=$ARGV[2];
chomp $outfn;

my$all_out_dir=$ARGV[3]; # directory where this script should put the final outfile into, from all infiles
chomp $all_out_dir;

my$full_out_dir="$out_dir/$all_out_dir";

# output dir
my$dirn="run_$outfn";

my$full_sample_dir="$out_dir/$dirn";
mkdir $full_sample_dir;
# and go there
chdir $full_sample_dir;

my $start = time;
# Do stuff
print ER "started at $start with find_circ updated sample $outfn ########################\n";
# first alignment with bowtie                                                                       # .fastqs are expected in repo/circs/.
#my$er_bt=`bowtie2 -p 12 --very-sensitive --mm --score-min=C,-15,0 -x $bowtie_index_dir/hg38 -1 $infile_dir/$lineonefile -2 $infile_dir/$linetwofile  >$full_sample_dir/temp.sam 2> $full_sample_dir/firstpass.log`;
my$er_bt=`bowtie2 -p 12 --very-sensitive --mm --score-min=C,-15,0 -x $bowtie_index_dir/hg38 -1 $infile_dir/$lineonefile -2 $infile_dir/$linetwofile 2> $full_sample_dir/firstpass.log | samtools view -hbuS -| samtools sort - auto`;

print ER "bowtie errors: $er_bt\n";

my$err_10=`samtools view -@ 10 -hf 4 $full_sample_dir/auto.bam | samtools view -@ 10 -Sb - > $full_sample_dir/unmapped_auto.bam`;
#samtools view -@ 10 -hf 4 $sample_dir/auto.bam | samtools view -@ 10 -Sb - > $sample_dir/unmapped_auto.bam
print ER "errors creating $full_sample_dir/unmapped_auto.bam:$err_10 \n";


# first find_circ script
print ER "splitting into $full_sample_dir/anchors...\n";
my$err5 = `python2.7 $scrips_dir/unmapped2anchors.py $full_sample_dir/unmapped_auto.bam > $full_sample_dir/auto_anchors.qfa`;
print ER "errors:$err5\n";

# anchor re-alignment
print ER "creating $dirn/auto_$dirn.sites.reads \tand  $dirn/auto_$dirn.sites.bed\n";                                                                                                                                                  # need to find these
my$err7 = `bowtie2 --reorder --mm -p 12 --score-min=C,-15,0 -q -x $bowtie_index_dir/hg38 -U $full_sample_dir/auto_anchors.qfa 2> $full_sample_dir/auto_bt2_secondpass.log  > $full_sample_dir/bowtie2_reorder.out`;

print ER "errors re-aligning into $full_sample_dir/bowtie2_reorder.out:$err7\n";
my$er_78=`cat $full_sample_dir/bowtie2_reorder.out | python2.7 $scrips_dir/find_circ.py -G $fastqs_files/ -p auto_$dirn -s $full_sample_dir/auto_$dirn.log >$full_sample_dir/auto_$dirn.sites.bed 2>$full_sample_dir/auto_$dirn.sites.read`;
print ER "errors executing find_circ for  $full_sample_dir:$er_78\n";



# steptwo.pl steps- first filtering for not unique, unambigious, unique anchor, max circ length
print ER "creating circ_candidates.bed with score filtering...\n";#				# trial fixinhg awk issue with {}
#my$5="$5";# maybe that works?
#my$err8 = system("grep CIRCULAR  $full_sample_dir/auto_$dirn.sites.bed | grep -v chrM | awk '{\$5>=2}' | grep UNAMBIGUOUS_BP | grep ANCHOR_UNIQUE | python2.7 $scrips_dir/maxlength.py 100000 >$full_sample_dir/$dirn.circ_candidates.bed");
my$err8 = `grep circ  $full_sample_dir/auto_$dirn.sites.bed | grep -v chrM | python2.7 $scrips_dir/sum.py -2,3 | python2.7 $scrips_dir/scorethresh.py -16 1 | python2.7 $scrips_dir/scorethresh.py -15 2 | python2.7 $scrips_dir/scorethresh.py -14 2 | python2.7 $scrips_dir/scorethresh.py 7 2 | python2.7 $scrips_dir/scorethresh.py 8,9 35 | python2.7 $scrips_dir/scorethresh.py -17 100000 >$full_sample_dir/$dirn.circ_candidates.bed`;
# outfile now now $full_sample_dir/auto_$dirn_circ_candidates.bed
print ER "errors creating $full_sample_dir/$dirn.circ_candidates.bed :$err8\n";

# annotation to refseqid
print ER "annotating $full_sample_dir/$dirn.circ_candidates.bed with $bowtie_index_dir/refseq_genes_hg38.bed...\n";
my$err9 = `bedtools window -a $full_sample_dir/$dirn.circ_candidates.bed -b  $bowtie_index_dir/chr_refseq_genes_hg38.bed >$full_sample_dir/auto_$dirn.sites_annotated.bed`;
print ER "errors creating $full_sample_dir/auto_$dirn.sites_annotated.bed :$err9\n";

open(IN,"$full_sample_dir/auto_$dirn.sites_annotated.bed")|| die "$!";
my@infile = <IN> ;

# new filename for steptwo output ;
# is now  $linfile.circ_candidates_auto_.bed.out_.processsed

my$out_parse_one_file= "$full_sample_dir/auto_$dirn.sites_annotated_parsed.bed";
#############

print ER "adding unique coordinates\ncreating $out_parse_one_file ...\n";
# output file second argument adding coordinates
open(OUT,">",$out_parse_one_file)|| die "$!";

foreach my $line (@infile){
	#print "$line";
	my@parts=split(/\t+/,$line);	# split line to find coordinates of gene
	my$chr=$parts[0];
	my$beg=$parts[1];
	my$end=$parts[2];
	my$un="$chr:$beg-$end";		# all coordinades together are the unique id here
	chomp $un;
	my$newline="$un\t$line";
	print OUT "$newline";
}

#############

# so this resulted in $full_sample_dir/auto_$dirn.sites_annotated_parsed.bed
# sorting
my$errso=`sort -k 1,1 $out_parse_one_file  >$out_parse_one_file.sorted`;
print ER "sorting $out_parse_one_file by coordinates...\nerrors creating $out_parse_one_file.sorted: $errso\n";

#############
# parsing of sorted parsed files- ending into $outfilethre
my$outfilethre="$full_sample_dir/$outfn.tsv";


# outfile for finding relevant columns...
print ER "creating $outfilethre...\n";
open(ND,">",$outfilethre)|| die "$!";
print ND "coordinates\tstrand\tsampleid\tunique_counts\tqualA\tqualB\tRefSeqID\n";

open(SO,"$out_parse_one_file.sorted")||die "$!";
# edit this file
my@newin = <SO>;

foreach my $lein (@newin){
	chomp $lein;
	my@all_things=split(/\s+/,$lein);
	# NOW GET ONLY RELEVANT THINGS
	my$ccord=$all_things[0]; # should be chr10:101654702-101656154
	my$long_id=$all_things[4]; # should be auto_circ_004447
	$long_id=~s/auto_run_//;
	## this string needs some work : remove circ_..
	$long_id =~s/circ\_*.[0-9]{1,20}//ig ;
	# removed circ_8945 for each line

	my$strand=$all_things[6];
	my$uniques=$all_things[7];
	my$bestqa=$all_things[8];
	my$bestqb=$all_things[9];
	my$refseqid=$all_things[22];

 	if(!($ccord=~/chrUn_gl/)){ # check for unsure coordinates
		if($refseqid=~/N/){
			print ND "$ccord\t$strand\t$long_id\t$uniques\t$bestqa\t$bestqb\t$refseqid\n";
		}

	}
}

#############
# final file is $full_sample_dir/$outfn.csv, need to put it into repo/circs/f_c_out/$all_out_dir/.
my$er_cp=`cp $full_sample_dir/$outfn.tsv $full_out_dir/`;
print ER "errors copy outfile to $full_out_dir: $er_cp \n";

my $duration = ((time - $start)/60);
print ER "Execution time find_circ upd $dirn: $duration minutes\ncreated $outfilethre\n";
print ER "############################################################\ndone.\n";
1;
