#/usr/bin/perl -w
use strict;
use Getopt::Long qw(GetOptions);
# perl find_circ_automation.pl --B base_dir/ --S scripts_dir/ --l path/to/logfile.log --t 24 --pld /path/to/parser_script/ --i "fastq1.fq  fastq2.fq  samplename  outputdir" --M pe --dry 0 --i_dir /path/to/fastq_files/ --f_q /path/to/reference/fastq/files/ --ref /refernce/genome_bowtie2/hgXX --anot /path/to/annotation/refernce/file.bed
# find_circ pipeline for one sample
# perl find_circ_autmation.pl --i  "$infileline"
# perl find_circ_autmation.pl --i "$infileline" --out /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/f_c2_out --f_q /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/all_chroms --ref /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38 --anot /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/hg38_ucsc_refseq_all.bed

#### input parameters ####
my $start = time;
# file directories- you will need to adapt those with the options given
my$base_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs";
my$out_dir="$base_dir/f_c_out";
my$scripts_dir="$base_dir/pipelines"; #dir for find_circ .py
my$pl_scripts="$base_dir/auto_find_circ";# dir where the parser script should be
my$infile_dir=$base_dir;
my$fastqs_files="$base_dir/pipelines/genome/chroms";# can also use /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/all_chroms/
# other parameters
my$refseq_file="$scripts_dir/Genes_RefSeq_hg19_09.20.2013.bed";# can also use /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/chr_refseq_genes_hg38.bed
my$bt_ref="$scripts_dir/hg19";# can also use /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38
my$input_line="A_1.fastq      A_2.fastq   A  output_fc";
my$mode="pe"; # "pe"- paired end, 2 fastq files per sample ;"se"- single end, one fastq file per sample (no linetwo file needed)
my$dryRun=0; # enter a 1 here to only have the used command printed out, and not executed (good for testing)
my$err_file="../logfile_auto.log"; # logfile
my$threads=12;
my$delete="none";# options are "none" for no deletion of in-between files, "big" for only big files
my$strict=0;
# retrieving non-default parameters if given- for --i "fastq.fastq (fastq2.fastq) samplename"
GetOptions('base_dir|B=s' => \$base_dir,'out_dir|out=s' =>\$out_dir,'scripts_dir|S=s' => \$scripts_dir,'logfile|l=s' => \$err_file,'threads|t=i' => \$threads,'parser_dir|pld=s'=>\$pl_scripts,'input_line|i=s' => \$input_line,'mode|M=s' => \$mode,'dry_run|dry=i' => \$dryRun,'infile_dir|i_dir=s' => \$infile_dir,'fastq_files|f_q=s' => \$fastqs_files,'bt_ref|ref=s' => \$bt_ref,'annotation_file|anot=s' =>\$refseq_file,'delete|del=s'=>\$delete,'strict|str=i'=>\$strict) or warn "Using default parameters now\n";
# 13 parameters
chomp($base_dir,$out_dir,$scripts_dir,$err_file,$threads,$input_line,$mode,$dryRun,$infile_dir,$fastqs_files,$bt_ref,$refseq_file,$pl_scripts,$strict);
chdir $out_dir;
# for debugging and logging: dump final params
# for input line: hand parts into final params
my@split_input_line=split(/\s+/,$input_line);
my$lineonefile=$split_input_line[0];
my$linetwofile=$split_input_line[1];
my$sample_name=$split_input_line[2];
my$outfiles_dir=$split_input_line[3];

if($mode eq "pe" && ($outfiles_dir eq "")){# 4th element is only whitespace
  print("set to paired-end mode but found only 3 columns in infile: switching to single end mode\n");
  $mode="se";
}

# single end mode = only one fastq file
if($mode eq "se"){
      $lineonefile=$split_input_line[0];
      $sample_name=$split_input_line[1];
      $outfiles_dir=$split_input_line[2];
      $linetwofile="none";# this var should then not be needed anymore

}
# creating full file paths for later
my$full_out_dir="$out_dir/$outfiles_dir";
my$sample_dir_out="$out_dir/run_$sample_name";# /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/f_c2_out/run_A/. <- where all files belonging to sample A will be saved
mkdir "$sample_dir_out";
chdir "$sample_dir_out";
#### start of execution ####
open(ER,'>>',$err_file)||die "$!";
print  ER "started find_circ $bt_ref run with parameters:\nbase_dir=$base_dir\nthreadsN=$threads\ninfile1=$lineonefile\tinfile2=$linetwofile\tmode=$mode\ndryRun=$dryRun\nsamplename=$sample_name\nout_dir_sample=$sample_dir_out\nreference_files_in_(bt2index)=$scripts_dir\nfastq_reference_in=$fastqs_files\nwill_copy_final_output_into=$full_out_dir\nfind_circ_scripts_are_in=$scripts_dir\nref=$bt_ref\nannotation_file=$refseq_file\nparser_dir=$pl_scripts\ndeleting=$delete\n";
# start of the actual process
if($dryRun){
      # just print all commands
      print"\n\ndry run mode. commands:\n";
      if($mode eq "pe"){

            print "bowtie2 -p $threads --very-sensitive --mm --score-min=C,-15,0 -x $bt_ref -1 $infile_dir/$lineonefile -2 $infile_dir/$linetwofile 2> $sample_dir_out/firstpass.log >$sample_dir_out/temp.sam\n" ;
      }
      elsif($mode eq "se"){
            print "bowtie2 -p $threads --very-sensitive --mm --score-min=C,-15,0 -x $bt_ref -q $infile_dir/$lineonefile >$sample_dir_out/temp.sam 2> $sample_dir_out/firstpass.log\n";
      }
      print "samtools view -@ $threads -hbuS -o $sample_dir_out/temp.bam $sample_dir_out/temp.sam\n";
      print "samtools sort -@ $threads -O bam -o $sample_dir_out/$sample_name.auto.bam $sample_dir_out/temp.bam\n";
      print "samtools view -@ $threads -hf 4 $sample_dir_out/$sample_name.auto.bam | samtools view -@ $threads -Sb - > $sample_dir_out/unmapped_auto.bam\n";
      print "python2.7 $scripts_dir/unmapped2anchors.py $sample_dir_out/unmapped_auto.bam > $sample_dir_out/auto_anchors.qfa\n";
      print "bowtie2 --reorder --mm --score-min=C,-15,0 -q -x $bt_ref -U $sample_dir_out/auto_anchors.qfa 2> $sample_dir_out/auto_bt2_secondpass.log | python2.7 $scripts_dir/find_circ.py -G $fastqs_files/ -p $sample_name -s $sample_dir_out/run_$sample_name.sites.log > $sample_dir_out/run_$sample_name.sites.bed 2> $sample_dir_out/run_$sample_name.sites.reads\n";
      print "grep circ $sample_dir_out/run_$sample_name.sites.bed | grep -v chrM | python2.7 $scripts_dir/sum.py -2,3 | python2.7 $scripts_dir/scorethresh.py -16 1 | python2.7 $scripts_dir/scorethresh.py -15 2 | python2.7 $scripts_dir/scorethresh.py -14 2 | python2.7 $scripts_dir/scorethresh.py 7 2 | python2.7 $scripts_dir/scorethresh.py 8,9 35 | python2.7 $scripts_dir/scorethresh.py -17 100000 >$sample_dir_out/run_$sample_name.circ_candidates_auto.bed\n";
      print "bedtools window -a $sample_dir_out/run_$sample_name.circ_candidates_auto.bed -b $refseq_file -w 1 >$sample_dir_out/run_$sample_name.circ_candidates_auto_bed.annotated\n";
      print "final file would be getting parsed:$sample_dir_out/run_$sample_name.circ_candidates_auto_bed.annotated \n";
      print "perl $pl_scripts/f_c_outreader.pl --i $sample_dir_out/run_$sample_name.circ_candidates_auto_bed.annotated --o $sample_dir_out/run_$sample_name.tsv --strict $strict\n";
      print "cp $sample_dir_out/run_$sample_name.tsv $full_out_dir/\n\n\nfinished.";
}
else{
      print ER "started at $start with find_circ updated sample $sample_name ########################\n";
      # alignment- > unmapped prep
      if($mode eq "pe"){
            my$er_bt=`bowtie2 -p $threads --very-sensitive --mm --score-min=C,-15,0 -x $bt_ref -1 $infile_dir/$lineonefile -2 $infile_dir/$linetwofile 2> $sample_dir_out/firstpass.log >$sample_dir_out/temp.sam`;
            print ER "bowtie2 first alignment paired end:$er_bt\n ";
      }

      elsif($mode eq "se"){
            # single read
            print ER "doing currently:\nbowtie2 -p $threads --very-sensitive --mm --score-min=C,-15,0 -x $bt_ref -q $infile_dir/$lineonefile >$sample_dir_out/temp.sam 2>$sample_dir_out/firstpass.log\n";
            my$err = `bowtie2 -p $threads --very-sensitive --mm --score-min=C,-15,0 -x $bt_ref -q $infile_dir/$lineonefile >$sample_dir_out/temp.sam 2> $sample_dir_out/firstpass.log`;
            print ER "bowtie2 first alignment single end:$err\n ";
      }

      # rest of the pipeline -

      my$err2 = `samtools view -@ $threads -hbuS -o $sample_dir_out/temp.bam $sample_dir_out/temp.sam`;
      print ER "creating temp.bam errors:$err2\n";
      # newest samtools sort: O option was removed
      my$err3 = `samtools sort -@ $threads -O bam -o $sample_dir_out/$sample_name.auto.bam $sample_dir_out/temp.bam`;
      print ER "sorting temp.bam errors:$err3\n";

      my$err4 = `samtools view -@ $threads -hf 4 $sample_dir_out/$sample_name.auto.bam | samtools view -@ $threads -Sb - > $sample_dir_out/unmapped_auto.bam`;
      print ER "getting the unmapped errors:$err4\n";

      my$err5 = `python2.7 $scripts_dir/unmapped2anchors.py $sample_dir_out/unmapped_auto.bam > $sample_dir_out/auto_anchors.qfa`;
      print ER "splitting into anchors errors:\n$err5\n";

      my$err7 = `bowtie2 --reorder --mm --score-min=C,-15,0 -q -x $bt_ref -U $sample_dir_out/auto_anchors.qfa 2> $sample_dir_out/auto_bt2_secondpass.log | python2.7 $scripts_dir/find_circ.py -G $fastqs_files/ -p $sample_name -s $sample_dir_out/run_$sample_name.sites.log > $sample_dir_out/run_$sample_name.sites.bed 2> $sample_dir_out/run_$sample_name.sites.reads`;
      print ER "creating $sample_dir_out/run_$sample_name.sites.bed and $sample_dir_out/run_$sample_name.sites.reads errors:\n$err7\n";

      my$err8 = `grep circ $sample_dir_out/run_$sample_name.sites.bed | grep -v chrM | python2.7 $scripts_dir/sum.py -2,3 | python2.7 $scripts_dir/scorethresh.py -16 1 | python2.7 $scripts_dir/scorethresh.py -15 2 | python2.7 $scripts_dir/scorethresh.py -14 2 | python2.7 $scripts_dir/scorethresh.py 7 2 | python2.7 $scripts_dir/scorethresh.py 8,9 35 | python2.7 $scripts_dir/scorethresh.py -17 100000 >$sample_dir_out/run_$sample_name.circ_candidates_auto.bed`;
      print ER "creating $sample_dir_out/run_$sample_name.circ_candidates_auto.bed with score filtering errors:\n$err8\n";

      my$err9=`bedtools window -a $sample_dir_out/run_$sample_name.circ_candidates_auto.bed -b $refseq_file -w 1 >$sample_dir_out/run_$sample_name.circ_candidates_auto_bed.annotated`;
      print ER "looking up generefs with $refseq_file\ncreating $sample_dir_out/run_$sample_name.circ_candidates_auto_bed.annotated errors:\n$err9\n";

      my$er_parse=`perl $pl_scripts/f_c_outreader.pl --i $sample_dir_out/run_$sample_name.circ_candidates_auto_bed.annotated --o $sample_dir_out/run_$sample_name.tsv -strict $strict`;
      print ER "errors parsing output: $er_parse\n";

      my$er_cp=`cp $sample_dir_out/run_$sample_name.tsv $full_out_dir/`;
      print ER "errors copy outfile to $full_out_dir: $er_cp \n";


      # deleting big in-between files
      if($delete eq "big"){
        chdir "$sample_dir_out";
        `rm $sample_dir_out/*.bam $sample_dir_out/*.sam $sample_dir_out/*.qfa $sample_dir_out/*Unmapped.out* $sample_dir_out/*.junction $sample_dir_out/*.reads`;
      }
      elsif($delete eq "none"){
        chdir "$sample_dir_out";
        print ER "user chose to not delete any files\n";
      }
      else{
        print ER "could not recognize delete choice: use --delete none|big\n";
      }

      my $duration = ((time - $start)/60);
      print ER "Execution time find_circ $sample_name: $duration minutes\ncreated $sample_dir_out/run_$sample_name.tsv\n";
      print ER "############################################################\ndone.\n";

}
1;
