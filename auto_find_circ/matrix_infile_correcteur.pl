#!/usr/bin/perl -w
use strict;
#matrix_infile_corecteur.pl
# checks file handled to matrixmaker if there qare any missaligned columns
my$start = time;
my$linfile= $ARGV[0];
chomp $linfile;
#to fix double lines: read coords+strand into hash, only add if not present
my%unique_circs_hash=();
open(IN,$linfile)|| die "$!";
my@allelines= <IN>; #input file
my$mist=0;
for (my$i=0;$i<scalar(@allelines);$i++){
	my$line_o_o=$allelines[$i];	# current line
	my@parts=split(/\t+/,$line_o_o);
	my$cord=$parts[0];
	my$strand=$parts[1];
	my$Refseqid=$parts[6];
	my$namesmale=$parts[2];# sample
	my$part_3=$parts[3];
	my$part_4=$parts[4];
	my$part_5=$parts[5];
	#  my$part_7=$parts[7];
	# now checking the inputfile
	my$unique_info="$cord.$strand.$namesmale";# this should be unique for every circ with each sample
	if(!(exists($unique_circs_hash{$unique_info}))){# filling hash with only unique infos
	$unique_circs_hash{$unique_info}=$i;# value= lin number, just to fill it with something
		if(!($strand=~/[+-]/)){
			#  warn "line $i file $linfile: strand is $strand \n";
			$mist++;
		}
		if(!($cord=~/chr/)){
			#  warn "line $i file $linfile: coordinates $cord does not include chromosome! \n";
			$mist++;
		}
		if(!($Refseqid=~/N[MR]_[0-9]{3,11}/)){
			#warn "line $i file $linfile: Refseqid $Refseqid does not include refseqid! \n";
			$mist++;
		}
		# sample name corrections
		if($namesmale=~/\-/){
			#  warn "line $i file $linfile: samplename $namesmale does have a minus in it! \n";
			#$mist++;
			$namesmale=~s/\-/_/g;
		}
		$namesmale=~s/s\_\d{6,9}//g;
		$namesmale=~s/\d{6,9}//g;
		if($mist > 0){
			warn "line $i with mistaken parts will be omitted: $line_o_o\n";
		}
		else{
			# line is mistake-free
			print "$cord\t$strand\t$namesmale\t$part_3\t$part_4\t$part_5\t$Refseqid";
		}
		$mist=0;
	}
}
