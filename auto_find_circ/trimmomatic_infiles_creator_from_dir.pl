#!/usr/bin/perl -w
use strict;
use Getopt::Long qw(GetOptions);

my$inputfile="all_gz.txt";# the  fastq(.gz) list file, includes all , including forward and reverse for each sample
my$mode="pez"; # modes= pe paired end (two files per sample) se single end (one file per sample) pez (paired end, .gz input files)
my$to_replace=""; #for paired end modes- first line identifier, + second lane idetifier:
my$replace_into="";
GetOptions('mode|m=s' => \$mode,'input|i=s' => \$inputfile,'to_replace|l1=s' => \$to_replace,'replace_into|l2=s' => \$replace_into) or warn "Using default parameters\n";

chomp $to_replace;
chomp $replace_into;
chomp $mode;
chomp $inputfile;
open(IN,$inputfile)|| die "$!"; # infile is a .csv file steptwo output.csv
my@lines=<IN>;
my$dir_f=`pwd`;
chomp $dir_f;
# TODO: make a guide-like execution flow?
foreach my $singleline (@lines){
      chomp $singleline;
      if($mode eq "pez"){
            if($singleline=~/$replace_into/){# looking for the second file first- the r! file is also there in above cases that are already covered
                  my$f_2=$singleline;# full file, ie Lena_01_L007_R2_001.fastq.gz
                  # first  file will be Lena_01_L007_R1_001.fastq.gz
                  my$f2_zwei=$f_2; # for de-gzing later
                  $f2_zwei=~s/\.gz//;
                  my$degz_file2=$`; # Lena_01_L007_R2_001.fastq - as ouput lane 2 fastq

                  $singleline =~s /$replace_into/ooo/;
                  $singleline=~s /ooo/$to_replace/;
                  my$first_fde=$singleline;# for de-gzing later
                  $first_fde=~s/\.gz//;

                  my$de_gz_file1=$`;# Lena_01_L007_R1_001.fastq
                  # print "checking for file $singleline\n";
                  my$there_test2= `ls $singleline`;
                  if($there_test2=~/\.fastq\.gz/){
 	                      # both files are there
                            print "$dir_f/$singleline\t$dir_f/$f_2\t$dir_f/trimmed.$first_fde\t$dir_f/$first_fde.rubbish\t$dir_f/trimmed.$f2_zwei\t$dir_f/$f2_zwei.rubbish\n";
                        #    $warn --;
                      }
                  else{
                        print "sample ommitted: $singleline\n";
                  }
            }
      }
      elsif($mode eq "pe"){
            if($singleline=~/$replace_into/){# looking for the second file first- the r! file is also there in above cases that are already covered
                  my$f_2=$singleline;# full file, ie Lena_01_L007_R2_001.fastq.gz
                  # first  file will be Lena_01_L007_R1_001.fastq.gz
                  my$f2_zwei=$f_2; # for de-gzing later
                  $f2_zwei=~s/\.fastq//;
                  my$degz_file2=$`; # Lena_01_L007_R2_001.fastq - as ouput lane 2 fastq

                  $singleline =~s /$replace_into/ooo/;
                  $singleline=~s /ooo/$to_replace/;
                  my$first_fde=$singleline;# for de-gzing later
                  $first_fde=~s/\.gz//;

                  my$de_gz_file1=$`;# Lena_01_L007_R1_001.fastq
                  # print "checking for file $singleline\n";
                  my$there_test2= `ls $singleline`;
                  if($there_test2=~/\.fastq/){
 	                      # both files are there
                            print "$dir_f/$singleline\t$dir_f/$f_2\t$dir_f/trimmed.$first_fde\t$dir_f/$first_fde.rubbish\t$dir_f/trimmed.$f2_zwei\t$dir_f/$f2_zwei.rubbish\n";
                        #    $warn --;
                      }
                  else{
                        print "sample ommitted: $singleline\n";
                  }
            }
      }
      elsif($mode eq "se"){
            #chomp $singleline;
            my$there_test2= `ls $singleline`;
            if($there_test2=~/$singleline/){
                      # both files are there
                      print "$dir_f/$singleline\t$dir_f/trimmed.$singleline\n";
                }
            else{
                  print "sample ommitted: $singleline\n";
            }

      }

}
1;
# for the qa/qc step after, we need only the relevant files in a nice format. to do this just do
# cat new_full_trim_files.tx | cut -f3,5 >qaqc_2_infil.txt
# this is the full file list as spitted out by this script, just the useful files
