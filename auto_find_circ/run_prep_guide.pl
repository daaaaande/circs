#!/usr/bin/perl -w
use strict;
# this script is intetended to be used to prepare auto_circs pipeline runs
# parameter given into script : $put_infile_and_fastqs_here_dir, $perl_scripts_dir
my$put_infile_and_fastqs_here_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs";
if(defined($ARGV[0])){
      # other dir to puit them
      $put_infile_and_fastqs_here_dir=$ARGV[0];
      chomp $put_infile_and_fastqs_here_dir;
}
# where all the helping perl script are
my$perl_scripts_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ";
if(defined($ARGV[1])){
      # other dir to get other perl scripts from
      $perl_scripts_dir=$ARGV[1];
      chomp $perl_scripts_dir;
}

my$paired_single_end_param="pe";


my$start_dir="/gpfs/project/daric102/circs_hilbert_scratchgs";
print "is the base in dir\n$start_dir\n? (y/n)\n";
my$arg_1=<STDIN>;
chomp $arg_1;
# check for other dir
if($arg_1=~/n/igx){
      print "enter new base in dir\n ";
      my$dir=<STDIN>;
      chomp $dir;
      $start_dir = $dir; # the new base in dir
}
print "okay. please give this run a name\noutput dirs, group and infiles will be named with this\n";

my$proj_name=<STDIN>;
chomp $proj_name;
print "started project $proj_name\n";

chdir $start_dir;
my$all_base_dir_dirs=`ls -df1 */`;
print "dirs to choose from:\n$all_base_dir_dirs\n please give the name of the dir where the infile .fastqs are currently\n";
my$go_to_dir=<STDIN>;
chomp $go_to_dir;
chdir "$start_dir/$go_to_dir";
########## here the best place to integrate the .gz execution if desired #########
print ".gz. infiles that need to be unpacked first? (y|n)\n";
my$unpack_maybe=<STDIN>;
chomp $unpack_maybe;
if($unpack_maybe=~/y/i){
      # now ask for pbs job or not, maybe if its more than 10 samples
      print "more than ~ 10 samples ? (y|n)if so, a PBS arrayjob will be used\n";
      my$pbs_yes=<STDIN>;
      chomp $pbs_yes;
      if($pbs_yes=~/y/i){
            # execute with array<job
            my$with_exec=`perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/execution_scripts/prep_unpack_job.pl `;
            print "executed job preparation and submit:\n$with_exec\n";
      }
      else{
            # execute without
            my$witouht=`perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/execution_scripts/prep_unpack_job.pl --pbs 0 `;
            print "executed ungzipping:\n$witouht\n";
      }
}


my$all_there_fastqs=`ls -f *.fastq`;
print "all files in $go_to_dir : \n$all_there_fastqs\ndoes this look like all the files you want to analyze with auto_circs?(y/n)\n";
# create allfastqs.list


my$confirm=<STDIN>;

if($confirm=~/y/igx){
      my$made_list_file=`ls -1f *.fastq>fastq_list_$proj_name.tx`;
      print "made fastq_list_$proj_name.tx\n";
}


print "paired end /single reads mode? (pe/se)\n";
$paired_single_end_param=<STDIN>;
chomp $paired_single_end_param;
my$lane_diff="R1";# default if nothing is given
my$lane_to_id="R2";
if($paired_single_end_param=~/pe/ig){

      print "how do file lane 1 and lane 2 differ? lane 1 identifier(R1|1.fastq)\n";
      $lane_diff=<STDIN>;
      chomp $lane_diff;
      print "how do file lane 1 and lane 2 differ? lane 2 identifier(R2|2.fastq)\n";
      $lane_to_id=<STDIN>;
      chomp $lane_to_id;


      # add module to trimm the reads
      print "do you want do trim the reads before? (y/n)\n";
      my$trim_yes=<STDIN>;
      chomp $trim_yes;
      if($trim_yes eq "y"){
            my$exex_trim=`perl $perl_scripts_dir/trimmomatic_infiles_creator_from_dir.pl --m $paired_single_end_param --i fastq_list_$proj_name.tx --l1 $lane_diff --l2 $lane_to_id >trimmomatic_infile_$proj_name.tx`;
            print "created trimming infile trimmomatic_infile_$proj_name.tx\nafter trimming the infile will need to be re-created\nexiting\n";
            die;
      }



      print "errors making infile:\n";
      # create infile with it
      my$create_infile=`perl $perl_scripts_dir/fastq_list_to_infile_circs_ext.pl --i fastq_list_$proj_name.tx --g $proj_name --l1 $lane_diff --l2 $lane_to_id --m $paired_single_end_param >infile_$proj_name.tx`;
      print "$create_infile\nnext, we will count the reads. download the file later for normalization\n";
      my$create_counts_infile=`cat infile_$proj_name.tx | cut -f1,2 >reads_infile_$proj_name.tx`;
      # create reqads infile with that
}# paired end mode done

elsif($paired_single_end_param =~/se/){ # single reads mode
      # same as above, but in one infile per line mode
      print "do you want do trim the reads before? (y/n)\n";
      my$trim_yes=<STDIN>;
      chomp $trim_yes;
      if($trim_yes eq "y"){
            my$exex_trim=`perl $perl_scripts_dir/trimmomatic_infiles_creator_from_dir.pl --m $paired_single_end_param --i fastq_list_$proj_name.tx --l1 $lane_diff --l2 $lane_to_id >trimmomatic_infile_$proj_name.tx`;
            print "created trimming infile trimmomatic_infile_$proj_name.tx\nafter trimming the infile will need to be re-created\nexiting\n";
            die;
      }



      print "errors making infile:\n";
      # create infile with it
      my$create_infile=`perl $perl_scripts_dir/fastq_list_to_infile_circs_ext.pl --i fastq_list_$proj_name.tx --g $proj_name --l1 $lane_diff --l2 $lane_to_id --m $paired_single_end_param >infile_$proj_name.tx`;
      print "$create_infile\nnext, we will count the reads. download the file later for normalization\n";
      my$create_counts_infile=`cat infile_$proj_name.tx | cut -f1 >reads_infile_$proj_name.tx`;
}
else{
      die "could not decide between paired and and single end mode \n";
}


print "made reads_infile_$proj_name.tx\ndoes this look good?(y/n)\n";
`head reads_infile_$proj_name.tx`;
my$check_2=<STDIN>;
chomp $check_2;
if($check_2=~/y/gix){
      print "good. making the next step, counting...\n";

}

else {
      print  "end.\n" ;
      die;
}

# read counts and save to file with that
my$coun_out=`perl $perl_scripts_dir/fastqs_to_reads_per_sample.pl --i reads_infile_$proj_name.tx --m $paired_single_end_param >reads_per_sample_$proj_name.tsv`;
      print "saved reads in file reads_per_sample_$proj_name.tsv\ngo and download it later to normalize the output!\n ";


# ask for hg19/hg38 or both
print "next,you want hg19 pipeline run, hg38 or both?(19/38/both)\n";
my$many_piplines=<STDIN>;
chomp $many_piplines;
my$create=0;
if($many_piplines=~/hg19/){
      mkdir "$put_infile_and_fastqs_here_dir/cx1_out/$proj_name";
      mkdir "$put_infile_and_fastqs_here_dir/f_c_out/$proj_name";
      mkdir "$put_infile_and_fastqs_here_dir/dcc_out/$proj_name";
      print "created $put_infile_and_fastqs_here_dir/*_out/$proj_name directories for hg19.\n";
      $create++;

}
if($many_piplines=~/hg38/){
      mkdir "$put_infile_and_fastqs_here_dir/cx2_out/$proj_name";
      mkdir "$put_infile_and_fastqs_here_dir/f_c2_out/$proj_name";
      mkdir "$put_infile_and_fastqs_here_dir/dcc_2_out/$proj_name";
      print "created $put_infile_and_fastqs_here_dir/*_out/$proj_name directories for hg38.\n";
      $create++;
}
if($many_piplines=~/both/){
      mkdir "$put_infile_and_fastqs_here_dir/cx1_out/$proj_name";
      mkdir "$put_infile_and_fastqs_here_dir/f_c_out/$proj_name";
      mkdir "$put_infile_and_fastqs_here_dir/dcc_out/$proj_name";
      mkdir "$put_infile_and_fastqs_here_dir/cx2_out/$proj_name";
      mkdir "$put_infile_and_fastqs_here_dir/f_c2_out/$proj_name";
      mkdir "$put_infile_and_fastqs_here_dir/dcc_2_out/$proj_name";
      print "created $put_infile_and_fastqs_here_dir/*_out/$proj_name directories for hg19 and hg38.\n";
      $create++;
}
if($create==0){
      print "did not create any output dirs.\n";
}
# create 3/6 outdirs
# move fastqs to indir
my$move_files=`mv $start_dir/$go_to_dir/*.fastq $put_infile_and_fastqs_here_dir`;
print "moved all .fastqs from $start_dir/$go_to_dir/ to $put_infile_and_fastqs_here_dir\n";
my$move_infile=`cp infile_$proj_name.tx $put_infile_and_fastqs_here_dir`;
print "moved files:\n$move_files\n$move_infile\n";
my$num_files_infile=`wc -l infile_$proj_name.tx`;
chomp $num_files_infile;
print "done.\nuseful information for your arrayjob: \n##################################\n";
print "pipelines_using=$many_piplines\nproject_name=$proj_name\ninfile_dir=$start_dir\nnum_samples=$num_files_infile\ninfile_name=infile_$proj_name.tx\npaired_single_end_mode=$paired_single_end_param \n############\nending.\n";
# move infile to indir
# show all relevant parameters for job creation: number of samples, biggest file size, example line, which hgrun


##### adding the job preparation+ execution with pbs ###############


print "known parameters that will be handled to the script if used:\npipelines_using:$many_piplines\nproject_name:$proj_name\nnum_samples:$num_files_infile\ninfile_name:infile_$proj_name.tx \npaired_single_end_mode:$paired_single_end_param \nfull infile with location:$put_infile_and_fastqs_here_dir/infile_$proj_name.tx\n\n\n";
print "\n\n start $perl_scripts_dir/pbs_array_execution.pl with known parameters?(y/n)\n";
my$start_array=<STDIN>;
chomp $start_array;

# ask here for automation vs default execution scripts
my$dry=0;
my$use_automation=0; # default is using the old ones

print"use automation.pl scripts? \n (y|n)\ny=automation.pl|n=single_sample.pl scripts\n";
my$answer_automation=<STDIN>;
chomp $answer_automation;
if($answer_automation=~/y/ig){
      $use_automation=1;
}


if($start_array =~/y/ig){
      # check if dry mode or not to qsub, then start the script with known parameters
      print "qsub the jobscripts to be executed aswell? (y/n)\nif no (n) it will just edit the files, and you will need to qsub them on your own\n";
      my$want_execute=<STDIN>;
      chomp $want_execute;
      if($want_execute=~/y/ig){
            # dryrn=1 param, actually qsub them here
            $dry=1;
            #`perl $perl_scripts_dir/pbs_array_execution.pl --m $paired_single_end_param --s 1 --g $many_piplines --i $put_infile_and_fastqs_here_dir/infile_$proj_name.tx --d 1 --jump_check 1 >$start_dir/$go_to_dir/pbs_exec_log_$proj_name.log`;
      }

      my$ou=`perl $perl_scripts_dir/pbs_array_execution.pl --m $paired_single_end_param --s 1 --g $many_piplines --i $put_infile_and_fastqs_here_dir/infile_$proj_name.tx --d $dry --jump_check 1 --au $use_automation >$start_dir/$go_to_dir/pbs_exec_log_$proj_name.log`;

      my$log_pbs=`cat $start_dir/$go_to_dir/pbs_exec_log_$proj_name.log`; # se the logs immediately after script is done
      print "pbs_execution logs:\n$log_pbs\n";
      print "created pbs_execution logfile $start_dir/$go_to_dir/pbs_exec_log_$proj_name.log \n";

}
else {
    print "ending.\n";
}
1;
