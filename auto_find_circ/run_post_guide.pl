#!/usr/bin/perl -w
use strict;
# this script is intetended to be used to prepare auto_circs pipeline output to matrixmaker jobs-
# folder cleanup 3-6x
# cat 3-6x
# matrix_infile_checker
# basic checks (circs per sample , samples in each out folder)
# TODO: catch all output into file to read later
# parameter given into script : $put_infile_and_fastqs_here_dir, $perl_scripts_dir
my$put_infile_and_fastqs_here_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs";
if(defined($ARGV[0])){
      # other dir to puit them
      $put_infile_and_fastqs_here_dir=$ARGV[0];
      chomp $put_infile_and_fastqs_here_dir;
}
# where all the helping perl script are
my$perl_scripts_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ";
if(defined($ARGV[1])){
      # other dir to get other perl scripts from
      $perl_scripts_dir=$ARGV[1];
      chomp$perl_scripts_dir;
}

# to get last proj name : ls -1f dcc_out/ | head -n 3 | tail -n 1
chdir $put_infile_and_fastqs_here_dir;


# ask for hg19/hg38 or both
print "HI!\ndid you do hg19 pipelene run, hg38 or both?(hg19/hg38/both)\n";
my$many_piplines=<STDIN>;
chomp $many_piplines;

# define output folders

my@out_dirs=();
my$proj_name="trash";

if($many_piplines=~/hg19/){
      $proj_name=`ls -1fth dcc_out/ | head -n 4 | tail -n 1`;
      chomp $proj_name;
      print "was the last project $proj_name ? (y/n)\n";
      my$agree_dir=<STDIN>;
      chomp $agree_dir;
      if($agree_dir=~/n/){
            # ask for full name
            print "then type in the target directory\n needs to be in $put_infile_and_fastqs_here_dir/*out/.(all_outfiles_runname)\n:";
            my$new_out_dir=<STDIN>;
            chomp $new_out_dir;
            $proj_name = $new_out_dir;
      }
      @out_dirs=("dcc_out","f_c_out","cx1_out");
}


elsif($many_piplines=~/hg38/){
      $proj_name=`ls -1fth dcc_2_out/ | head -n 1 | tail -n 1`;
      chomp $proj_name;
      print "was the last project $proj_name ? (y/n)\n";
      my$agree_dir=<STDIN>;
      chomp $agree_dir;
      if($agree_dir=~/n/){
            # ask for full name
            print "then type in the target directory\n needs to be in $put_infile_and_fastqs_here_dir/*out/.(all_outfiles_runname)\n:";
            my$new_out_dir=<STDIN>;
            chomp $new_out_dir;
            $proj_name = $new_out_dir;
      }
      @out_dirs=("f_c2_out","dcc_2_out","cx2_out");
}


elsif($many_piplines=~/both/){
      $proj_name=`ls -1fth dcc_2_out/ | head -n 1 | tail -n 1`;
      chomp $proj_name;
      print "was the last project $proj_name ? (y/n)\n";
      my$agree_dir=<STDIN>;
      chomp $agree_dir;
      if($agree_dir=~/n/){
            # ask for full name
            print "then type in the target directory\n needs to be in $put_infile_and_fastqs_here_dir/*out/.(all_outfiles_runname)\n:";
            my$new_out_dir=<STDIN>;
            chomp $new_out_dir;
            $proj_name = $new_out_dir;
      }
      @out_dirs=("f_c2_out","dcc_2_out","cx2_out","dcc_out","f_c_out","cx1_out");
}
else{
      print "did not create any output dirs or files .\n";
}
# create a file where all pre-defined commands will be written into- for easy matrix creation
my$outfile="commands_matrix_creation.$proj_name.txt";
open(OU,">",$outfile)|| die "$!";
# write the infile for the matrix into there
my$logfile="$put_infile_and_fastqs_here_dir/post_guide_output.$proj_name.log"; # where we catch all script output and collect it for later
open(LO,">",$logfile)|| die "$!";
# output dirs are known. now catting all outfiles together, clenup, matrix infile checker ...
foreach my $n (@out_dirs) {
      print "working on  $n ...\n";
      print LO "working on  $n ...\n";
      my$lines_per_sample=`wc -l $n/$proj_name/*.tsv`;
      print "all samples being processed now :\n$lines_per_sample\n";
      print LO "all samples being processed now :\n$lines_per_sample\n";
      my$cat_dcc1=`cat $n/$proj_name/*.tsv | grep -v sample >$n/$proj_name/all_samples_$proj_name.$n.tsv `;
      print "catted $n output: $cat_dcc1\nrunning matrix_input_checker:\n";
      print LO "catted $n output: $cat_dcc1\nrunning matrix_input_checker:\n";
      my$num_lines_mi=`wc -l $n/$proj_name/all_samples_$proj_name.$n.tsv`;
      chomp $num_lines_mi;
      print LO "new numer of circs :\n$num_lines_mi\n";
      my$chck_dcc1_out=`perl $perl_scripts_dir/matrix_infile_checker.pl $n/$proj_name/all_samples_$proj_name.$n.tsv`;
      print "errors in matrix infile $n: $chck_dcc1_out\n";
      my$correct_infile=`perl $perl_scripts_dir/matrix_infile_correcteur.pl $n/$proj_name/all_samples_$proj_name.$n.tsv >$n/$proj_name/corrected_all_samples_$proj_name.$n.tsv`;
      print LO "errors in matrix infile $n: $chck_dcc1_out\ncorrecting: $correct_infile\n";


      print "now vacuuming in $n/...\n";
      mkdir "$n/samples_done_$proj_name";
      my$mv_folders_done=`mv $n/run_* $n/samples_done_$proj_name`;
      print "moved output folders into $n/samples_done_$proj_name:$mv_folders_done\n";
      print LO "moved output folders into $n/samples_done_$proj_name:$mv_folders_done\n";
      print "info for matrix creation:\ndir: $put_infile_and_fastqs_here_dir/$n/$proj_name/\ninfile: corrected_all_samples_$proj_name.$n.tsv\nnumber of lines infile: $num_lines_mi\n";
      print LO "info for matrix creation:\ndir: $put_infile_and_fastqs_here_dir/$n/$proj_name/\ninfile: corrected_all_samples_$proj_name.$n.tsv\nnumber of lines infile: $num_lines_mi\n";



      my$matrix_one_command="";
      my$matrix_two_command="";
      if(!($n=~/2_/)){# detects if a folder is hg38 or hg19
            # hg38 auto detection
            $matrix_one_command="perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/matrixmaker-V4.pl --i $n/$proj_name/corrected_all_samples_$proj_name.$n.tsv --o $n/$proj_name/mat1.all_samples_$proj_name.$n.mat1 --g /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/refseq_to_gene_names_hg38.tsv";
            $matrix_two_command="perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/matrixtwo_V4.pl --i $n/$proj_name/mat1.all_samples_$proj_name.$n.mat1 --o $n/$proj_name/mat2.all_samples_$proj_name.$n.mat2 --excl_cb 1";
            # things for matrix creation: $proj_name dcc_out/$proj_name/all_samples_$proj_name.dcc_hg19.tsv
      }
      elsif($n=~/2_/){
            # hg38 case- add the non-default parameters                                                                                   # hg38 refseqid is not fixed for now, if we then run the correcteur no valid line will come out. to disabled the correcteur for hg38 for now
            $matrix_one_command="perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/matrixmaker-V4.pl --i $n/$proj_name/all_samples_$proj_name.$n.tsv --o $n/$proj_name/mat1.all_samples_$proj_name.$n.mat1 --l /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/logfile_auto.log --c /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/hg_38_circbase_known_circs.bed --g /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/refseq_to_gene_names_hg38.tsv";
            $matrix_two_command="perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/matrixtwo_V4.pl --i $n/$proj_name/mat1.all_samples_$proj_name.$n.mat1 --o $n/$proj_name/mat2.all_samples_$proj_name.$n.mat2 --excl_cb 1";

      }
      print OU "cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/\n";
      print OU "$matrix_one_command\n";
      print OU "$matrix_two_command\n";

}

close(OU);


print "done so far. also create matrix exe dir, pbs script and qsub it ?(y/n)\n";
my$execute_pbs_matrix_job=<STDIN>;
chomp $execute_pbs_matrix_job;
########################################## new parts: pbs_array_execution just for the matrix files #############################

if($execute_pbs_matrix_job=~/y/ig){
      # first, we will not need the execution file

      # create directory in /exec/logs/matrix$job
      my$make_mat_log_dir_here="/gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs";
      my$here_is_example_exex_file="/gpfs/project/daric102/circs_hilbert_scratchgs/exec";
      my$example_exex_file="e_example_hg19_matrix1_2_MMV4.sh";
      print "create logs/exec dir for run $proj_name in $make_mat_log_dir_here ? (y/n)\n";
      my$agree_create=<STDIN>;
      chomp $agree_create;
      if ($agree_create =~/n/ig){
            print "type in correct exec  base dir: (/gpfs/project/daric102/circs_hilbert_scratchgs/exec) \n";
            my$new_c_d=<STDIN>;
            chomp $new_c_d;
            $make_mat_log_dir_here = $new_c_d; # changed the dir where matrix_exec_run/ will be created
            $agree_create ="y";# corrected dir, now jump to "yes as answer"- does this work?
      }
      if($agree_create=~/y/ig){
            # agreed to create dir
            mkdir "$make_mat_log_dir_here/matrix_$proj_name";
            print "created $make_mat_log_dir_here/matrix_$proj_name\n";
            print LO "created $make_mat_log_dir_here/matrix_$proj_name\n";

            chdir "$make_mat_log_dir_here/matrix_$proj_name";
            #
            # copy example file
            my$cp_ex=`cp $here_is_example_exex_file/$example_exex_file $make_mat_log_dir_here/matrix_$proj_name/matrix_execution_$proj_name.sh`;
            print "copied example file : $cp_ex \n";
            print LO "copied example file : $cp_ex \n";

            my$edit_job_name=`sed -i 's/NAME_JOB/matrix_$proj_name/' $make_mat_log_dir_here/matrix_$proj_name/matrix_execution_$proj_name.sh`;
            print "changed jobname: $edit_job_name\n";
            print LO "changed jobname: $edit_job_name\n";
            # for the insert, we will need the commands file
            #sed -i '/cdef/r add.txt' input.txt
            # move command file into current dir, just to be safe
            my$move_commands=`mv  /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/commands_matrix_creation.$proj_name.txt $make_mat_log_dir_here/matrix_$proj_name/`;
            #my$insert_commands=`sed -i '/MAT1_exec/r commands_matrix_creation.$proj_name.txt ' $make_mat_log_dir_here/matrix_$proj_name/matrix_execution_$proj_name.sh`;
            my$insert_commands=`sed '/#MAT1_exec/r $make_mat_log_dir_here/matrix_$proj_name/commands_matrix_creation.$proj_name.txt' $make_mat_log_dir_here/matrix_$proj_name/matrix_execution_$proj_name.sh >$make_mat_log_dir_here/matrix_$proj_name/edited_matrix_execution_$proj_name.sh`;
            # my$insert_commands=`sed -i '/MAT1_exec/r commands_matrix_creation.$proj_name.txt ' $make_mat_log_dir_here/matrix_$proj_name/matrix_execution_$proj_name.sh`;
            # sed -e '/MAT1_exec/r commands_matrix_creation.test_array_mb30.txt' -e 'x;$G' matrix_execution_test_array_mb30.sh >edited_matrix_execution.sh
            # sed '/MAT1_exec/r commands_matrix_creation.test_array_mb30.txt' ../../e_example_hg19_matrix1_2_MMV4.sh  >try3_edited_matrix_execution.sh
            # then delete the commands file
            print "errors inserting lines from commands_matrix_creation.$proj_name.txt into \n$make_mat_log_dir_here/matrix_$proj_name/matrix_execution_$proj_name.sh\n: $insert_commands\n";
            print LO "errors inserting lines from commands_matrix_creation.$proj_name.txt into \n$make_mat_log_dir_here/matrix_$proj_name/matrix_execution_$proj_name.sh\n: $insert_commands\n";

            # insert all execution lines, should be in @all_matrix_commands

            print "qsub the script matrix_execution_$proj_name.sh? (yes/no)\n";
            my$qsub_maybe=<STDIN>;
            chomp $qsub_maybe;
            if($qsub_maybe=~/yes/ig){
                  chdir "$make_mat_log_dir_here/matrix_$proj_name"; # just in case- all logfiles should end up here
                  my$qsub_com=`qsub -q default $make_mat_log_dir_here/matrix_$proj_name/edited_matrix_execution_$proj_name.sh`;

                  print "submitted matrix job: $qsub_com\n";
                  print LO "submitted matrix job: $qsub_com\n";

            }
            # qsub
            # move the logfile into the matrix pbs logs dir
            close $logfile;
            my$mv_logfile_to_logdir=`mv $logfile $make_mat_log_dir_here/matrix_$proj_name/`;
            print "moved logfile $logfile to $logfile $make_mat_log_dir_here/matrix_$proj_name/: \n$mv_logfile_to_logdir\n";

      }
      else{
            die "could not read dir creation answer, end.\n";
      }

}


print "done.\n";
1;
