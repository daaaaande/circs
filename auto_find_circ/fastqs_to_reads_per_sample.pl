#!/usr/bin/perl -w
use strict;
# count reads in two .fastq files delivered in one line
# can use the first two columns of a pipeline infile as input
# like cat  infile.infile | cut -f1,2 >read_files_list,tx
# needs to be executed where .fastqs are or get full file paths
use Getopt::Long qw(GetOptions);


my$linfile= "all_fastqs.txt";
my$mode="pe";

GetOptions('infile|i=s' => \$linfile,'mode|m=s'=>\$mode) or warn "Using default parameters: $0 --from NAME\n";

chomp $linfile;
chomp $mode;
#print "reading input file $linfile ...\n";
open(IN,$linfile)|| die "$!";
my@allelines= <IN>;
print "sample_short\treads_total\n";
foreach my $two_file_line (@allelines){
      if($mode eq "pe"){
      my@pair_files=split(/\s+/,$two_file_line);
      my$file_1=$pair_files[0];
      my$reads_file1=`cat $file_1|wc -l`;
      chomp $reads_file1;
      my$total_reads_sample=$reads_file1/4;
      # in paired end, if we have one read across two alnes that makes it still one read, so we dont need to add up reads here.
      # also, the number of reads should be exactly the same in lane1 and lane2 file, hence we can use only one of those

      # every 4 lines in BOTH files is one read

      # making the sample name nicer
      my$sample_name=$file_1;
      $sample_name=~s/00[12]//g;# remove lane1/2 indicator
      $sample_name=~s/\.fastq//;# remove file format
      $sample_name=~s/R[12]//;
      $sample_name=~s/L00[0-9]{1,5}//;
      $sample_name=~s/__/_/g;
      $sample_name=~s/__[12]//;
      $sample_name=~s/_L_//;
      $sample_name=~s/__/_/g;
      # sample name as tidy as possible
      print "$sample_name\t$total_reads_sample\n";
      # execute echo $(cat yourfile.fastq|wc -l)/4|bc
      # get the output for each two files and make a sum
      # output a table with easy samplenames and the total sum of reads in both files
}
elsif($mode eq "se"){
      chomp $two_file_line;
      my$reads_file1=`cat $two_file_line|wc -l`;
      chomp $reads_file1;
      my$total_reads_sample= $reads_file1/4;
      my$sample_name=$two_file_line;
      $sample_name=~s/00[12]//g;# remove lane1/2 indicator
      $sample_name=~s/\.fastq//;# remove file format
      $sample_name=~s/R[12]//;
      $sample_name=~s/L00[0-9]{1,5}//;
      $sample_name=~s/__/_/g;
      $sample_name=~s/__[12]//;
      $sample_name=~s/_L_//;
      $sample_name=~s/__/_/g;
      # sample name as tidy as possible
      print "$sample_name\t$total_reads_sample\n";
}
}
1;
