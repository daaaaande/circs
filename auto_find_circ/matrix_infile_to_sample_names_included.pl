#!/usr/bin/perl -w
use strict;
# script to quickly check the samples included in a matrix infile
my$start = time;
my$linfile= $ARGV[0];
chomp $linfile;
open(IN,$linfile)|| die "$!";
my@allelines= <IN>; #input file



my$sample_string="";
my$e=0;

foreach my $line (@allelines){
      $e++;
      chomp $line;
      my@parts=split(/\t+/,$line);
      my$sample=$parts[2];
      if(!($sample_string=~/$sample/)){
            $sample_string="$sample_string\n$sample";
            #print "new sample $sample at line $e\n";
      }


}

print "samples included in $linfile:\n$sample_string\n";
1;
