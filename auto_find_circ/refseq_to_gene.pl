#!/usr/bin/perl -w
use strict;
use Getopt::Long qw(GetOptions);
######################
# get a file with only a list of refseqids (NM_0009) and print only their gene names according to mapping files
# usage; perl refseq_to_gene.pl refseqs_column.txt >genenames.txt
######################
my $start = time;

my$infile="in.tab";
my$mapping_file="/home/daric/work_enclave/auto_circs/auto_find_circ/refseq_to_gene_names_hg38.tsv";
GetOptions('mapping_file|m=s' => \$mapping_file,'infile|i=s'=>\$infile )|| warn "using default parameters!\n";
chomp ($mapping_file,$infile);
my%mapping=();
open(MA,$mapping_file)|| die "$!";# need to change this before git push

my@allemappings= <MA>;
########################################################################### gene mapping file reading into hash %mapping

# each line now one array part
#print ER "reading gene mapping...\n";

foreach my $mapline (@allemappings){
	# fill a hash that is used later
	chomp $mapline;
	if(!($mapline=~/^$/)){
		my@slit=split(/\t+/,$mapline);
      	if(scalar(@slit)>1){
			my$genene=$slit[0];
			$genene =~ s/\s+//g; # remove emptieness
			my$nnum=$slit[1];# will be key
			$nnum =~ s/\s+//g;
			if($nnum=~/N/){ # empty lines do not help
				$mapping{"$nnum"}="$genene";
				#	print "mapping now key  $nnum to $genene \n";
				#mapping now key  NM_001199238 to GPANK1
			}
		}	
	}
}

close MA;

my$linfile= $infile;
chomp $linfile;
open(IN,$linfile)|| die "$!";
my@allelines= <IN>; #input file
# candidatelist_auto_all_sites.bed.csv file created with steptwo.pl
my$i=0;
foreach my $line (@allelines){
  $i++;
#  print "now in line $i with $line \n";
  my$gene="None";
#	my$line=$allelines[$i];
  chomp $line;
  $line=~s/\s//g;
  if(exists($mapping{$line})){
    $gene=$mapping{$line};

  }

  print "$gene\n";
}
1;
