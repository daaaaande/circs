#/usr/bin/perl -w

# this is the extended version- groupname is now a parameter to set,
# run example here : perl ~/Desktop/fastq_list_to_infile_circs_ext.pl fastqs.list group_name >godfather_infile.txt


# sample name structures that should be parsed correctly:
 #Med8a_Ctrl_R1_1.fastq and Med8a_Ctrl_R1_2.fastq
 #CHLA266_R1_trimmed.fastq and CHLA266_R2_trimmed.fastq


# script to get from a list of .fastqs to a acceptable infile for godfather.pl
#usage:
# go to folder with only interesting fastq files
#   ls -1 *.fastq >files.list
#     perl ~/Desktop/fastq_list_to_infile_circs.pl fastqs.list  group_name lane_1_indicator lane_2_indicator >godfather_infile.txt
#       move/copy .fastqs to /find_circ
#         copy godfather_infile.txt into find_circ/
#           cd find_circ/auto_find_circ
#             perl godftaher.pl godfather_infile.txt
use strict;
use Getopt::Long qw(GetOptions);

my$inputfile="fastq_list.txt";
my$group_string="default_group";
my$to_replace="001";
my$replace_into="002";
my$mode="pe";# pe for two lanes (files ) se for single lane input

GetOptions('group_string|g=s' => \$group_string,'to_replace|l1=s' => \$to_replace,'infile|i=s' => \$inputfile,'replace_into|l2=s' => \$replace_into,'mode|m=s'=>\$mode) or warn "Using default parameters: $0 --from NAME\n";

chomp $group_string;
chomp $to_replace;
chomp $replace_into;
chomp $inputfile;
chomp $mode;

open(IN,$inputfile)|| die "$!";	# infile is a .csv file steptwo output.csv
my@lines=<IN>;
my$i=0;
foreach my $singleline (@lines){
      my$i++;
      # check for first file
      # if there use parameters to replace into second file
      # make nice samplename,
      # print in stdout: first file, second file, sample name, group
      chomp $singleline;
      my$file_1_check=`ls -1 $singleline`;
      if($mode eq "pe"){
            if(($file_1_check=~/\.fastq/)&&($singleline=~/$to_replace/)){ # if file is there and the current line has what every second file should have
                  # first file is there
                  my$to_change=$singleline;
                  my$to_change_2=$singleline;
                  $to_change =~s /$to_replace/ooo/o;# so we cant do it all at once, need some very unique thing as surrogate
                  $to_change=~s/ooo/$replace_into/o;
                  #print "done changing file $singleline into $to_change\n";
                  my$file_two_check=`ls -1 $to_change`;
                  if($file_two_check=~/\.fastq/){
                        # make samplename nice
                        $to_change_2=~s/\.fastq//;
                        $to_change_2=~s/__/_/;
                        $to_change_2=~s/00[12]//;
                        $to_change_2=~s/R[12]//;
                        $to_change_2=~s/L[0-9]{2,7}//;
                        $to_change_2=~s/__/_/;
                        $to_change_2=~s/trimmed/tr_/;
                        # should be nice enough

                        # if we run in pe but have only 1 file, enter this regardless - will then be run as a single end file
                        # this can then be changed later or omitted
                        # as an indicator, add 1lane_ or somthing to samplename

                        # original file- lane 2 file - sample name - group
                        print "$singleline\t$to_change\t$to_change_2\t$group_string\n";
                  }
                  else{
                    warn "sample $singleline: in paired end mode, but did not find 2nd .fastq $to_change\t switching this sample to single end...\n ";
                    print "$singleline\t$to_change_2\t$group_string\n";
                  }
            }
      }
      elsif($mode eq "se"){
            my$to_change_2=$singleline;
            $to_change_2=~s/\.fastq//;
            $to_change_2=~s/__/_/;
            $to_change_2=~s/00[12]//;
            $to_change_2=~s/R[12]//;
            $to_change_2=~s/L[0-9]{2,7}//;
            $to_change_2=~s/__/_/;
            $to_change_2=~s/trimmed/tr_/;
            print "$singleline\t$to_change_2\t$group_string\n";

      }
}

1;
