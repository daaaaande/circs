#!/usr/bin/perl -w
use strict;
use Getopt::Long qw(GetOptions);
# script to get from the end of run_prep_guide.pl an already prepared jobscript to start immediately

# go to repo/circs/.
# perl auto_find_circ/pbs_array_execution.pl --m pe --s 1 --g hg19 --d 0
# perl auto_find_circ/pbs_array_execution.pl --m pe --s 1 --g hg19 --i /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/test_pbs_prep_infile.txt --d 0
# perl auto_find_circ/pbs_array_execution.pl --m pe --s 1 --g both --i /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/test_pbs_prep_infile.txt --d 0
#things it should be able to do :
#- get the infile name, location, nlines
#- make dir in /exec/logs/array_$jobname
#- copy the sample scripte to there
#- detect if hg19/38 or both are used
#- edit the hg19/38 jobscript:
#                       - nexecs (arraylength)
#                        - infile
#                        - run name?

# then qsub both / one jobscript,
# watch until finished
# notice when post_run_guide could be started

# get the infile name
# TODO: add parameter to do the automation:scripts instead of the old ones-
# first we have to know the status the script is started in
# we expect post execution of run_prep_guide.pl, so all files are where they need to be, we can parse all needed infromation from the infile



my$use_automation=0; # parameter to set automation scripts as the executed ones instead of the old ones, will also use the --delete big function

# possible status ids:
# 1-> execution of run_prep_guide.pl finished
my$status_prep=1;

# mode: important to be able to parse the infile: paired end vs single read mode
#pe -> paired end, 2 infiles per sample , exec mode paired end, paired end execution scripts
my$mode="pe";

my$logs_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs";
my$scripts_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/exec";# /gpfs/project/daric102/circs_hilbert_scratchgs/exec/ for single_sample. /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/execution_scripts/ for automation
my$genome_mode="hg19"; # hg19, hg38, both - execute hg19 , hg38 or both?

# get the default infile- whatever is in repo/circs
# get the infile
my$infile_cands=`ls -1f /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/*infile*.tx* | head -n 1 `;
chomp $infile_cands;
my$full_infile_cand="$infile_cands";
# check for more than one file
my$num_infile_check=`ls -1f /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/*infile*.tx* | wc -l `;
if($num_infile_check > 1){
      print "more than one infile found, using ___ $full_infile_cand ____ \n" ;
      my@all_files=split(/\n/,$full_infile_cand);
      my$final_file=$all_files[0];
      chomp $final_file;
      $full_infile_cand=$final_file;
}

# make into 1 for just print commands, not execute any thet create dirs or change files or execute jobs
my$dry_run=0;

my$jump_check=0;# to avoid the stdin questions if used non-interactively (1) for jump, 0 for ask interactively


GetOptions('status|s=i' => \$status_prep,'mode|m=s'=>\$mode,'genome_vers|g=s'=>\$genome_mode,'infile|i=s'=>\$full_infile_cand,'dry_run|d=i'=>\$dry_run,'jump_check|chk=i'=>\$jump_check,'automation|au=i'=>\$use_automation,'scripts_dir|scr=s'=>\$scripts_dir) or warn "Using default parameters: $0 --from NAME\n";
chomp($status_prep,$mode,$genome_mode,$full_infile_cand,$dry_run,$jump_check,$use_automation,$scripts_dir);

# parse the infile: number of samples, run name

my$num_lines_infile=`wc -l $full_infile_cand`;
chomp $num_lines_infile;
$num_lines_infile =~s /$full_infile_cand//;
$num_lines_infile =~s/\s+//;
# number of samples = $num_lines_infile

# option to change from single_sample.pl scripts to automation.pl scripts
my$hg19_example_script="$scripts_dir/hg19_array_example.sh";
my$hg38_example_script="$scripts_dir/hg38_array_example.sh";

if(($use_automation)){# if the default is changed by --au 1
      # re-define the jobscripts here only if non-default option is given - later we have to pass this choice to run_prep_guide.pl as a question
      $hg19_example_script="$scripts_dir/automation_hg19_array_example.sh";
      $hg38_example_script="$scripts_dir/automation_hg38_array_example.sh";
      # also to-do: check if all the replacements will still work in the automation job scripts
}

# new, automation.pl versions
# /gpfs/project/daric102/circs_hilbert_scratchgs/exec
# automation_hg19_array_example.sh
# automation_hg38_array_example.sh




# run_name
my$first_line=`head -n 1 $full_infile_cand`;
chomp $first_line;
my@split_first=split(/\s+/,$first_line);
# depending on paired end/ single read mode we get the runname now
my$exec_name="";
if($mode eq "pe"){
      # exec name is the 4th element, number 3
      $exec_name=$split_first[3];
}
elsif($mode eq "se"){
      $exec_name= $split_first[2];
}

chomp $exec_name;

# to-do:
# make array execution dir
# copy 1-2 files from examples there
# edit: arraylength, execution name , infile for 1-2 files
# qsub
# starting from here, we need dryrun= no qsub ?
my$check_res=0;
if($jump_check==0){# mdefault behaviour: let the user confirm that everything is ok

      # check if the infile is correct
      print "please check below:\ninfile_full_path:$full_infile_cand\nnumber_samples:$num_lines_infile\nrun_name from infile:$exec_name\ngenome_using:$genome_mode\nexecuting_qsub:$dry_run\n\neverything correct?(y|n):";
      my$check=<STDIN>;# user HAS to approve all information given
      chomp($check);
      if(!($check=~/y/ig)){
            die "killed by user: wrong parameters.\ncheck mode/status/infile parameters? checkings parameters: \nchecked_things=$check_res\nskip_interactive_checking=$jump_check\nuser_input_check=$check\n";
      }
      else{
            $check_res=1;
      }
}

else{
      print "jump_check parameter set to 1, thus skipping the checks...\nchecking parameters: \nchecked_things=$check_res\nskip_interactive_checking=$jump_check\n";
      $check_res = $jump_check; # both is true now: we need no check, therefore we can continue
}


if($check_res < 1){
      die "killed by user: parameters unchecked .\ncheck mode/status/infile parameters?\n\nchecked_things=$check_res\nskip_interactive_checking=$jump_check\n";
}

 # if jumpcheck==1, execute independent of the stdin
# old, default chioces
# TODO: add se/pe parameter from answers into execution line pbs scripts

else{# else is now if jumpcheck ==1 or if the interactive check was a sucess
      # only print

      print "control parameters: \nchecked_things=$check_res\nskip_interactive_checking=$jump_check\nchecking completed, creating directory ...\n";
      # make the logs dir
      mkdir "/gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name"; # make the dir where the logs, and sh scripts will be
      print "errors making the dir /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name : \n";
      chdir "/gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name";# go into log dir- here we should be able to see all logs later
      # make array to dump all scripts to be submitted into
      my@aray_scripts_finished=();
      if($genome_mode eq "hg19"){
            # copy the example script to the new logs dir
            my$cp_er=`cp $hg19_example_script /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/hg19_execution_$exec_name.sh`;
            print"errors copy file: $cp_er\n";
            # better way: do sed, file gets printed, catch the output and do that for each time
            # sed 's/^\#PBS\s\-J.*$/#PBS -J 1-12/' test_hg19_script.sh | head -n 12

            my$sed_array_l=`sed 's|^\#PBS\ \-J.*|#PBS\ -J\ 1-$num_lines_infile|' /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/hg19_execution_$exec_name.sh >/gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh `;
            print "hg19:changing array size to $num_lines_infile: $sed_array_l\n";
            #sed 's/infile_halo1_rerun_1\.tx/infile_test\.tx/' test_hg19_script.sh
            # cat ../../hg19_array_example.sh | sed 's/trimmed_fastqs_lena\.infile/test_infile\.test/'

            # infile MUST be in /repo/circ/.
            my$sed_infile= `sed -i 's|FULL_INFILE_PATH|$full_infile_cand|' /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh`;
            print "hg19:adjusting infile name : $sed_infile\n";

            my$sed_array_name=`sed -i 's|^\#PBS\ \-N.*|#PBS\ -N\ $exec_name.hg19|' /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh`;
            print"hg19:adjusting job name : $sed_array_name\n";
            # now we have the edited file with array length, project name and infile
            # file is : /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh

            #my$sub_co=`qsub -q default /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh`;
            #print "errors submitting job  hg19 $exec_name \nwith exec file /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh\n: $sub_co\n";
            # check if the edits work
                  # qsub
            push(@aray_scripts_finished,"/gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh");
      }
      elsif($genome_mode eq "hg38"){
        # # copy the example script to the new logs dir
        my$cp_er=`cp $hg38_example_script /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/hg38_execution_$exec_name.sh`;
        print"errors copy file: $cp_er\n";
        my$sed_array_l=`sed 's|^\#PBS\ \-J.*|#PBS -J 1-$num_lines_infile|' /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/hg38_execution_$exec_name.sh >/gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg38_execution_$exec_name.sh `;
        print "hg38:changing array size to $num_lines_infile: $sed_array_l\n";
        #sed 's/infile_halo1_rerun_1\.tx/infile_test\.tx/' test_hg19_script.sh
        # cat ../../hg19_array_example.sh | sed 's/trimmed_fastqs_lena\.infile/test_infile\.test/'

        # infile MUST be in /repo/circ/.
        my$sed_infile= `sed -i 's|FULL_INFILE_PATH|$full_infile_cand|' /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg38_execution_$exec_name.sh`;
        print "hg38:adjusting infile name : $sed_infile\n";

        my$sed_array_name=`sed -i 's|^\#PBS\ \-N.*|#PBS -N $exec_name.hg19|' /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg38_execution_$exec_name.sh`;
        print"hg38:adjusting job name : $sed_array_name\n";
        # now we have the edited file with array length, project name and infile
        # file is : /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh

        #my$sub_co=`qsub -q default /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg38_execution_$exec_name.sh`;
        #print "errors submitting job  hg38 $exec_name \nwith exec file /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh\n: $sub_co\n";
        push(@aray_scripts_finished,"/gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg38_execution_$exec_name.sh");
      }
      elsif($genome_mode eq "both"){
            # hg19
                                                                  # make this changeable
            my$cp_er=`cp $hg19_example_script /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/hg19_execution_$exec_name.sh`;
            print"hg19:errors copy file: $cp_er\n";
            # better way: do sed, file gets printed, catch the output and do that for each time
            # sed 's/^\#PBS\s\-J.*$/#PBS -J 1-12/' test_hg19_script.sh | head -n 12

            my$sed_array_l=`sed 's|^\#PBS\ \-J.*|#PBS -J 1-$num_lines_infile|' /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/hg19_execution_$exec_name.sh >/gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh `;
            print "hg19:changing array size to $num_lines_infile: $sed_array_l\n";
            #sed 's/infile_halo1_rerun_1\.tx/infile_test\.tx/' test_hg19_script.sh
            # cat ../../hg19_array_example.sh | sed 's/trimmed_fastqs_lena\.infile/test_infile\.test/'

            # infile MUST be in /repo/circ/.
            my$sed_infile= `sed -i 's|FULL_INFILE_PATH|$full_infile_cand|' /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh`;
            print "hg19:adjusting infile name : $sed_infile\n";

            my$sed_array_name=`sed -i 's|^\#PBS\ \-N.*|#PBS -N $exec_name.hg19|' /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh`;
            print"hg19:adjusting job name : $sed_array_name\n";
            # now we have the edited file with array length, project name and infile
            # file is : /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh

            #my$sub_co=`qsub -q default /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh`;
            #print "errors submitting job  hg19 $exec_name \nwith exec file /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh\n: $sub_co\n";

            # hg38

            $cp_er=`cp $hg38_example_script /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/hg38_execution_$exec_name.sh`;
            print"hg38:errors copy file: $cp_er\n";
            $sed_array_l=`sed 's|^\#PBS\ \-J.*|#PBS -J 1-$num_lines_infile|' /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/hg38_execution_$exec_name.sh >/gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg38_execution_$exec_name.sh `;
            print "hg38:changing array size to $num_lines_infile: $sed_array_l\n";
            #sed 's/infile_halo1_rerun_1\.tx/infile_test\.tx/' test_hg19_script.sh
            # cat ../../hg19_array_example.sh | sed 's/trimmed_fastqs_lena\.infile/test_infile\.test/'

            # infile MUST be in /repo/circ/.
            $sed_infile= `sed -i 's|FULL_INFILE_PATH|$full_infile_cand|' /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg38_execution_$exec_name.sh`;
            print "hg38:adjusting infile name : $sed_infile\n";

            $sed_array_name=`sed -i 's|^\#PBS\ \-N.*|#PBS -N $exec_name.hg38|' /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg38_execution_$exec_name.sh`;
            print"hg38:adjusting job name : $sed_array_name\n";

            # now we have the edited file with array length, project name and infile
            # file is : /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh

            #my$sub_co=`qsub -q default /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg38_execution_$exec_name.sh`;
            #print "errors submitting job  hg38 $exec_name \nwith exec file /gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh\n: $sub_co\n";
            push(@aray_scripts_finished,"/gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg19_execution_$exec_name.sh");
            push(@aray_scripts_finished,"/gpfs/project/daric102/circs_hilbert_scratchgs/exec/logs/array_$exec_name/edited_hg38_execution_$exec_name.sh");

      }
      # checking was ok, all.sh files are prepared. now qsub, qstat and finished notification maybe?


      if($dry_run){# if --d 1 is set, we not only edit but also qsub the jobs
            foreach my $finished_job_script (@aray_scripts_finished){
                  if($finished_job_script=~/[a-z]/){
                              # execute qsub, print job array name?
                              chomp $finished_job_script;
                              my$qsub_co=`qsub -q default $finished_job_script`;
                              sleep(3);# to not overload the scheduler
                              print"submitted script file $finished_job_script: \n$qsub_co \n";
                  }# check for empty elements, just in case
            }

            # execute
      }
}
1;
