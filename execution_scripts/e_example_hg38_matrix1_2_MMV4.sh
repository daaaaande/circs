#PBS -l select=1:ncpus=12:mem=12gb:arch=ivybridge
#PBS -l walltime=63:59:00
#PBS -A circs
#PBS -N m_hg38
#PBS -q BioJob
#PBS -m abe
#PBS -r y

module load Parallel
module load Perl/5.18.1
module load intel/xe2019



LOGFILE=$PBS_O_WORKDIR/$PBS_JOBNAME"."$PBS_JOBID".log"
echo "$PBS_JOBID ($PBS_JOBNAME) @ `hostname` at `date` in "$RUNDIR" START" > $LOGFILE
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo "this is hg38 lena2 fc cleaned" >> $LOGFILE


cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/ 2>&1 >>$LOGFILE


# starting

perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/matrixmaker-V4.pl --i f_c2_out/test_array_mb30/fc2_matrix_infile_arraytest_2.tsv --o f_c2_out/test_array_mb30/fc2_matrix_infile_3.mat1 --l /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/logfile_auto.log --c /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/hg_38_circbase_known_circs.bed --chseqi 0 --g /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/refseq_to_gene_names_hg38.tsv
perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/matrixmaker-V4.pl --i dcc_2_out/test_array_mb30/dcc2_matrix_infile_arraytest2.tsv --o dcc_2_out/test_array_mb30/dcc2_matrix_infile_3.mat1 --l /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/logfile_auto.log --c /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/hg_38_circbase_known_circs.bed --chseqi 0 --g /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/refseq_to_gene_names_hg38.tsv
perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/matrixmaker-V4.pl --i cx2_out/test_array_mb30/cx2_matrix_infile_arraytest2.tsv --o cx2_out/test_array_mb30/cx2_matrix_infile_3.mat1 --l /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/logfile_auto.log --c /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/hg_38_circbase_known_circs.bed --chseqi 0 --g /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/refseq_to_gene_names_hg38.tsv


echo " matrixmaker V4 hg38 params run is done ...">>$LOGFILE

echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE


perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/matrixtwo_V4.pl --i f_c2_out/test_array_mb30/fc2_matrix_infile_3.mat1 --o f_c2_out/test_array_mb30/fc2_matrix_infile_3.mat2 --excl_cb 1
perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/matrixtwo_V4.pl --i dcc_2_out/test_array_mb30/dcc2_matrix_infile_3.mat1 --o dcc_2_out/test_array_mb30/dcc2_matrix_infile_3.mat2 --excl_cb 1
perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/matrixtwo_V4.pl --i cx2_out/test_array_mb30/cx2_matrix_infile_3.mat1 --o cx2_out/test_array_mb30/cx2_matrix_infile_3.mat2 --excl_cb 1

echo " matrix2  run V4 is done ...">>$LOGFILE

echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE

qstat -f $PBS_JOBID
