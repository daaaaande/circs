#PBS -l select=1:ncpus=12:mem=42gb:arch=ivybridge
#PBS -l walltime=8:59:00
#PBS -A circs
#PBS -N lena_hg19
#PBS -q BioJob
#PBS -m abe
#PBS -r y
#PBS -J 1-12

# just in case- load all relevant
module load Bowtie2
module load Python/2.7.5
#module load Parallel
module load SamTools/1.6
module load bedtools/2.26.0
#module load BamTools
#module load R
#module load intel/xe2019



LOGFILE=$PBS_O_WORKDIR/$PBS_JOBNAME"."$PBS_JOBID".log"
echo "$PBS_JOBID ($PBS_JOBNAME) @ `hostname` at `date` in "$RUNDIR" START" > $LOGFILE
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo "this is run hg19  infiles ######################################################################################" >> $LOGFILE


cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/ 2>&1 >>$LOGFILE
echo " changing dir..">>$LOGFILE
# now starting the dependencies war...
# term problems in the past...
export TERM=xterm

# filename retrieval
infileline=`cat FULL_INFILE_PATH | tail -n +${PBS_ARRAY_INDEX} | head -1`


echo "this is run 1 of sample $infileline " >> $LOGFILE


# find_circ

cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/ 2>&1 >>$LOGFILE

perl f_c_single_sample.pl $infileline

echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " f_c run $infileline is done ...">>$LOGFILE


# DCC

cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/automate_DCC/ 2>&1 >>$LOGFILE
module load STAR/2.5.1b

perl dcc_ex_single_sample.pl $infileline

echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " DCC run $infileline is done ...">>$LOGFILE


# CX1

cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/circexplorer1_auto/ 2>&1 >>$LOGFILE

perl cx1_ex_single_sample.pl $infileline

echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " CIRCex1 run $infileline should also be done ...">>$LOGFILE



echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE

qstat -f $PBS_JOBID
