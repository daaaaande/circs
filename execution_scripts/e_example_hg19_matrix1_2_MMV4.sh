#PBS -l select=1:ncpus=12:mem=12gb:arch=ivybridge                           
#PBS -l walltime=63:59:00
#PBS -A circs
#PBS -N NAME_JOB
#PBS -q BioJob
#PBS -m abe
#PBS -r y

module load Parallel
module load Perl/5.18.1
module load intel/xe2019



LOGFILE=$PBS_O_WORKDIR/$PBS_JOBNAME"."$PBS_JOBID".log"
echo "$PBS_JOBID ($PBS_JOBNAME) @ `hostname` at `date` in "$RUNDIR" START" > $LOGFILE
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo "this is M4 creation " >> $LOGFILE


cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/ 2>&1 >>$LOGFILE


# starting 
#perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/matrixmaker-V4.pl --i f_c_out/myc_rnase_part2/all_samples_myc_rnase_part2.f_c_out.tsv --o f_c_out/myc_rnase_part2/all_samples_myc_rnase_part2.f_c_out.mat1

#MAT1_exec

echo " matrixmaker V4 run is done ...">>$LOGFILE

echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE


#perl /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/matrixtwo_V4.pl --i f_c_out/myc_rnase_part2/all_samples_myc_rnase_part2.f_c_out.mat1 --o f_c_out/myc_rnase_part2/all_samples_myc_rnase_part2.f_c_out.mat2

echo " matrix2  run is done ...">>$LOGFILE

echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE

qstat -f $PBS_JOBID
