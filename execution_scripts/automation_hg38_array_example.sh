#PBS -l select=1:ncpus=12:mem=42gb:arch=ivybridge
#PBS -l walltime=8:59:00
#PBS -A circs
#PBS -N _hg38
#PBS -q BioJob
#PBS -m abe
#PBS -r y
#PBS -J 1-12

# loading needed libraries
module load Bowtie2
module load Python/2.7.5
module load SamTools/1.6
module load bedtools/2.26.0

LOGFILE=$PBS_O_WORKDIR/$PBS_JOBNAME"."$PBS_JOBID".log"
echo "$PBS_JOBID ($PBS_JOBNAME) @ `hostname` at `date` in "$RUNDIR" START" > $LOGFILE
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/ 2>&1 >>$LOGFILE
echo " changing dir..">>$LOGFILE
export TERM=xterm
infileline=`cat FULL_INFILE_PATH | tail -n +${PBS_ARRAY_INDEX} | head -1`
echo "this is run automation hg38 of sample $infileline " >> $LOGFILE


# find_circ
cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/ 2>&1 >>$LOGFILE
perl find_circ_automation.pl --i "$infileline" --out /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/f_c2_out \
--f_q /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/all_chroms \
--ref /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38 \
--anot /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/hg38_ucsc_refseq.bed \
--delete big
# note: can be added params: --strict 1 to only accept 40x40 align quality junctions:
# perl find_circ_automation.pl --i "$infileline" --out /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/f_c2_out --f_q /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/all_chroms --ref /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38 --anot /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/hg38_ucsc_refseq.bed --delete big --strict 1
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " f_c_automation hg38 run $infileline is done ...">>$LOGFILE


# DCC
module load STAR/2.7.1a
cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/automate_DCC/ 2>&1 >>$LOGFILE
perl dcc_automation.pl --i "$infileline" --out /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_2_out \
--f_q /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_full.fa \
--ref /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full \
--anot /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_ucsc_ref_new_chroms.gtf \
--bed /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/hg38_ucsc_refseq.bed \
--parser dcc_2_outreader.pl --delete big --linear 1
# for strict filtering, use :
# perl dcc_automation.pl --i "$infileline" --out /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_2_out --parser dcc_2_outreader.pl --f_q /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_full.fa --ref /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full --anot /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_ucsc_ref_new_chroms.gtf --bed /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/hg38_ucsc_refseq.bed  --linear 1 --filter " -F -R /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/data_files/hg38_repeatmasker_simplerepeats_merged.gtf"
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " DCC_automation hg38 run $infileline is done ...">>$LOGFILE

# CX1
cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/circexplorer1_auto/ 2>&1 >>$LOGFILE
perl circexplorer_automation.pl --i "$infileline" --out /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/cx2_out \
--f_q /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_full.fa \
--ref /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full \
--anot /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg_38_ref_fetched.txt \
--bed /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/chr_refseq_genes_hg38.bed \
--delete big
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " CIRCex1_automation hg38 run $infileline  done ...">>$LOGFILE

qstat -f $PBS_JOBID
