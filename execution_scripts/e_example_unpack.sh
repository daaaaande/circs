#PBS -l select=1:ncpus=1:mem=8gb:arch=ivybridge
#PBS -l walltime=6:00:00
#PBS -A circs
#PBS -N unpack_gz
#PBS -q BioJob
#PBS -m bea
#PBS -r y
#PBS -J 1-PBS_NUMFILES

module load intel/xe2019
#module load STAR
#module load Bowtie2

LOGFILE=$PBS_O_WORKDIR/$PBS_JOBNAME"."$PBS_JOBID".log"
echo "$PBS_JOBID ($PBS_JOBNAME) @ `hostname` at `date` in "$RUNDIR" START" > $LOGFILE
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE

# unpacking


cd DIR_TO_GZ_FILES 2>&1 >>$LOGFILE
echo " changing dir..">>$LOGFILE
filename=`ls -f1 DIR_TO_GZ_FILES/*.gz | tail -n +${PBS_ARRAY_INDEX} | head -1`

gunzip  $filename


echo " $filename is decompressed...">>$LOGFILE



echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE

qstat -f $PBS_JOBID
