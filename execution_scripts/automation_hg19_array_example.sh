#PBS -l select=1:ncpus=12:mem=42gb:arch=ivybridge
#PBS -l walltime=8:59:00
#PBS -A circs
#PBS -N _hg19
#PBS -q BioJob
#PBS -m abe
#PBS -r y
#PBS -J 1-12

# loading needed libraries
module load Bowtie2
module load Python/2.7.5
module load SamTools/1.6
module load bedtools/2.26.0
module load SamTools

#
LOGFILE=$PBS_O_WORKDIR/$PBS_JOBNAME"."$PBS_JOBID".log"
echo "$PBS_JOBID ($PBS_JOBNAME) @ `hostname` at `date` in "$RUNDIR" START" > $LOGFILE
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/ 2>&1 >>$LOGFILE
echo " changing dir..">>$LOGFILE
export TERM=xterm
infileline=`cat FULL_INFILE_PATH | tail -n +${PBS_ARRAY_INDEX} | head -1`
echo "this is run automation hg19  of sample $infileline " >> $LOGFILE

# find_circ
cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/ 2>&1 >>$LOGFILE
perl find_circ_automation.pl --i "$infileline" --l $LOGFILE --delete big
# for strict execution, use line below (only 40x40 anchors get re-aligned)
# perl find_circ_automation.pl --i "$infileline" --l $LOGFILE --delete big --strict 1

echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " f_c_automation hg19  run $infileline is done ...">>$LOGFILE
# note: can be added params: --strict 1 to only accept 40x40 align quality junctions

# DCC
module load STAR/2.5.1b
cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/automate_DCC/ 2>&1 >>$LOGFILE
perl dcc_automation.pl --i "$infileline" --star STAR --l $LOGFILE --delete big --linear 1
# strict alternative (filter + reapeats ignore)
# perl dcc_automation.pl --i "$infileline" --star STAR --l $LOGFILE --delete big --linear 1 --filter "-F -R /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/data_files/hg19_repeatmasker_simplerepeats_merged.gtf"
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " DCC_automation hg19 run $infileline is done ...">>$LOGFILE
# note: can add repeatsfilter like -- filter "-R path/to_hg_.gtf"

# CX1
cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/circexplorer1_auto/ 2>&1 >>$LOGFILE
perl circexplorer_automation.pl --i "$infileline" --delete big --l $LOGFILE
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " CIRCex1_automation hg19 run $infileline done ...">>$LOGFILE

qstat -f $PBS_JOBID
