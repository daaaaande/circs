#PBS -l select=1:ncpus=12:mem=46gb:arch=ivybridge
#PBS -l walltime=59:59:00
#PBS -A circs
#PBS -N aut_t2_175
#PBS -q BioJob
#PBS -m abe
#PBS -r y

# just in case- load all relevant
module load Bowtie2
module load Python/2.7.5
module load Parallel
module load SamTools/1.6
module load bedtools/2.26.0
module load BamTools
module load R
module load intel/xe2019



LOGFILE=$PBS_O_WORKDIR/$PBS_JOBNAME"."$PBS_JOBID".log"
echo "$PBS_JOBID ($PBS_JOBNAME) @ `hostname` at `date` in "$RUNDIR" START" > $LOGFILE
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo "this istest al automation runs ######################################################################################" >> $LOGFILE


cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/ 2>&1 >>$LOGFILE
echo " changing dir..">>$LOGFILE
export TERM=xterm

# filenames retrieval
#infileline=`cat /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/infile_myc_rnase_part2.tx | tail -n +${PBS_ARRAY_INDEX} | head -1`
infileline="AK055.mkdup_1.fastq  AK055.mkdup_2.fastq  sample_run_2_175  test_runs"

echo "this is automation run of sample $infileline " >> $LOGFILE


# find_circ
cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/ 2>&1 >>$LOGFILE
# hg19
perl find_circ_automation.pl --i  "$infileline" --l $LOGFILE
# now the same with hg38 params
perl find_circ_automation.pl --i "$infileline" --out /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/f_c2_out --f_q /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/all_chroms --ref /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38 --anot /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/hg38_ucsc_refseq_all.bed --l $LOGFILE
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " f_c run $infileline is done ...">>$LOGFILE


# DCC
cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/automate_DCC/ 2>&1 >>$LOGFILE
# hg19
module load STAR/2.5.1b
perl dcc_automation.pl --i "$infileline" --star STAR --l $LOGFILE
module unload STAR/2.5.1b
# hg38
module load STAR/2.7.1a
perl dcc_automation.pl --i "$infileline" --out /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/dcc_2_out --f_q /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_full.fa --ref /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full --anot /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_ucsc_ref_new_chroms.gtf --bed /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/hg38_ucsc_refseq_all.bed --l $LOGFILE --dc_co "python2 /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/DCC/DCC/main.py"
module unload STAR/2.7.1a
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " DCC run $infileline is done ...">>$LOGFILE


# CX
cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/circexplorer1_auto/ 2>&1 >>$LOGFILE
#hg19
module load STAR/2.5.1b
perl circexplorer_automation.pl --i "$infileline" --l $LOGFILE
# hg38
module unload STAR/2.5.1b
module load STAR/2.7.1a
perl circexplorer_automation.pl --i "$infileline" --out /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/cx2_out --f_q /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_full.fa --ref /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full --anot /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg_38_ref_fetched.txt --bed /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/hg38_ucsc_refseq_all.bed --l $LOGFILE
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " CIRCex run $infileline should also be done ...">>$LOGFILE

# end
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE

qstat -f $PBS_JOBID
