#PBS -l select=1:ncpus=12:mem=42gb:arch=ivybridge
#PBS -l walltime=9:59:59
#PBS -A circs
#PBS -N lena_hg38
#PBS -q BioJob
#PBS -m abe
#PBS -r y
#PBS -J 1-12


# just in case- load all relevant
module load Bowtie2
module load Python/2.7.5
#module load Parallel
module load SamTools/1.6
module load bedtools/2.26.0
#module load TopHat
#module load BamTools
#module load Perl
#module load R
#module load intel/xe2019
#module load gcc/runtime 


LOGFILE=$PBS_O_WORKDIR/$PBS_JOBNAME"."$PBS_JOBID".log"
echo "$PBS_JOBID ($PBS_JOBNAME) @ `hostname` at `date` in "$RUNDIR" START" > $LOGFILE
echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo "this is run hg 38 test files  ######################################################################################" >> $LOGFILE


cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/ 2>&1 >>$LOGFILE
echo " changing dir..">>$LOGFILE
# now starting the dependencies war...
# term problems in the past...
export TERM=xterm

# DCC install

cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/dcc/DCC

# filename retrieval
infileline=`cat FULL_INFILE_PATH | tail -n +${PBS_ARRAY_INDEX} | head -1`
echo "this is run 1 of sample $infileline " >> $LOGFILE


# find_circ

cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/ 2>&1 >>$LOGFILE
perl full_find_circ_upd.pl	$infileline

echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " f_c run is done ...">>$LOGFILE


# DCC

cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/automate_DCC/ 2>&1 >>$LOGFILE
module load STAR/2.7.1a

perl dcc_single_sample_upd.pl	$infileline

echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " DCC run is done ...">>$LOGFILE


# CX1

cd /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/circexplorer1_auto/ 2>&1 >>$LOGFILE
perl circex2_one_sample_upd.pl    $infileline

echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE
echo " CIRCex1 run should also be done ...">>$LOGFILE



echo "`date +"%d.%m.%Y-%T"`" >> $LOGFILE

qstat -f $PBS_JOBID
