#!/usr/bin/perl -w
use strict;
# script that - if invoked by user (either from run_prep_guide.pl or manually) unpacks all .gz files with an arrayjob of given directory


# non-interactive for now


use Getopt::Long qw(GetOptions);

my$dir_to_unpack=`pwd`;# if no dir is given, its .
my$use_pbs_job=1;       # if no pbs job will be used, just execute all at once as part of the interactive jpb (--pbs 0 or --use_pbs 0)
my$pbs_job_example_file="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/execution_scripts/e_example_unpack.sh";

GetOptions('gz_dir|g=s' => \$dir_to_unpack,'jobfile|j=s'=>\$pbs_job_example_file,'use_pbs|pbs=i'=>\$use_pbs_job )|| warn "using default parameters!\n";

# get array size
# check/print output
# edit jobscript
# qsub maybe?
#
# here we need only the dir, we will figure out how many .gz files we have anyway with ls -f1 *.gz | wc -l


# if the job script is started with ". as dir, it will fail- we need tp pwd the dir we are currently in
#"


my$files_list_to_unpack=`ls -f1 $dir_to_unpack/*.gz`;
chomp $files_list_to_unpack;
my@all_gzs=split(/\s+/,$files_list_to_unpack);
my$numfiles_to_unpack=`ls -f1 $dir_to_unpack/*.gz | wc -l`;
chomp $numfiles_to_unpack;
print "in dir $dir_to_unpack found $numfiles_to_unpack .gz packed files, preparing to unpack ...\n";


if($use_pbs_job){
      # copy the file to .
      `cp $pbs_job_example_file ./unpack_job_script.sh`;
      # edit e_example_unpack.sh
      # number of files
      `sed -i 's|PBS_NUMFILES|$numfiles_to_unpack|' ./unpack_job_script.sh`;
      # next, direcotry

      `sed -i 's|DIR_TO_GZ_FILES|$dir_to_unpack|' ./unpack_job_script.sh`;

      print "edited the example unpack file, now qsub...\n";

      #, qsub
      `qsub -q default ./unpack_job_script.sh`;

}
else{
      foreach my $file (@all_gzs){
            print "unpacking $dir_to_unpack/$file ...\n";
            `gunzip $dir_to_unpack/$file`;
      }

}
