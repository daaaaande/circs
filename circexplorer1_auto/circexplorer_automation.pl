#/usr/bin/perl -w
use strict;
use Getopt::Long qw(GetOptions);
# CIRCex1/2 hg38/hg19 for paired end/single end
# perl /auto_circs/circexplorer1_auto/circexplorer_automation.pl --i "sample1_fq1.fq  sample1_fq2.fq  samplename  outdir_allsamples" --m pe|se --cx cx1|cx2 --t THREADS --dry 1|0 --pld /path/to/circexoutreader_pl_script/ --l logs/logfile.log --i_dir infile_fastq_/location/dir/ --f_q path/to_reference_genome/fastqs/fastq.fq --ref path/to/star/genome/index/dir --anot path/to_annotation_file.txt
# perl circexplorer_automation.pl --i "$infileline"
# perl circexplorer_automation.pl --i "$infileline" --out /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/cx2_out --f_q /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_full.fa --ref /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full --anot /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg_38_ref_fetched.txt --bed /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/auto_find_circ/hg38_ucsc_refseq_all.bed

#### input parameters ####
my $start = time;
# file directories- you will need to adapt those with the options given                                     # hg 38 /CIRCexplorer2 equivalent opotions
my$base_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs";
my$out_dir="$base_dir/cx1_out"; # can also use                                                              "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/cx2_out"
my$exec_mode="cx1"; # cx1=CIRCexplorer1, cx2=CIRCexplorer2 commands
my$cx_command_parse="python2.7 $base_dir/pipelines/star_parse.py"; # thats how one starts CX or
my$cx_command_exec="python2.7  $base_dir/pipelines/CIRCexplorer.py";     #
my$cx2_command_parse="python /home/daric102/.local/bin/CIRCexplorer2 parse -t STAR"; # for --cx cx2 option
my$cx2_command_exec="python /home/daric102/.local/bin/CIRCexplorer2 annotate";      # for --cx cx2 option

my$pl_scripts="$base_dir/circexplorer1_auto";# dir where the parser script should be
my$infile_dir=$base_dir;
my$fastqs_files="$base_dir/pipelines/hg19.fa";# can also use                                              "gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg38_full.fa"
# other parameters
my$refseq_file="$base_dir/pipelines/hg19_ref.txt";# can also use                                             "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/hg_38_ref_fetched.txt"
my$bt_ref="$base_dir/pipelines";# where STAR genome index is stored can also use                            "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full"
my$input_line="A_1.fastq      A_2.fastq   A  output_cx";
my$bed_ref="$base_dir/pipelines/Genes_RefSeq_hg19_09.20.2013.bed"; # can also be                             "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full/chr_refseq_genes_hg38.bed"
my$mode="pe"; # "pe"- paired end, 2 fastq files per sample ;"se"- single end, one fastq file per sample (no linetwo file needed)
my$dryRun=0; # enter a 1 here to only have the used command printed out, and not executed (good for testing)
my$err_file="../logfile_auto.log"; # logfile
my$threads=12;
my$star_command="STAR";
my$delete="none";# options are "none" for no deletion of in-between files, "big" for only big files
# STAR versions: 2.7.1a /2.5.1.b
# hg19 own source: "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/star/STAR-2.5.1b/bin/Linux_x86_64/STAR"
# hg38 own source: "/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/star/STAR-master/bin/Linux_x86_64/STAR"

# retrieving non-default parameters if given- for --i "fastq.fastq (fastq2.fastq) samplename outdir"
GetOptions('base_dir|B=s' => \$base_dir,'out_dir|out=s' =>\$out_dir,'logfile|l=s' => \$err_file,'threads|t=i' => \$threads,'parser_dir|pld=s'=>\$pl_scripts,'exec_mode|cx=s' =>\$exec_mode,'input_line|i=s' => \$input_line,'mode|M=s' => \$mode,'dry_run|dry=i' => \$dryRun,'infile_dir|i_dir=s' => \$infile_dir,'fastq_files|f_q=s' => \$fastqs_files,'bt_ref|ref=s' => \$bt_ref,'annotation_file|anot=s' =>\$refseq_file,'bed_ref|bed=s'=>\$bed_ref,'star_command|star=s'=>\$star_command,'delete|del=s'=>\$delete) or warn "Using default now\n";
# 13 parameters
chomp($base_dir,$out_dir,$err_file,$threads,$input_line,$mode,$dryRun,$infile_dir,$fastqs_files,$bt_ref,$delete,$refseq_file,$pl_scripts,$exec_mode,$bed_ref,$star_command);
chdir $out_dir;
# for debugging and logging: dump final params
# for input line: hand parts into final params
my@split_input_line=split(/\s+/,$input_line);
my$lineonefile=$split_input_line[0];
my$linetwofile=$split_input_line[1];
my$sample_name=$split_input_line[2];
my$outfiles_dir=$split_input_line[3];

if($mode eq "pe" && ($outfiles_dir eq "")){# 4th element is only whitespace
  print("set to paired-end mode but found only 3 columns in infile: switching to single end mode\n");
  $mode="se";
}

# single end mode = only one fastq file
if($mode eq "se"){
      $lineonefile=$split_input_line[0];
      $sample_name=$split_input_line[1];
      $outfiles_dir=$split_input_line[2];
      $linetwofile="none";# this var should then not be needed anymore

}

# check the input file itself: if the line has onöy three components, it should run as


# creating full file paths for later
my$full_out_dir="$out_dir/$outfiles_dir";
my$sample_dir_out="$out_dir/run_$sample_name";# /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/f_c2_out/run_A/. <- where all files belonging to sample A will be saved
mkdir "$sample_dir_out";
chdir "$sample_dir_out";
#### start of execution ####
open(ER,'>>',$err_file)||die "$!";
print ER "started CIRCexplorer $exec_mode $bt_ref run with parameters:\nbase_dir=$base_dir\nthreadsN=$threads\ninfile1=$lineonefile\tinfile2=$linetwofile\tmode=$mode\ndryRun=$dryRun\nsamplename=$sample_name\nout_dir_sample=$sample_dir_out\nreference_files_in_(STAR index dir)=$bt_ref\nfastq_reference_in=$fastqs_files\nwill_copy_final_output_into=$full_out_dir\nref=$bt_ref\nannotation_file=$refseq_file\nparser_dir=$pl_scripts\nstar_command=$star_command\ndeleting=$delete\n";

if($dryRun){
      # just print all commands
      print"\n\ndry run mode. commands:\n";
      if($mode eq "pe"){
            # first alignment: both lanes
            print "$star_command --runThreadN $threads --genomeDir $bt_ref --readFilesIn $infile_dir/$lineonefile $infile_dir/$linetwofile --chimSegmentMin 10 --limitBAMsortRAM 512000000000\n";
      }
      elsif($mode eq "se"){
            # single read
            print "$star_command --runThreadN $threads --genomeDir $bt_ref --readFilesIn $infile_dir/$lineonefile --chimSegmentMin 10 --limitBAMsortRAM 512000000000\n";
      }
      if($exec_mode eq "cx1"){
            # execute cx1 commands here
            print "$cx_command_parse $sample_dir_out/Chimeric.out.junction $sample_dir_out/fusion_junction.txt\n";
            print "$cx_command_exec -j $sample_dir_out/fusion_junction.txt -g $fastqs_files -r $refseq_file\n";

      }
      elsif($exec_mode eq "cx2"){
            print "$cx2_command_parse $sample_dir_out/Chimeric.out.junction >$sample_dir_out/CIRCexplorer2_parse.log\n";
            print "$cx2_command_exec -r $refseq_file -g $fastqs_files -b $sample_dir_out/back_spliced_junction.bed -o $sample_dir_out/$sample_name.processed.tsv\n";
      }
      else{
            die "no exec mode detected\nuse --cx option (--cx cx1 || --cx cx2 )\n";
      }
      print "perl $pl_scripts/circexplorer1_out_reader.pl $sample_dir_out/CIRCexplorer_circ.txt $sample_dir_out/$sample_name.processed.tsv $sample_name\n";
      print "cp $sample_dir_out/$sample_name.processed.tsv $full_out_dir/\n";
      print "dry run done. \n";
}
# dry run finish
else{
      print ER "started at $start with CX sample $sample_name ########################\n";
      if($mode eq "pe"){
            # first alignment: both lanes
            my$er_bt=`$star_command --runThreadN $threads --genomeDir $bt_ref --readFilesIn $infile_dir/$lineonefile $infile_dir/$linetwofile --chimSegmentMin 10 --limitBAMsortRAM 512000000000`;
            print ER "STAR first alignment paired end:$er_bt\n ";
      }

      elsif($mode eq "se"){
            # single read
            my$err_star_line1=`$star_command --runThreadN $threads --genomeDir $bt_ref --readFilesIn $infile_dir/$lineonefile --chimSegmentMin 10 --limitBAMsortRAM 512000000000`;
            print ER "errors during STAR single lane alignment:\n $err_star_line1\n";
      }

      if($exec_mode eq "cx1"){
            # execute cx1 commands here
            my$btofq= `$cx_command_parse $sample_dir_out/Chimeric.out.junction $sample_dir_out/fusion_junction.txt`;
            print ER "errors converting: in $sample_dir_out/ : $btofq\n";

            my$err_steptwocx=`$cx_command_exec -j $sample_dir_out/fusion_junction.txt -g $fastqs_files -r $refseq_file`;
            print ER "errors executing $cx_command_exec in $sample_dir_out/ :$err_steptwocx \n ";


      }
      elsif($exec_mode eq "cx2"){
            my$cx2_parse_out=`$cx2_command_parse $sample_dir_out/Chimeric.out.junction >$sample_dir_out/CIRCexplorer2_parse.log`;
            print ER "CX2 parsing sample $sample_name errors: $cx2_parse_out\n";

            my$anot_cx2=`$cx2_command_exec -r $refseq_file -g $fastqs_files -b $sample_dir_out/back_spliced_junction.bed -o $sample_dir_out/$sample_name.processed.tsv`;
            print ER "CX2 annotate $sample_name: $anot_cx2\n";

      }
      else{
            die "no exec mode detected\nuse --cx option (--cx cx1 || --cx cx2 )\n";
      }


      my$err2=`perl $pl_scripts/circexplorer1_out_reader.pl $sample_dir_out/CIRCexplorer_circ.txt $sample_dir_out/$sample_name.processed.tsv $sample_name`;
      print ER "errors executing circexplorer1_out_reader:\n$err2\n";

      my$errortwo=`cp $sample_dir_out/$sample_name.processed.tsv $full_out_dir/`;
      print ER "CX2 e_copy $sample_name: $errortwo\n";

      # now deleting if desired the in-between files
      if($delete eq "big"){
        chdir "$sample_dir_out";
        `rm $sample_dir_out/*.bam $sample_dir_out/*.sam $sample_dir_out/*.qfa $sample_dir_out/*Unmapped.out* $sample_dir_out/*.junction $sample_dir_out/*.reads`;
      }
      elsif($delete eq "none"){
        chdir "$sample_dir_out";
        print ER "user chose to not delete any files\n";
      }
      else{
        print ER "could not recognize delete choice: use --delete none|big\n";
      }

      my$fulltime=((time - $start)/60);
      print ER "done processing $sample_name in $fulltime minutes \n";
      print ER "############################################################\n";

}
1;
