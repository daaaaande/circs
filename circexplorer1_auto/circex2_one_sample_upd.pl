# wrapper for one single execution per sample- at a time
#!/usr/bin/perl -w
use strict;

open(ER,'>>',"../logfile_auto.log")||die "$!";		# global logfile

chdir "../cx2_out/";

my$file_1=$ARGV[0];
chomp $file_1;
my$file_2=$ARGV[1];
chomp $file_2;
my$sample_name=$ARGV[2];
chomp $sample_name;
my$all_out_dir=$ARGV[3]; # directory where this script should put the final outfile into, from all infiles
chomp $all_out_dir;

my$out_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/cx2_out";
my$scrips_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/upd/CIRCexplorer2/circ2";# really?
my$bowtie_index_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/pipelines/hg38_full"; # bowtie2 index build dir, refernce out called "hg38".
my$infile_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs";

# execute everything for that samploe in that sample dir
mkdir "run_$sample_name";
# make dir, move files there, one dir per sample
chdir "run_$sample_name";#
# we are now in /gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/cx2_out/run_$sample_name/.
my$exec_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs/cx2_out/run_$sample_name";

my$tim=localtime();
my$starttime= time;
print ER "##############################################################\n";
print ER "starting @ $tim \nfinding circs in sample $sample_name with CIRCexplorer2...\n";

my$er_star=`STAR --chimSegmentMin 10 --runThreadN 10 --genomeDir $bowtie_index_dir/ --readFilesIn $infile_dir/$file_1 $infile_dir/$file_2 --limitBAMsortRAM 512000000000`;
print ER "STAR CX2 errors: $er_star\n";


# parsing
my$cx2_parse=`python /home/daric102/.local/bin/CIRCexplorer2 parse -t STAR $exec_dir/Chimeric.out.junction >$exec_dir/CIRCexplorer2_parse.log`;
print ER "CX2 parsing sample $sample_name errors: $cx2_parse\n";

#my$cx_edit=`sed 's/^/chr/' $exec_dir/back_spliced_junction.bed >$exec_dir/corrected_back_spliced_junction.bed`;# coordinates here are based on chr+num, not num only.
#print ER "CX2 edit $sample_name: $cx_edit\n";

# annotating
my$anot_cx2=`python /home/daric102/.local/bin/CIRCexplorer2 annotate -r $bowtie_index_dir/hg_38_ref_fetched.txt -g $bowtie_index_dir/hg38_full.fa -b $exec_dir/back_spliced_junction.bed -o $exec_dir/cx2_out_$sample_name.tsv`;
print ER "CX2 annotate $sample_name: $anot_cx2\n";
# now the outreader should work from previous versions

my$convert_cx2=`perl $infile_dir/circexplorer1_auto/circexplorer1_out_reader.pl $exec_dir/cx2_out_$sample_name.tsv $exec_dir/parsed_cx2_out_$sample_name.tsv $sample_name`;
print ER "CX2 circexplorer1_out_reader $sample_name: $convert_cx2\n";
# now copy the file to third argument
my$cp_out=`cp $exec_dir/parsed_cx2_out_$sample_name.tsv $out_dir/$all_out_dir/`;
print ER "CX2 e_copy $sample_name: $cp_out\n";



my$fulltime=((time - $starttime)/60);
print ER "done processing $sample_name in $fulltime minutes \n";

print ER "############################################################\nsample  CX2 hg38  $sample_name done.\n";
1;
