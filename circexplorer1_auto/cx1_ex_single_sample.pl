# wrapper for one single execution per sample- at a time
#!/usr/bin/perl -w
use strict;

`clear`;

open(ER,'>>',"../logfile_auto.log")||die "$!";		# global logfile

chdir "../cx1_out/";
# we expect here the same input as we got from the prior infile:
#inputs: file1,file2,sample,(group)
# could be doable with head/tail of the infile based on array index
# such as:
# filename=`cat /scratch_gs/daric102/pipeline_old_circs_infile.infile | tail -n +${PBS_ARRAY_INDEX} | head -1`
#so now we would get the full line, we might have to split it, argv should take care of this
my$file_1=$ARGV[0];
my$file_2=$ARGV[1];
my$sample_name=$ARGV[2];
my$all_out_dir=$ARGV[3]; # directory where this script should put the final outfile into, from all infiles

my$base_dir="/gpfs/project/daric102/circs_hilbert_scratchgs/repo/circs";
my$tim=localtime();
print ER "##############################################################\n";
print ER "starting @ $tim \nfinding circs in sample $sample_name with circexplorer1...\n";

# first execute circex1
my$error_1=`perl $base_dir/circexplorer1_auto/circexplorer1_starter_1.pl $file_1 $file_2 $sample_name`;
print "errors executing cx1 on $sample_name:\n$error_1\n";
my$err2=`perl $base_dir/circexplorer1_auto/circexplorer1_out_reader.pl run_$sample_name/CIRCexplorer_circ.txt run_$sample_name/$sample_name.processed.tsv $sample_name`;
print "errors executing circexplorer1_out_reader:\n$err2\n";
# puts the outfile where all cx1 outfiles will be
my$errortwo=`cp run_$sample_name/$sample_name.processed.tsv $all_out_dir/`;
1;
